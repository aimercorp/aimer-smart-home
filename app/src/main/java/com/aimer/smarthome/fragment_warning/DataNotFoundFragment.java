package com.aimer.smarthome.fragment_warning;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.DialogFragment;

import com.aimer.smarthome.R;


public class DataNotFoundFragment extends DialogFragment implements View.OnClickListener {


    public DataNotFoundFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_not_found, container, false);
        Button mBtnCancel = view.findViewById(R.id.btn_data_not_found_cancel);

        mBtnCancel.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_data_not_found_cancel:
                    dismiss();
                break;
        }
    }
}
