package com.aimer.smarthome.sectioned_expandable;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import com.aimer.smarthome.R;
import com.aimer.smarthome.model.DeviceAlert;
import com.aimer.smarthome.model.DeviceCCTV;
import com.aimer.smarthome.model.DeviceElectricMeter;
import com.aimer.smarthome.model.DeviceFireAlarm;
import com.aimer.smarthome.model.DeviceFrontSlidingDoor;
import com.aimer.smarthome.model.DeviceLight;
import com.aimer.smarthome.model.DeviceMagneticDoor;
import com.aimer.smarthome.model.DeviceMagneticSensor;
import com.aimer.smarthome.model.DeviceTelevision;
import com.aimer.smarthome.model.DeviceThermometer;
import com.aimer.smarthome.model.DevicesAirConditioner;
import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.model.ModelDevices;
import com.aimer.smarthome.mqtt.MqttHelper;
import com.aimer.smarthome.utils.ItemClickListener;
import com.aimer.smarthome.utils.ItemClickListenerOnClickDevices;
import com.aimer.smarthome.utils.SectionStateChangeListener;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SectionedBoardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private static final String TAG ="SectionedBoardAdapter";
    //data array
    private ArrayList<Object> mDataArrayList;
    private ArrayList<Object> mDataArrayListFiltered = new ArrayList<Object>();

    //context
    private final Context mContext;

    //listeners
    private final ItemClickListener mItemClickListener;
    private final ItemClickListenerOnClickDevices mItemClickListenerOnClickDevices;
    private final SectionStateChangeListener mSectionStateChangeListener;

    //view type
    private static final int VIEW_TYPE_BOARD = 0;
    private static final int VIEW_TYPE_LIGHT = 1;
    private static final int VIEW_TYPE_THERMOMETER = 2;
    private static final int VIEW_TYPE_MAGNETIC_DOOR = 3;
    private static final int VIEW_TYPE_AIR_CONDITIONER = 4;
    private static final int VIEW_TYPE_TELEVISION = 5;
    private static final int VIEW_TYPE_CCTV = 6;
    private static final int VIEW_TYPE_MAGNETIC_SENSOR = 7;
    private static final int VIEW_TYPE_FIRE_ALARM = 8;
    private static final int VIEW_TYPE_ALERT = 9;
    private static final int VIEW_TYPE_FRONT_SLIDING_DOOR = 10;
    private static final int VIEW_TYPE_ELECTRIC_METER = 11;


    //mqtt helper
    private MqttHelper MqttClient;
    private MqttAndroidClient client;
    private String android_id;


    public SectionedBoardAdapter(Context context, ArrayList<Object> dataArrayList, final GridLayoutManager gridLayoutManager, SectionStateChangeListener sectionStateChangeListener,ItemClickListener itemClickListener,ItemClickListenerOnClickDevices itemClickListenerOnClickDevices) {
        this.mContext = context;
        this.mDataArrayList = dataArrayList;
        this.mSectionStateChangeListener = sectionStateChangeListener;
        this.mItemClickListener = itemClickListener;
        this.mItemClickListenerOnClickDevices = itemClickListenerOnClickDevices;
        this.mDataArrayListFiltered = dataArrayList;

        android_id = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        MqttClient = new MqttHelper();
        client = MqttClient.getMqttClient(mContext, android_id);


        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return isSection(position)?gridLayoutManager.getSpanCount():1;
            }
        });
    }




    private boolean isSection(int position) {
        return mDataArrayListFiltered.get(position) instanceof ModelBoard;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEW_TYPE_BOARD:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_expandable_home, parent, false);
                return new BoardViewHolder(view);
            case VIEW_TYPE_LIGHT:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_expandable_devices, parent, false);
                return new DeviceLightHolder(view);
            case VIEW_TYPE_THERMOMETER:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_item_thermometer, parent, false);
                return new DeviceThermometerHolder(view);
            case VIEW_TYPE_MAGNETIC_DOOR:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_item_magnetic_door, parent, false);
                return new DeviceMagneticDoorHolder(view);
            case VIEW_TYPE_AIR_CONDITIONER:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_expandable_devices, parent, false);
                return new DeviceAirConditionerHolder(view);
            case VIEW_TYPE_TELEVISION:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_expandable_devices, parent, false);
                return new DeviceTelevisionHolder(view);
            case VIEW_TYPE_CCTV:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_expandable_devices, parent, false);
                return new DeviceCCTVHolder(view);
            case VIEW_TYPE_MAGNETIC_SENSOR:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_expandable_devices, parent, false);
                return new DeviceMagneticSensorHolder(view);
            case VIEW_TYPE_FIRE_ALARM:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_expandable_devices, parent, false);
                return new DeviceFireAlarmHolder(view);
            case VIEW_TYPE_ALERT:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_expandable_devices, parent, false);
                return new DeviceAlertHolder(view);
            case VIEW_TYPE_FRONT_SLIDING_DOOR:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_item_magnetic_door, parent, false);
                return new DeviceFrontSlidingDoorHolder(view);
            case VIEW_TYPE_ELECTRIC_METER:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_expandable_devices, parent, false);
                return new DeviceElectricMeterHolder(view);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();
        switch (viewType){
            case VIEW_TYPE_BOARD:
                final ModelBoard board = (ModelBoard) mDataArrayListFiltered.get(position);
                ((BoardViewHolder)holder).mNameBoard.setText(board.getBoardName());
                ((BoardViewHolder)holder).mSectionToggleButton.setChecked(board.isExpanded);
                ((BoardViewHolder)holder).mSectionToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mSectionStateChangeListener.onSectionStateChanged(board, isChecked);
                    }
                });
                break;
            case VIEW_TYPE_LIGHT:
                final DeviceLight light = (DeviceLight) mDataArrayListFiltered.get(position);
                ((DeviceLightHolder)holder).mNameDevices.setText(light.getDevicesName());
                if (light.getDevicesIcon() != null && light.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, light.getDevicesIcon());
                    ((DeviceLightHolder)holder).imgDevices.setImageResource(resId);
                }

                if (light.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceLightHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceLightHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                if (light.getDevicesStatus().equals("1")) {
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceLightHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceLightHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);

                } else {
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceLightHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceLightHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                }

                ((DeviceLightHolder)holder).clickOnDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceLightHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceLightHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOnDevices(light);
                    }
                });

                ((DeviceLightHolder)holder).clickOffDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceLightHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceLightHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOffDevices(light);
                    }
                });

                ((DeviceLightHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(light);
                    }
                });

                ((DeviceLightHolder)holder).imgFavoriteDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(light,((DeviceLightHolder)holder).imgFavoriteDevices);

                    }
                });

                break;
            case VIEW_TYPE_THERMOMETER:
                final DeviceThermometer thermometer = (DeviceThermometer) mDataArrayListFiltered.get(position);
                ((DeviceThermometerHolder)holder).mNameThermometer.setText(thermometer.getDevicesName());
                if (thermometer.getDevicesIcon() != null && thermometer.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, thermometer.getDevicesIcon());
                    ((DeviceThermometerHolder)holder).imgThermometer.setImageResource(resId);
                }

                if (thermometer.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceThermometerHolder)holder).imgFavoriteThermometer.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceThermometerHolder)holder).imgFavoriteThermometer.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                String JsonFormat = "{\"deviceId\":\""+thermometer.getModelBoard().getBoardMacAddress()+"\",\"action\":\"action.dht.GetTemp\"}";
                sendEvent(JsonFormat,thermometer.getModelBoard().getBoardMacAddress()+"/ESP/DHT");

                try {
                    MqttClient.subscribe(client,thermometer.getModelBoard().getBoardMacAddress()+"/ESP/DHT",0);
                } catch (MqttException e) {
                    e.printStackTrace();
                }
                    mItemClickListener.setTemp(((DeviceThermometerHolder)holder).itemThermometer);
                ((DeviceThermometerHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedThermometer(thermometer);
                    }
                });

                ((DeviceThermometerHolder)holder).imgFavoriteThermometer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(thermometer, ((DeviceThermometerHolder)holder).imgFavoriteThermometer);
                    }
                });


                break;
            case VIEW_TYPE_MAGNETIC_DOOR:
                final DeviceMagneticDoor magneticDoor = (DeviceMagneticDoor) mDataArrayListFiltered.get(position);
                ((DeviceMagneticDoorHolder)holder).mNameMagneticDoor.setText(magneticDoor.getDevicesName());
                if (magneticDoor.getDevicesIcon() != null && magneticDoor.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, magneticDoor.getDevicesIcon());
                    ((DeviceMagneticDoorHolder)holder).imgMagneticDoor.setImageResource(resId);
                }

                if (magneticDoor.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceMagneticDoorHolder)holder).imgFavoriteMagneticDoor.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceMagneticDoorHolder)holder).imgFavoriteMagneticDoor.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                if (magneticDoor.getDevicesStatus().equals("1")) {
                    int resIdOn = getResourceColorByCode(mContext, R.color.open_magnetic_door);
                    int resIdOff = getResourceColorByCode(mContext, R.color.colorDark);
                    ((DeviceMagneticDoorHolder)holder).onClickOpenMagneticDoor.setTextColor(resIdOn);
                    ((DeviceMagneticDoorHolder)holder).onClickCloseMagneticDoor.setTextColor(resIdOff);

                } else {
                    int resIdOff = getResourceColorByCode(mContext, R.color.close_magnetic_door);
                    int resIdOn = getResourceColorByCode(mContext, R.color.colorDark);
                    ((DeviceMagneticDoorHolder)holder).onClickOpenMagneticDoor.setTextColor(resIdOn);
                    ((DeviceMagneticDoorHolder)holder).onClickCloseMagneticDoor.setTextColor(resIdOff);
                }

                ((DeviceMagneticDoorHolder)holder).onClickOpenMagneticDoor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListenerOnClickDevices.itemClickedOnDevices(magneticDoor);
                        int resIdOn = getResourceColorByCode(mContext, R.color.open_magnetic_door);
                        int resIdOff = getResourceColorByCode(mContext, R.color.colorDark);
                        ((DeviceMagneticDoorHolder)holder).onClickOpenMagneticDoor.setTextColor(resIdOn);
                        ((DeviceMagneticDoorHolder)holder).onClickCloseMagneticDoor.setTextColor(resIdOff);
                    }
                });

                ((DeviceMagneticDoorHolder)holder).onClickCloseMagneticDoor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOff = getResourceColorByCode(mContext, R.color.close_magnetic_door);
                        int resIdOn = getResourceColorByCode(mContext, R.color.colorDark);
                        ((DeviceMagneticDoorHolder)holder).onClickOpenMagneticDoor.setTextColor(resIdOn);
                        ((DeviceMagneticDoorHolder)holder).onClickCloseMagneticDoor.setTextColor(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOffDevices(magneticDoor);
                    }
                });

                ((DeviceMagneticDoorHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedMagneticDoor(magneticDoor);
                    }
                });

                ((DeviceMagneticDoorHolder)holder).imgFavoriteMagneticDoor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(magneticDoor,((DeviceMagneticDoorHolder)holder).imgFavoriteMagneticDoor);
                    }
                });

                break;
            case VIEW_TYPE_AIR_CONDITIONER:
                final DevicesAirConditioner airConditioner = (DevicesAirConditioner) mDataArrayListFiltered.get(position);
                ((DeviceAirConditionerHolder)holder).mNameDevices.setText(airConditioner.getDevicesName());
                if (airConditioner.getDevicesIcon() != null && airConditioner.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, airConditioner.getDevicesIcon());
                    ((DeviceAirConditionerHolder)holder).imgDevices.setImageResource(resId);
                }

                if (airConditioner.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceAirConditionerHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceAirConditionerHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                if (airConditioner.getDevicesStatus().equals("1")) {
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceAirConditionerHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceAirConditionerHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);

                } else {
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceAirConditionerHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceAirConditionerHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                }

                ((DeviceAirConditionerHolder)holder).clickOnDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceAirConditionerHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceAirConditionerHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOnDevices(airConditioner);
                    }
                });

                ((DeviceAirConditionerHolder)holder).clickOffDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceAirConditionerHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceAirConditionerHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOffDevices(airConditioner);
                    }
                });

                ((DeviceAirConditionerHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(airConditioner);
                    }
                });

                ((DeviceAirConditionerHolder)holder).imgFavoriteDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(airConditioner,((DeviceAirConditionerHolder)holder).imgFavoriteDevices);

                    }
                });
                break;

            case VIEW_TYPE_TELEVISION:
                final DeviceTelevision television = (DeviceTelevision) mDataArrayListFiltered.get(position);
                ((DeviceTelevisionHolder)holder).mNameDevices.setText(television.getDevicesName());
                if (television.getDevicesIcon() != null && television.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, television.getDevicesIcon());
                    ((DeviceTelevisionHolder)holder).imgDevices.setImageResource(resId);
                }

                if (television.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceTelevisionHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceTelevisionHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                if (television.getDevicesStatus().equals("1")) {
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceTelevisionHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceTelevisionHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);

                } else {
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceTelevisionHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceTelevisionHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                }

                ((DeviceTelevisionHolder)holder).clickOnDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceTelevisionHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceTelevisionHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOnDevices(television);
                    }
                });

                ((DeviceTelevisionHolder)holder).clickOffDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceTelevisionHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceTelevisionHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOffDevices(television);
                    }
                });

                ((DeviceTelevisionHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(television);
                    }
                });

                ((DeviceTelevisionHolder)holder).imgFavoriteDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(television,((DeviceTelevisionHolder)holder).imgFavoriteDevices);

                    }
                });
                break;

            case VIEW_TYPE_CCTV:
                final DeviceCCTV cctv = (DeviceCCTV) mDataArrayListFiltered.get(position);
                ((DeviceCCTVHolder)holder).mNameDevices.setText(cctv.getDevicesName());
                if (cctv.getDevicesIcon() != null && cctv.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, cctv.getDevicesIcon());
                    ((DeviceCCTVHolder)holder).imgDevices.setImageResource(resId);
                }

                if (cctv.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceCCTVHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceCCTVHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                if (cctv.getDevicesStatus().equals("1")) {
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceCCTVHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceCCTVHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);

                } else {
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceCCTVHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceCCTVHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                }

                ((DeviceCCTVHolder)holder).clickOnDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceCCTVHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceCCTVHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOnDevices(cctv);
                    }
                });

                ((DeviceCCTVHolder)holder).clickOffDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceCCTVHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceCCTVHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOffDevices(cctv);
                    }
                });

                ((DeviceCCTVHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(cctv);
                    }
                });

                ((DeviceCCTVHolder)holder).imgFavoriteDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(cctv,((DeviceCCTVHolder)holder).imgFavoriteDevices);

                    }
                });
                break;

            case VIEW_TYPE_MAGNETIC_SENSOR:
                final DeviceMagneticSensor magneticSensor = (DeviceMagneticSensor) mDataArrayListFiltered.get(position);
                ((DeviceMagneticSensorHolder)holder).mNameDevices.setText(magneticSensor.getDevicesName());
                if (magneticSensor.getDevicesIcon() != null && magneticSensor.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, magneticSensor.getDevicesIcon());
                    ((DeviceMagneticSensorHolder)holder).imgDevices.setImageResource(resId);
                }

                if (magneticSensor.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceMagneticSensorHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceMagneticSensorHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                if (magneticSensor.getDevicesStatus().equals("1")) {
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceMagneticSensorHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceMagneticSensorHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);

                } else {
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceMagneticSensorHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceMagneticSensorHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                }

                ((DeviceMagneticSensorHolder)holder).clickOnDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceMagneticSensorHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceMagneticSensorHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOnDevices(magneticSensor);
                    }
                });

                ((DeviceMagneticSensorHolder)holder).clickOffDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceMagneticSensorHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceMagneticSensorHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOffDevices(magneticSensor);
                    }
                });

                ((DeviceMagneticSensorHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(magneticSensor);
                    }
                });

                ((DeviceMagneticSensorHolder)holder).imgFavoriteDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(magneticSensor,((DeviceMagneticSensorHolder)holder).imgFavoriteDevices);

                    }
                });
                break;

            case VIEW_TYPE_FIRE_ALARM:
                final DeviceFireAlarm fireAlarm = (DeviceFireAlarm) mDataArrayListFiltered.get(position);
                ((DeviceFireAlarmHolder)holder).mNameDevices.setText(fireAlarm.getDevicesName());
                if (fireAlarm.getDevicesIcon() != null && fireAlarm.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, fireAlarm.getDevicesIcon());
                    ((DeviceFireAlarmHolder)holder).imgDevices.setImageResource(resId);
                }

                if (fireAlarm.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceFireAlarmHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceFireAlarmHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                if (fireAlarm.getDevicesStatus().equals("1")) {
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceFireAlarmHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceFireAlarmHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);

                } else {
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceFireAlarmHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceFireAlarmHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                }

                ((DeviceFireAlarmHolder)holder).clickOnDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceFireAlarmHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceFireAlarmHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOnDevices(fireAlarm);
                    }
                });

                ((DeviceFireAlarmHolder)holder).clickOffDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceFireAlarmHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceFireAlarmHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOffDevices(fireAlarm);
                    }
                });

                ((DeviceFireAlarmHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(fireAlarm);
                    }
                });

                ((DeviceFireAlarmHolder)holder).imgFavoriteDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(fireAlarm,((DeviceFireAlarmHolder)holder).imgFavoriteDevices);

                    }
                });
                break;

            case VIEW_TYPE_ALERT:
                final DeviceAlert alert = (DeviceAlert) mDataArrayListFiltered.get(position);
                ((DeviceAlertHolder)holder).mNameDevices.setText(alert.getDevicesName());
                if (alert.getDevicesIcon() != null && alert.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, alert.getDevicesIcon());
                    ((DeviceAlertHolder)holder).imgDevices.setImageResource(resId);
                }

                if (alert.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceAlertHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceAlertHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                if (alert.getDevicesStatus().equals("1")) {
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceAlertHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceAlertHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);

                } else {
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceAlertHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceAlertHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                }

                ((DeviceAlertHolder)holder).clickOnDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceAlertHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceAlertHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOnDevices(alert);
                    }
                });

                ((DeviceAlertHolder)holder).clickOffDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceAlertHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceAlertHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOffDevices(alert);
                    }
                });

                ((DeviceAlertHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(alert);
                    }
                });

                ((DeviceAlertHolder)holder).imgFavoriteDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(alert,((DeviceAlertHolder)holder).imgFavoriteDevices);

                    }
                });
                break;

            case VIEW_TYPE_FRONT_SLIDING_DOOR:
                final DeviceFrontSlidingDoor frontSlidingDoor = (DeviceFrontSlidingDoor) mDataArrayListFiltered.get(position);
                ((DeviceFrontSlidingDoorHolder)holder).mNameFrontSlidingDoor.setText(frontSlidingDoor.getDevicesName());
                if (frontSlidingDoor.getDevicesIcon() != null && frontSlidingDoor.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, frontSlidingDoor.getDevicesIcon());
                    ((DeviceFrontSlidingDoorHolder)holder).imgFrontSlidingDoorDoor.setImageResource(resId);
                }

                if (frontSlidingDoor.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceFrontSlidingDoorHolder)holder).imgFavoriteFrontSlidingDoor.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceFrontSlidingDoorHolder)holder).imgFavoriteFrontSlidingDoor.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                if (frontSlidingDoor.getDevicesStatus().equals("1")) {
                    int resIdOn = getResourceColorByCode(mContext, R.color.open_magnetic_door);
                    int resIdOff = getResourceColorByCode(mContext, R.color.colorDark);
                    ((DeviceFrontSlidingDoorHolder)holder).onClickOpenFrontSlidingDoor.setTextColor(resIdOn);
                    ((DeviceFrontSlidingDoorHolder)holder).onClickCloseFrontSlidingDoor.setTextColor(resIdOff);

                } else {
                    int resIdOff = getResourceColorByCode(mContext, R.color.close_magnetic_door);
                    int resIdOn = getResourceColorByCode(mContext, R.color.colorDark);
                    ((DeviceFrontSlidingDoorHolder)holder).onClickOpenFrontSlidingDoor.setTextColor(resIdOn);
                    ((DeviceFrontSlidingDoorHolder)holder).onClickCloseFrontSlidingDoor.setTextColor(resIdOff);
                }

                ((DeviceFrontSlidingDoorHolder)holder).onClickOpenFrontSlidingDoor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListenerOnClickDevices.itemClickedOnDevices(frontSlidingDoor);
                        int resIdOn = getResourceColorByCode(mContext, R.color.open_magnetic_door);
                        int resIdOff = getResourceColorByCode(mContext, R.color.colorDark);
                        ((DeviceFrontSlidingDoorHolder)holder).onClickOpenFrontSlidingDoor.setTextColor(resIdOn);
                        ((DeviceFrontSlidingDoorHolder)holder).onClickCloseFrontSlidingDoor.setTextColor(resIdOff);
                    }
                });

                ((DeviceFrontSlidingDoorHolder)holder).onClickCloseFrontSlidingDoor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOff = getResourceColorByCode(mContext, R.color.close_magnetic_door);
                        int resIdOn = getResourceColorByCode(mContext, R.color.colorDark);
                        ((DeviceFrontSlidingDoorHolder)holder).onClickOpenFrontSlidingDoor.setTextColor(resIdOn);
                        ((DeviceFrontSlidingDoorHolder)holder).onClickCloseFrontSlidingDoor.setTextColor(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOffDevices(frontSlidingDoor);
                    }
                });

                ((DeviceFrontSlidingDoorHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        mItemClickListener.itemClickedMagneticDoor(frontSlidingDoor);
                    }
                });

                ((DeviceFrontSlidingDoorHolder)holder).imgFavoriteFrontSlidingDoor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(frontSlidingDoor,((DeviceFrontSlidingDoorHolder)holder).imgFavoriteFrontSlidingDoor);
                    }
                });

                break;

            case VIEW_TYPE_ELECTRIC_METER:
                final DeviceElectricMeter electricMeter = (DeviceElectricMeter) mDataArrayListFiltered.get(position);
                ((DeviceElectricMeterHolder)holder).mNameDevices.setText(electricMeter.getDevicesName());
                if (electricMeter.getDevicesIcon() != null && electricMeter.getDevicesIcon() != "null") {
                    int resId = getResourceByFilename(mContext, electricMeter.getDevicesIcon());
                    ((DeviceElectricMeterHolder)holder).imgDevices.setImageResource(resId);
                }

                if (electricMeter.isFavouritesStatus()){
                    DrawableCompat.setTint(((DeviceElectricMeterHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));
                }else {
                    DrawableCompat.setTint(((DeviceElectricMeterHolder)holder).imgFavoriteDevices.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
                }

                if (electricMeter.getDevicesStatus().equals("1")) {
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceElectricMeterHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceElectricMeterHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);

                } else {
                    int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                    int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                    ((DeviceElectricMeterHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                    ((DeviceElectricMeterHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                }

                ((DeviceElectricMeterHolder)holder).clickOnDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_on_click");
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceElectricMeterHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceElectricMeterHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOnDevices(electricMeter);
                    }
                });

                ((DeviceElectricMeterHolder)holder).clickOffDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resIdOff = getResourceByFilename(mContext, "shape_btn_on_off");
                        int resIdOn = getResourceByFilename(mContext, "shape_btn_base_bg");
                        ((DeviceElectricMeterHolder)holder).clickOnDevices.setBackgroundResource(resIdOn);
                        ((DeviceElectricMeterHolder)holder).clickOffDevices.setBackgroundResource(resIdOff);
                        mItemClickListenerOnClickDevices.itemClickedOffDevices(electricMeter);
                    }
                });

                ((DeviceElectricMeterHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(electricMeter);
                    }
                });

                ((DeviceElectricMeterHolder)holder).imgFavoriteDevices.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClickedFavourite(electricMeter,((DeviceElectricMeterHolder)holder).imgFavoriteDevices);

                    }
                });
                break;
        }
    }

    private void sendEvent(String JsonFormat,String Topic)
    {
        try {
            MqttClient.publishMessage(client, JsonFormat, 1, Topic);
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDataArrayListFiltered.get(position) instanceof ModelBoard){
            return VIEW_TYPE_BOARD;
        }else if (mDataArrayListFiltered.get(position) instanceof DeviceLight){
            return VIEW_TYPE_LIGHT;
        }else if (mDataArrayListFiltered.get(position) instanceof DeviceThermometer){
            return VIEW_TYPE_THERMOMETER;
        }else if (mDataArrayListFiltered.get(position) instanceof DeviceMagneticDoor){
            return VIEW_TYPE_MAGNETIC_DOOR;
        }else if (mDataArrayListFiltered.get(position) instanceof DevicesAirConditioner){
            return VIEW_TYPE_AIR_CONDITIONER;
        }else if (mDataArrayListFiltered.get(position) instanceof DeviceTelevision){
            return VIEW_TYPE_TELEVISION;
        }else if (mDataArrayListFiltered.get(position) instanceof DeviceCCTV){
            return VIEW_TYPE_CCTV;
        }else if (mDataArrayListFiltered.get(position) instanceof DeviceMagneticSensor){
            return VIEW_TYPE_MAGNETIC_SENSOR;
        }else if (mDataArrayListFiltered.get(position) instanceof DeviceFireAlarm){
            return VIEW_TYPE_FIRE_ALARM;
        }else if (mDataArrayListFiltered.get(position) instanceof DeviceAlert){
            return VIEW_TYPE_ALERT;
        }else if (mDataArrayListFiltered.get(position) instanceof DeviceFrontSlidingDoor){
            return VIEW_TYPE_FRONT_SLIDING_DOOR;
        }else if (mDataArrayListFiltered.get(position) instanceof DeviceElectricMeter){
            return VIEW_TYPE_ELECTRIC_METER;
        }

        return -1;
    }

    @Override
    public int getItemCount() {
        return mDataArrayListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String key = constraint.toString().replaceAll(" ", "");
                if (key.isEmpty()) {
                    mDataArrayListFiltered = mDataArrayList;
                }else if (key.equalsIgnoreCase("AllDevices") || key.equalsIgnoreCase("Other")) {
                    mDataArrayListFiltered = mDataArrayList;
                }else {
                    final ArrayList<Object> listFiltered = new ArrayList<Object>();
                    for (Object row : mDataArrayList){
                        if (row.toString().toLowerCase().contains(key.toLowerCase())){
                            listFiltered.add(row);
                        }
                    }
                    mDataArrayListFiltered = listFiltered;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mDataArrayListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mDataArrayListFiltered = (ArrayList<Object>) results.values;
                notifyDataSetChanged();
            }
        };
    }


    public class BoardViewHolder extends RecyclerView.ViewHolder{

        private TextView mNameBoard;
        private ToggleButton mSectionToggleButton;

        public BoardViewHolder(@NonNull View itemView) {
            super(itemView);

            mNameBoard =  itemView.findViewById(R.id.TextView_HouseName);
            mSectionToggleButton =  itemView.findViewById(R.id.toggle_button_section);
        }
    }

    public class DeviceLightHolder extends RecyclerView.ViewHolder{

        private TextView mNameDevices,clickOnDevices,clickOffDevices;
        private ImageView imgDevices;
        private ImageView imgFavoriteDevices;

        public DeviceLightHolder(@NonNull View itemView) {
            super(itemView);

            mNameDevices =  itemView.findViewById(R.id.TextView_DevicesName);
            imgDevices = itemView.findViewById(R.id.ImageView_ItemsDevices);
            imgFavoriteDevices = itemView.findViewById(R.id.img_favoriteDevices);
            clickOnDevices = itemView.findViewById(R.id.onClickOnDevices);
            clickOffDevices = itemView.findViewById(R.id.onClickOffDevices);

        }
    }

    public class DeviceThermometerHolder extends RecyclerView.ViewHolder{

        private ImageView imgThermometer;
        private TextView mNameThermometer,itemThermometer;
        private ImageView imgFavoriteThermometer;

        public DeviceThermometerHolder(@NonNull View itemView) {
            super(itemView);
            imgThermometer = itemView.findViewById(R.id.img_items_thermometer);
            mNameThermometer = itemView.findViewById(R.id.txt_name_thermometer);
            itemThermometer = itemView.findViewById(R.id.txt_items_thermometer);
            imgFavoriteThermometer = itemView.findViewById(R.id.img_favorite_thermometer);
        }
    }

    public class DeviceMagneticDoorHolder extends RecyclerView.ViewHolder{

        private ImageView imgMagneticDoor;
        private TextView mNameMagneticDoor,onClickOpenMagneticDoor,onClickCloseMagneticDoor;
        private ImageView imgFavoriteMagneticDoor;

        public DeviceMagneticDoorHolder(@NonNull View itemView) {
            super(itemView);
            imgMagneticDoor = itemView.findViewById(R.id.img_MagneticDoor);
            mNameMagneticDoor = itemView.findViewById(R.id.txt_name_MagneticDoor);
            onClickOpenMagneticDoor = itemView.findViewById(R.id.onClickOpenMagneticDoor);
            onClickCloseMagneticDoor = itemView.findViewById(R.id.onClickCloseMagneticDoor);
            imgFavoriteMagneticDoor = itemView.findViewById(R.id.img_favorite_magnetic_door);
        }
    }

    public class DeviceAirConditionerHolder extends RecyclerView.ViewHolder{
        private TextView mNameDevices,clickOnDevices,clickOffDevices;
        private ImageView imgDevices;
        private ImageView imgFavoriteDevices;
        public DeviceAirConditionerHolder(@NonNull View itemView) {
            super(itemView);
            mNameDevices =  itemView.findViewById(R.id.TextView_DevicesName);
            imgDevices = itemView.findViewById(R.id.ImageView_ItemsDevices);
            imgFavoriteDevices = itemView.findViewById(R.id.img_favoriteDevices);
            clickOnDevices = itemView.findViewById(R.id.onClickOnDevices);
            clickOffDevices = itemView.findViewById(R.id.onClickOffDevices);
        }
    }

    public class DeviceTelevisionHolder extends RecyclerView.ViewHolder{
        private TextView mNameDevices,clickOnDevices,clickOffDevices;
        private ImageView imgDevices;
        private ImageView imgFavoriteDevices;
        public DeviceTelevisionHolder(@NonNull View itemView) {
            super(itemView);
            mNameDevices =  itemView.findViewById(R.id.TextView_DevicesName);
            imgDevices = itemView.findViewById(R.id.ImageView_ItemsDevices);
            imgFavoriteDevices = itemView.findViewById(R.id.img_favoriteDevices);
            clickOnDevices = itemView.findViewById(R.id.onClickOnDevices);
            clickOffDevices = itemView.findViewById(R.id.onClickOffDevices);
        }
    }

    public class DeviceCCTVHolder extends RecyclerView.ViewHolder{
        private TextView mNameDevices,clickOnDevices,clickOffDevices;
        private ImageView imgDevices;
        private ImageView imgFavoriteDevices;
        public DeviceCCTVHolder(@NonNull View itemView) {
            super(itemView);
            mNameDevices =  itemView.findViewById(R.id.TextView_DevicesName);
            imgDevices = itemView.findViewById(R.id.ImageView_ItemsDevices);
            imgFavoriteDevices = itemView.findViewById(R.id.img_favoriteDevices);
            clickOnDevices = itemView.findViewById(R.id.onClickOnDevices);
            clickOffDevices = itemView.findViewById(R.id.onClickOffDevices);
        }
    }

    public class DeviceMagneticSensorHolder extends RecyclerView.ViewHolder{
        private TextView mNameDevices,clickOnDevices,clickOffDevices;
        private ImageView imgDevices;
        private ImageView imgFavoriteDevices;
        public DeviceMagneticSensorHolder(@NonNull View itemView) {
            super(itemView);
            mNameDevices =  itemView.findViewById(R.id.TextView_DevicesName);
            imgDevices = itemView.findViewById(R.id.ImageView_ItemsDevices);
            imgFavoriteDevices = itemView.findViewById(R.id.img_favoriteDevices);
            clickOnDevices = itemView.findViewById(R.id.onClickOnDevices);
            clickOffDevices = itemView.findViewById(R.id.onClickOffDevices);
        }
    }

    public class DeviceFireAlarmHolder extends RecyclerView.ViewHolder{
        private TextView mNameDevices,clickOnDevices,clickOffDevices;
        private ImageView imgDevices;
        private ImageView imgFavoriteDevices;
        public DeviceFireAlarmHolder(@NonNull View itemView) {
            super(itemView);
            mNameDevices =  itemView.findViewById(R.id.TextView_DevicesName);
            imgDevices = itemView.findViewById(R.id.ImageView_ItemsDevices);
            imgFavoriteDevices = itemView.findViewById(R.id.img_favoriteDevices);
            clickOnDevices = itemView.findViewById(R.id.onClickOnDevices);
            clickOffDevices = itemView.findViewById(R.id.onClickOffDevices);
        }
    }

    public class DeviceAlertHolder extends RecyclerView.ViewHolder{
        private TextView mNameDevices,clickOnDevices,clickOffDevices;
        private ImageView imgDevices;
        private ImageView imgFavoriteDevices;
        public DeviceAlertHolder(@NonNull View itemView) {
            super(itemView);
            mNameDevices =  itemView.findViewById(R.id.TextView_DevicesName);
            imgDevices = itemView.findViewById(R.id.ImageView_ItemsDevices);
            imgFavoriteDevices = itemView.findViewById(R.id.img_favoriteDevices);
            clickOnDevices = itemView.findViewById(R.id.onClickOnDevices);
            clickOffDevices = itemView.findViewById(R.id.onClickOffDevices);
        }
    }

    public class DeviceFrontSlidingDoorHolder extends RecyclerView.ViewHolder{
        private ImageView imgFrontSlidingDoorDoor;
        private TextView mNameFrontSlidingDoor,onClickOpenFrontSlidingDoor,onClickCloseFrontSlidingDoor;
        private ImageView imgFavoriteFrontSlidingDoor;
        public DeviceFrontSlidingDoorHolder(@NonNull View itemView) {
            super(itemView);
            imgFrontSlidingDoorDoor = itemView.findViewById(R.id.img_MagneticDoor);
            mNameFrontSlidingDoor = itemView.findViewById(R.id.txt_name_MagneticDoor);
            onClickOpenFrontSlidingDoor = itemView.findViewById(R.id.onClickOpenMagneticDoor);
            onClickCloseFrontSlidingDoor = itemView.findViewById(R.id.onClickCloseMagneticDoor);
            imgFavoriteFrontSlidingDoor = itemView.findViewById(R.id.img_favorite_magnetic_door);
        }
    }

    public class DeviceElectricMeterHolder extends RecyclerView.ViewHolder{
        private TextView mNameDevices,clickOnDevices,clickOffDevices;
        private ImageView imgDevices;
        private ImageView imgFavoriteDevices;
        public DeviceElectricMeterHolder(@NonNull View itemView) {
            super(itemView);
            mNameDevices =  itemView.findViewById(R.id.TextView_DevicesName);
            imgDevices = itemView.findViewById(R.id.ImageView_ItemsDevices);
            imgFavoriteDevices = itemView.findViewById(R.id.img_favoriteDevices);
            clickOnDevices = itemView.findViewById(R.id.onClickOnDevices);
            clickOffDevices = itemView.findViewById(R.id.onClickOffDevices);
        }
    }

    private static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }

    private static int getResourceColorByCode(Context context, int codecolor){
        return context.getResources().getColor(codecolor);
    }

}
