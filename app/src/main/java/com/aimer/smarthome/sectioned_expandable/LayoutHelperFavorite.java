package com.aimer.smarthome.sectioned_expandable;

import android.content.Context;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.utils.ItemClickListener;
import com.aimer.smarthome.utils.ItemClickListenerOnClickDevices;
import com.aimer.smarthome.utils.SectionStateChangeListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class LayoutHelperFavorite implements SectionStateChangeListener{
    //data list
    private LinkedHashMap<ModelBoard, ArrayList<Object>> mSectionDataMap = new LinkedHashMap<ModelBoard, ArrayList<Object>>();
    private ArrayList<Object> mDataArrayList = new ArrayList<Object>();


    //section map
    private HashMap<String, ModelBoard> mSectionMap = new HashMap<String, ModelBoard>();

    //adapter
    private SectionedFavoriteAdapter mSectionedFavoriteAdapter;

    //recycler view
    private RecyclerView mRecyclerView;

    public LayoutHelperFavorite(Context context, RecyclerView recyclerView, int gridSpanCount, ItemClickListener itemClickListener, ItemClickListenerOnClickDevices mItemClickListenerOnClickDevices){
        //setting the recycler view
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, gridSpanCount);
        recyclerView.setLayoutManager(gridLayoutManager);
        mSectionedFavoriteAdapter = new SectionedFavoriteAdapter(context, mDataArrayList,gridLayoutManager,this,itemClickListener,mItemClickListenerOnClickDevices);
        recyclerView.setAdapter(mSectionedFavoriteAdapter);

        mRecyclerView = recyclerView;
    }

    public void addSection(int board_id,String board_MacAddress,String board_name, ArrayList<Object> devices) {
        ModelBoard newBoard;
        mSectionMap.put(board_name, (newBoard = new ModelBoard(board_id,board_MacAddress,board_name)));
        mSectionDataMap.put(newBoard, devices);
    }

    public void getFilterFavorite(CharSequence constraint){
        mSectionedFavoriteAdapter.getFilter().filter(constraint);
    }

    public void addItem(String board, Object devices) {
        mSectionDataMap.get(mSectionMap.get(board)).add(devices);
    }

    public void removeItem(String board, Object devices) {
        mSectionDataMap.get(mSectionMap.get(board)).remove(devices);
    }

    public void removeSection(String board) {
        mSectionDataMap.remove(mSectionMap.get(board));
        mSectionMap.remove(board);
    }

    public void clearAllSection(){
        mSectionDataMap.clear();
    }


    public void notifyDataSetChanged() {
        generateDataList();
        mSectionedFavoriteAdapter.notifyDataSetChanged();
    }

    private void generateDataList () {
        mDataArrayList.clear();
        for (Map.Entry<ModelBoard, ArrayList<Object>> entry : mSectionDataMap.entrySet()) {
            ModelBoard key;
            mDataArrayList.add((key = entry.getKey()));
            if (key.isExpanded)
                mDataArrayList.addAll(entry.getValue());
        }
    }

    @Override
    public void onSectionStateChanged(ModelBoard modelBoard, boolean isOpen) {
        if (!mRecyclerView.isComputingLayout()) {
            modelBoard.isExpanded = isOpen;
            notifyDataSetChanged();
        }
    }


}
