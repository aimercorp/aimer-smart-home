package com.aimer.smarthome.sectioned_expandable;

import android.content.Context;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.utils.ItemClickListener;
import com.aimer.smarthome.utils.ItemClickListenerOnClickDevices;
import com.aimer.smarthome.utils.SectionStateChangeListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
public class LayoutHelperBoard implements SectionStateChangeListener{
    //data list
    private LinkedHashMap<ModelBoard, ArrayList<Object>> mSectionDataMap = new LinkedHashMap<ModelBoard, ArrayList<Object>>();
    private ArrayList<Object> mDataArrayList = new ArrayList<Object>();


    //section map
    private HashMap<String, ModelBoard> mSectionMap = new HashMap<String, ModelBoard>();

    //adapter
    private SectionedBoardAdapter mSectionedBoardAdapter;

    //recycler view
    private RecyclerView mRecyclerView;

    public LayoutHelperBoard(Context context, RecyclerView recyclerView, int gridSpanCount, ItemClickListener itemClickListener, ItemClickListenerOnClickDevices mItemClickListenerOnClickDevices){
        //setting the recycler view
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, gridSpanCount);
        recyclerView.setLayoutManager(gridLayoutManager);
        mSectionedBoardAdapter = new SectionedBoardAdapter(context, mDataArrayList,gridLayoutManager,this,itemClickListener,mItemClickListenerOnClickDevices);
        recyclerView.setAdapter(mSectionedBoardAdapter);

        mRecyclerView = recyclerView;
    }

    public void addSection(int board_id,String board_MacAddress,String board_name, ArrayList<Object> devices) {
        ModelBoard newBoard;
        mSectionMap.put(board_name, (newBoard = new ModelBoard(board_id,board_MacAddress,board_name)));
        mSectionDataMap.put(newBoard, devices);
    }

    public void setFilter(CharSequence sequence){
        mSectionedBoardAdapter.getFilter().filter(sequence);
    }

    public void addItem(String board, Object devices) {
        mSectionDataMap.get(mSectionMap.get(board)).add(devices);
    }

    public void removeItem(String board, Object devices) {
        mSectionDataMap.get(mSectionMap.get(board)).remove(devices);
    }

    public void removeSection(String board) {
        mSectionDataMap.remove(mSectionMap.get(board));
        mSectionMap.remove(board);
    }

    public void clearAllSection(){
        mSectionDataMap.clear();
    }


    public void notifyDataSetChanged() {
        generateDataList();
        mSectionedBoardAdapter.notifyDataSetChanged();
    }

    private void generateDataList () {
        mDataArrayList.clear();
        for (Map.Entry<ModelBoard, ArrayList<Object>> entry : mSectionDataMap.entrySet()) {
            ModelBoard key;
            mDataArrayList.add((key = entry.getKey()));
            if (key.isExpanded)
                mDataArrayList.addAll(entry.getValue());
        }
    }

    @Override
    public void onSectionStateChanged(ModelBoard modelBoard, boolean isOpen) {
        if (!mRecyclerView.isComputingLayout()) {
            modelBoard.isExpanded = isOpen;
            notifyDataSetChanged();
        }
    }


}
