package com.aimer.smarthome.utils;

import com.aimer.smarthome.model.ModelTypeDevices;

public interface ItemClickListenerSpinnerFilter {
    void itemClicked(ModelTypeDevices typeDevices);
}
