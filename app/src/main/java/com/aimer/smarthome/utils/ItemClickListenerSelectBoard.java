package com.aimer.smarthome.utils;


public interface ItemClickListenerSelectBoard {
    void itemClicked(int position);
}
