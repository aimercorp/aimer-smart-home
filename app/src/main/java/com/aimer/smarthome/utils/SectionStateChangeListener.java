package com.aimer.smarthome.utils;

import com.aimer.smarthome.model.ModelBoard;

public interface SectionStateChangeListener {
    void onSectionStateChanged(ModelBoard modelBoard, boolean isOpen);
}
