package com.aimer.smarthome.utils;

import com.aimer.smarthome.model.ModelDevices;

public interface ItemClickListenerSelectChannel {
    void itemClicked(ModelDevices modelDevices);
}
