package com.aimer.smarthome.utils;

import com.aimer.smarthome.model.ModelAccounts;


public interface ItemClickListenerJoinFamily {
    void itemClicked(ModelAccounts modelAccounts);
}
