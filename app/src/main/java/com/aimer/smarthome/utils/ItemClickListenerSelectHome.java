package com.aimer.smarthome.utils;

import com.aimer.smarthome.model.ModelHome;

public interface ItemClickListenerSelectHome {
    void itemClicked(ModelHome home, int position);
}
