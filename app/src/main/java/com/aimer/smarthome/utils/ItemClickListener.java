package com.aimer.smarthome.utils;

import android.widget.ImageView;
import android.widget.TextView;

import com.aimer.smarthome.model.DeviceLight;
import com.aimer.smarthome.model.DeviceMagneticDoor;
import com.aimer.smarthome.model.DeviceThermometer;
import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.model.ModelDevices;

public interface ItemClickListener {
    void itemClicked(ModelBoard modelBoard);
    void itemClicked(ModelDevices modelDevices);
    void itemClickedThermometer(DeviceThermometer thermometer);
    void itemClickedMagneticDoor(DeviceMagneticDoor magneticDoor);
    void itemClickedFavourite(ModelDevices modelDevices, ImageView imgFavourite);
    void setTemp(final TextView temp);
}
