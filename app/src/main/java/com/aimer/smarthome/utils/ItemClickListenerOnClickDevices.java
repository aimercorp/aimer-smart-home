package com.aimer.smarthome.utils;

import com.aimer.smarthome.model.ModelDevices;

public interface ItemClickListenerOnClickDevices {
    void itemClickedOnDevices(ModelDevices modelDevices);
    void itemClickedOffDevices(ModelDevices modelDevices);

}
