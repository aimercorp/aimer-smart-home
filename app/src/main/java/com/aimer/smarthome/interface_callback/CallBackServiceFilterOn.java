package com.aimer.smarthome.interface_callback;

public interface CallBackServiceFilterOn {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(boolean result);
    void onRequestFailed(String result);
}
