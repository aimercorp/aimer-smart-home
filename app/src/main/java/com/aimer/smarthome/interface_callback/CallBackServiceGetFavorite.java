package com.aimer.smarthome.interface_callback;

import com.aimer.smarthome.model.ModelBoard;

import java.util.ArrayList;

public interface CallBackServiceGetFavorite {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerListDevices(ArrayList<ModelBoard> listBoard);
    void onRequestFailed(String result);
}
