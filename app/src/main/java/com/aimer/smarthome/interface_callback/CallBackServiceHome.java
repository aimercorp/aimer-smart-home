package com.aimer.smarthome.interface_callback;

import com.aimer.smarthome.model.ModelHome;

import java.util.ArrayList;

public interface CallBackServiceHome {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelHome> modelHomes);
    void onRequestFailed(String result);
}
