package com.aimer.smarthome.interface_callback;

import com.aimer.smarthome.model.ModelAccounts;

public interface CallBackServiceAccount {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ModelAccounts accounts);
    void onRequestFailed(String result);
}
