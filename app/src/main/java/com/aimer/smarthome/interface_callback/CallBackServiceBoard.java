package com.aimer.smarthome.interface_callback;

import com.aimer.smarthome.model.ModelBoard;

import java.util.ArrayList;

public interface CallBackServiceBoard {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelBoard> listBoard);
    void onRequestFailed(String result);
}
