package com.aimer.smarthome.interface_callback;

import com.aimer.smarthome.model.ModelTypeDevices;

import java.util.ArrayList;

public interface CallBackServiceTypeDevices {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerModelTypeDevices(ArrayList<ModelTypeDevices> modelTypeDevices);
    void onRequestFailed(String result);
}
