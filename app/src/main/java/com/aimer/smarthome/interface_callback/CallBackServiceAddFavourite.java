package com.aimer.smarthome.interface_callback;

import java.util.ArrayList;

public interface CallBackServiceAddFavourite {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerStatusFavorite(ArrayList<Object> status);
    void onRequestFailed(String result);
}
