package com.aimer.smarthome.interface_callback;

import com.aimer.smarthome.model.ModelAccounts;

public interface CallBackServiceInviteFamily {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteModelAccounts(ModelAccounts accounts);
    void onRequestFailed(String result);
}
