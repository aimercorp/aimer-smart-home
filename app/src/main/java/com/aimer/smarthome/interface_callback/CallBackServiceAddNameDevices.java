package com.aimer.smarthome.interface_callback;

public interface CallBackServiceAddNameDevices {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(String result);
    void onRequestFailed(String result);
}
