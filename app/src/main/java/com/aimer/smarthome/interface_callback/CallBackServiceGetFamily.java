package com.aimer.smarthome.interface_callback;

import com.aimer.smarthome.model.ModelAccounts;

import java.util.ArrayList;

public interface CallBackServiceGetFamily {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerFamily(ArrayList<ModelAccounts> listAccounts);
    void onRequestFailed(String result);
}
