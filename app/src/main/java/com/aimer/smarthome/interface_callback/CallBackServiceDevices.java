package com.aimer.smarthome.interface_callback;

import com.aimer.smarthome.model.ModelBoard;

import java.util.ArrayList;

public interface CallBackServiceDevices {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerListDevices(ArrayList<ModelBoard> listBoard);
    void onRequestFailed(String result);
}
