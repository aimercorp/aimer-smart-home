package com.aimer.smarthome.interface_callback;

import com.aimer.smarthome.model.ModelAccounts;

import java.util.ArrayList;

public interface CallBackServiceInvited {

    void onPreCallService();
    void onCallService();
    void onRequestCompleteListenerInvited(ArrayList<ModelAccounts> listAccounts);
    void onRequestFailed(String result);
}
