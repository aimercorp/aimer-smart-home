package com.aimer.smarthome.interface_callback;

import com.aimer.smarthome.model.ModelWeather;

public interface CallBackServiceWeather {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ModelWeather weather);
    void onRequestFailed(String result);
}
