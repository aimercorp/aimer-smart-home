package com.aimer.smarthome.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ModelHome implements Parcelable {
    private int homeId;
    private String homeName;
    private int isAccept;
    private ArrayList<ModelAccounts> modelAccounts;
    private ArrayList<ModelBoard> modelBoards;

    public ModelHome() {

    }

    public int getHomeId() {
        return homeId;
    }

    public void setHomeId(int homeId) {
        this.homeId = homeId;
    }

    public String getHomeName() {
        return homeName;
    }

    public void setHomeName(String homeName) {
        this.homeName = homeName;
    }

    public int getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(int isAccept) {
        this.isAccept = isAccept;
    }

    public ArrayList<ModelAccounts> getModelAccounts() {
        return modelAccounts;
    }

    public void setModelAccounts(ArrayList<ModelAccounts> modelAccounts) {
        this.modelAccounts = modelAccounts;
    }

    public ArrayList<ModelBoard> getModelBoards() {
        return modelBoards;
    }

    public void setModelBoards(ArrayList<ModelBoard> modelBoards) {
        this.modelBoards = modelBoards;
    }

    protected ModelHome(Parcel in) {
        homeId = in.readInt();
        homeName = in.readString();
        isAccept = in.readInt();
        modelAccounts = in.createTypedArrayList(ModelAccounts.CREATOR);
        modelBoards = in.createTypedArrayList(ModelBoard.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(homeId);
        dest.writeString(homeName);
        dest.writeInt(isAccept);
        dest.writeTypedList(modelAccounts);
        dest.writeTypedList(modelBoards);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelHome> CREATOR = new Creator<ModelHome>() {
        @Override
        public ModelHome createFromParcel(Parcel in) {
            return new ModelHome(in);
        }

        @Override
        public ModelHome[] newArray(int size) {
            return new ModelHome[size];
        }
    };
}
