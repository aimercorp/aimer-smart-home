package com.aimer.smarthome.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ModelPin implements Parcelable {
    private int pinId;
    private int pin;
    private String typePin;
    private ArrayList<ModelBoard> modelBoards;

    public ModelPin() {

    }

    public int getPinId() {
        return pinId;
    }

    public void setPinId(int pinId) {
        this.pinId = pinId;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public String getTypePin() {
        return typePin;
    }

    public void setTypePin(String typePin) {
        this.typePin = typePin;
    }

    public ArrayList<ModelBoard> getModelBoards() {
        return modelBoards;
    }

    public void setModelBoards(ArrayList<ModelBoard> modelBoards) {
        this.modelBoards = modelBoards;
    }

    protected ModelPin(Parcel in) {
        pinId = in.readInt();
        pin = in.readInt();
        typePin = in.readString();
        modelBoards = in.createTypedArrayList(ModelBoard.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(pinId);
        dest.writeInt(pin);
        dest.writeString(typePin);
        dest.writeTypedList(modelBoards);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelPin> CREATOR = new Creator<ModelPin>() {
        @Override
        public ModelPin createFromParcel(Parcel in) {
            return new ModelPin(in);
        }

        @Override
        public ModelPin[] newArray(int size) {
            return new ModelPin[size];
        }
    };
}
