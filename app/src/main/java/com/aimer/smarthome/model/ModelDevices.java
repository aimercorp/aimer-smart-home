package com.aimer.smarthome.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelDevices implements Parcelable {
    private int devicesId;
    private String devicesName;
    private int devicesIsActive;
    private String devicesStatus;
    private String devicesIcon;
    private boolean favouritesStatus;
    private ModelPin modelPin;
    private ModelBoard modelBoard;
    private ModelTypeDevices modelTypeDevices;

    public ModelDevices() {
    }

    public int getDevicesId() {
        return devicesId;
    }

    public void setDevicesId(int devicesId) {
        this.devicesId = devicesId;
    }

    public String getDevicesName() {
        return devicesName;
    }

    public void setDevicesName(String devicesName) {
        this.devicesName = devicesName;
    }

    public int getDevicesIsActive() {
        return devicesIsActive;
    }

    public void setDevicesIsActive(int devicesIsActive) {
        this.devicesIsActive = devicesIsActive;
    }

    public String getDevicesStatus() {
        return devicesStatus;
    }

    public void setDevicesStatus(String devicesStatus) {
        this.devicesStatus = devicesStatus;
    }

    public String getDevicesIcon() {
        return devicesIcon;
    }

    public void setDevicesIcon(String devicesIcon) {
        this.devicesIcon = devicesIcon;
    }

    public boolean isFavouritesStatus() {
        return favouritesStatus;
    }

    public void setFavouritesStatus(boolean favouritesStatus) {
        this.favouritesStatus = favouritesStatus;
    }

    public ModelPin getModelPin() {
        return modelPin;
    }

    public void setModelPin(ModelPin modelPin) {
        this.modelPin = modelPin;
    }

    public ModelBoard getModelBoard() {
        return modelBoard;
    }

    public void setModelBoard(ModelBoard modelBoard) {
        this.modelBoard = modelBoard;
    }

    public ModelTypeDevices getModelTypeDevices() {
        return modelTypeDevices;
    }

    public void setModelTypeDevices(ModelTypeDevices modelTypeDevices) {
        this.modelTypeDevices = modelTypeDevices;
    }

    protected ModelDevices(Parcel in) {
        devicesId = in.readInt();
        devicesName = in.readString();
        devicesIsActive = in.readInt();
        devicesStatus = in.readString();
        devicesIcon = in.readString();
        favouritesStatus = in.readByte() != 0;
        modelPin = in.readParcelable(ModelPin.class.getClassLoader());
        modelBoard = in.readParcelable(ModelBoard.class.getClassLoader());
        modelTypeDevices = in.readParcelable(ModelTypeDevices.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(devicesId);
        dest.writeString(devicesName);
        dest.writeInt(devicesIsActive);
        dest.writeString(devicesStatus);
        dest.writeString(devicesIcon);
        dest.writeByte((byte) (favouritesStatus ? 1 : 0));
        dest.writeParcelable(modelPin, flags);
        dest.writeParcelable(modelBoard, flags);
        dest.writeParcelable(modelTypeDevices, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelDevices> CREATOR = new Creator<ModelDevices>() {
        @Override
        public ModelDevices createFromParcel(Parcel in) {
            return new ModelDevices(in);
        }

        @Override
        public ModelDevices[] newArray(int size) {
            return new ModelDevices[size];
        }
    };
}
