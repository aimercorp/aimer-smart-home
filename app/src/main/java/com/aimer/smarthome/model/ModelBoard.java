package com.aimer.smarthome.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ModelBoard implements Parcelable {
    private int boardId;
    private String boardMacAddress;
    private String boardName;
    private ModelTypeBoard modelTypeBoard;
    private int type_sizeInPut;
    private int type_sizeOutPut;
    private String type_size;
    private ArrayList<ModelDevices> modelDevices;
    public boolean isExpanded = false;

    public ModelBoard() {

    }

    public ModelBoard(int boardId,String boardMacAddress,String boardName){
        this.boardId = boardId;
        this.boardMacAddress = boardMacAddress;
        this.boardName = boardName;
        isExpanded = true;
    }

    public int getBoardId() {
        return boardId;
    }

    public void setBoardId(int boardId) {
        this.boardId = boardId;
    }

    public String getBoardMacAddress() {
        return boardMacAddress;
    }

    public void setBoardMacAddress(String boardMacAddress) {
        this.boardMacAddress = boardMacAddress;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public ModelTypeBoard getModelTypeBoard() {
        return modelTypeBoard;
    }

    public void setModelTypeBoard(ModelTypeBoard modelTypeBoard) {
        this.modelTypeBoard = modelTypeBoard;
    }

    public int getType_sizeInPut() {
        return type_sizeInPut;
    }

    public void setType_sizeInPut(int type_sizeInPut) {
        this.type_sizeInPut = type_sizeInPut;
    }

    public int getType_sizeOutPut() {
        return type_sizeOutPut;
    }

    public void setType_sizeOutPut(int type_sizeOutPut) {
        this.type_sizeOutPut = type_sizeOutPut;
    }

    public String getType_size() {
        return type_size;
    }

    public void setType_size(String type_size) {
        this.type_size = type_size;
    }

    public ArrayList<ModelDevices> getModelDevices() {
        return modelDevices;
    }

    public void setModelDevices(ArrayList<ModelDevices> modelDevices) {
        this.modelDevices = modelDevices;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    protected ModelBoard(Parcel in) {
        boardId = in.readInt();
        boardMacAddress = in.readString();
        boardName = in.readString();
        modelTypeBoard = in.readParcelable(ModelTypeBoard.class.getClassLoader());
        type_sizeInPut = in.readInt();
        type_sizeOutPut = in.readInt();
        type_size = in.readString();
        modelDevices = in.createTypedArrayList(ModelDevices.CREATOR);
        isExpanded = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(boardId);
        dest.writeString(boardMacAddress);
        dest.writeString(boardName);
        dest.writeParcelable(modelTypeBoard, flags);
        dest.writeInt(type_sizeInPut);
        dest.writeInt(type_sizeOutPut);
        dest.writeString(type_size);
        dest.writeTypedList(modelDevices);
        dest.writeByte((byte) (isExpanded ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelBoard> CREATOR = new Creator<ModelBoard>() {
        @Override
        public ModelBoard createFromParcel(Parcel in) {
            return new ModelBoard(in);
        }

        @Override
        public ModelBoard[] newArray(int size) {
            return new ModelBoard[size];
        }
    };
}
