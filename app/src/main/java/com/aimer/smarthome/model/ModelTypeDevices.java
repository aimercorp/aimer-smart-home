package com.aimer.smarthome.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelTypeDevices implements Parcelable {
    private int typeDevicesId;
    private String typeDevices;

    public ModelTypeDevices() {
    }

    public ModelTypeDevices(int typeDevicesId, String typeDevices) {
        this.typeDevicesId = typeDevicesId;
        this.typeDevices = typeDevices;
    }

    public int getTypeDevicesId() {
        return typeDevicesId;
    }

    public void setTypeDevicesId(int typeDevicesId) {
        this.typeDevicesId = typeDevicesId;
    }

    public String getTypeDevices() {
        return typeDevices;
    }

    public void setTypeDevices(String typeDevices) {
        this.typeDevices = typeDevices;
    }

    protected ModelTypeDevices(Parcel in) {
        typeDevicesId = in.readInt();
        typeDevices = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(typeDevicesId);
        dest.writeString(typeDevices);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelTypeDevices> CREATOR = new Creator<ModelTypeDevices>() {
        @Override
        public ModelTypeDevices createFromParcel(Parcel in) {
            return new ModelTypeDevices(in);
        }

        @Override
        public ModelTypeDevices[] newArray(int size) {
            return new ModelTypeDevices[size];
        }
    };
}
