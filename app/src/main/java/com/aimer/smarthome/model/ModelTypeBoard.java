package com.aimer.smarthome.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelTypeBoard implements Parcelable {
    private int typeBoardId;
    private int typeSizeInput;
    private int typeSizeOutput;
    private String size;

    public ModelTypeBoard() {

    }

    protected ModelTypeBoard(Parcel in) {
        typeBoardId = in.readInt();
        typeSizeInput = in.readInt();
        typeSizeOutput = in.readInt();
        size = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(typeBoardId);
        dest.writeInt(typeSizeInput);
        dest.writeInt(typeSizeOutput);
        dest.writeString(size);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelTypeBoard> CREATOR = new Creator<ModelTypeBoard>() {
        @Override
        public ModelTypeBoard createFromParcel(Parcel in) {
            return new ModelTypeBoard(in);
        }

        @Override
        public ModelTypeBoard[] newArray(int size) {
            return new ModelTypeBoard[size];
        }
    };

    public int getTypeBoardId() {
        return typeBoardId;
    }

    public void setTypeBoardId(int typeBoardId) {
        this.typeBoardId = typeBoardId;
    }

    public int getTypeSizeInput() {
        return typeSizeInput;
    }

    public void setTypeSizeInput(int typeSizeInput) {
        this.typeSizeInput = typeSizeInput;
    }

    public int getTypeSizeOutput() {
        return typeSizeOutput;
    }

    public void setTypeSizeOutput(int typeSizeOutput) {
        this.typeSizeOutput = typeSizeOutput;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
