package com.aimer.smarthome.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelWeather implements Parcelable {
    private String icon;
    private String address;
    private String updated_at;
    private String status;
    private String temp;
    private String tempMin;
    private String tempMax;
    private String sunrise;
    private String sunset;
    private String windSpeed;
    private String pressure;
    private String humidity;
    private String description;

    public ModelWeather() {

    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTempMin() {
        return tempMin;
    }

    public void setTempMin(String tempMin) {
        this.tempMin = tempMin;
    }

    public String getTempMax() {
        return tempMax;
    }

    public void setTempMax(String tempMax) {
        this.tempMax = tempMax;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    protected ModelWeather(Parcel in) {
        icon = in.readString();
        address = in.readString();
        updated_at = in.readString();
        status = in.readString();
        temp = in.readString();
        tempMin = in.readString();
        tempMax = in.readString();
        sunrise = in.readString();
        sunset = in.readString();
        windSpeed = in.readString();
        pressure = in.readString();
        humidity = in.readString();
        description = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(icon);
        dest.writeString(address);
        dest.writeString(updated_at);
        dest.writeString(status);
        dest.writeString(temp);
        dest.writeString(tempMin);
        dest.writeString(tempMax);
        dest.writeString(sunrise);
        dest.writeString(sunset);
        dest.writeString(windSpeed);
        dest.writeString(pressure);
        dest.writeString(humidity);
        dest.writeString(description);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelWeather> CREATOR = new Creator<ModelWeather>() {
        @Override
        public ModelWeather createFromParcel(Parcel in) {
            return new ModelWeather(in);
        }

        @Override
        public ModelWeather[] newArray(int size) {
            return new ModelWeather[size];
        }
    };
}
