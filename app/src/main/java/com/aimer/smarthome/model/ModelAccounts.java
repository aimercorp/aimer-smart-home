package com.aimer.smarthome.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ModelAccounts implements Parcelable {
    private int accountsId;
    private String accountsKey;
    private String accountsEmail;
    private String accountsPassword;
    private String accountsPin;
    private String accountsToken;
    private String accountsName;
    private String accountsPhoto;
    private int AccountsIsActive;
    private ArrayList<ModelHome> modelHomeArrayList;
    private ModelHome modelHome;

    public ModelAccounts() {

    }

    public int getAccountsId() {
        return accountsId;
    }

    public void setAccountsId(int accountsId) {
        this.accountsId = accountsId;
    }

    public String getAccountsKey() {
        return accountsKey;
    }

    public void setAccountsKey(String accountsKey) {
        this.accountsKey = accountsKey;
    }

    public String getAccountsEmail() {
        return accountsEmail;
    }

    public void setAccountsEmail(String accountsEmail) {
        this.accountsEmail = accountsEmail;
    }

    public String getAccountsPassword() {
        return accountsPassword;
    }

    public void setAccountsPassword(String accountsPassword) {
        this.accountsPassword = accountsPassword;
    }

    public String getAccountsPin() {
        return accountsPin;
    }

    public void setAccountsPin(String accountsPin) {
        this.accountsPin = accountsPin;
    }

    public String getAccountsToken() {
        return accountsToken;
    }

    public void setAccountsToken(String accountsToken) {
        this.accountsToken = accountsToken;
    }

    public String getAccountsName() {
        return accountsName;
    }

    public void setAccountsName(String accountsName) {
        this.accountsName = accountsName;
    }

    public String getAccountsPhoto() {
        return accountsPhoto;
    }

    public void setAccountsPhoto(String accountsPhoto) {
        this.accountsPhoto = accountsPhoto;
    }

    public int getAccountsIsActive() {
        return AccountsIsActive;
    }

    public void setAccountsIsActive(int accountsIsActive) {
        AccountsIsActive = accountsIsActive;
    }

    public ArrayList<ModelHome> getModelHomeArrayList() {
        return modelHomeArrayList;
    }

    public void setModelHomeArrayList(ArrayList<ModelHome> modelHomeArrayList) {
        this.modelHomeArrayList = modelHomeArrayList;
    }

    public ModelHome getModelHome() {
        return modelHome;
    }

    public void setModelHome(ModelHome modelHome) {
        this.modelHome = modelHome;
    }

    protected ModelAccounts(Parcel in) {
        accountsId = in.readInt();
        accountsKey = in.readString();
        accountsEmail = in.readString();
        accountsPassword = in.readString();
        accountsPin = in.readString();
        accountsToken = in.readString();
        accountsName = in.readString();
        accountsPhoto = in.readString();
        AccountsIsActive = in.readInt();
        modelHomeArrayList = in.createTypedArrayList(ModelHome.CREATOR);
        modelHome = in.readParcelable(ModelHome.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(accountsId);
        dest.writeString(accountsKey);
        dest.writeString(accountsEmail);
        dest.writeString(accountsPassword);
        dest.writeString(accountsPin);
        dest.writeString(accountsToken);
        dest.writeString(accountsName);
        dest.writeString(accountsPhoto);
        dest.writeInt(AccountsIsActive);
        dest.writeTypedList(modelHomeArrayList);
        dest.writeParcelable(modelHome, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelAccounts> CREATOR = new Creator<ModelAccounts>() {
        @Override
        public ModelAccounts createFromParcel(Parcel in) {
            return new ModelAccounts(in);
        }

        @Override
        public ModelAccounts[] newArray(int size) {
            return new ModelAccounts[size];
        }
    };
}
