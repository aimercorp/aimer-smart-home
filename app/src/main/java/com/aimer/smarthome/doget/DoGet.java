package com.aimer.smarthome.doget;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.aimer.smarthome.activity.NewControllerActivity;
import com.aimer.smarthome.activity.NewHomeActivity;
import com.aimer.smarthome.activity.SignInActivity;
import com.aimer.smarthome.fragment.FavoritesFragment;
import com.aimer.smarthome.fragment.HomeFragment;
import com.aimer.smarthome.fragment_filter.FilterALLFragment;
import com.aimer.smarthome.fragment_filter.FilterFavoriteFragment;
import com.aimer.smarthome.fragment_filter.FilterOffFragment;
import com.aimer.smarthome.fragment_filter.FilterOnFragment;
import com.aimer.smarthome.fragment_popup.DeleteHomeFragment;
import com.aimer.smarthome.fragment_popup.JoinFamilyFragment;
import com.aimer.smarthome.fragment_popup.PowerPushFragment;
import com.aimer.smarthome.fragment_popup.RenameHomeFragment;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DoGet extends AsyncTask<String,Integer,String> {

    public DoGet(SignInActivity context){

    }
    public DoGet(NewControllerActivity.EsptouchAsyncTask4 context){

    }

    public DoGet(HomeFragment homeFragment) {

    }

    public DoGet(DeleteHomeFragment deleteHomeFragment) {

    }

    public DoGet(RenameHomeFragment renameHomeFragment) {

    }

    public DoGet(FilterALLFragment filterALLFragment) {

    }

    public DoGet(FilterOnFragment filterOnFragment) {

    }

    public DoGet(FilterOffFragment filterOffFragment) {

    }

    public DoGet(FilterFavoriteFragment filterFavoriteFragment) {
    }

    public DoGet(PowerPushFragment powerPushFragment) {

    }

    public DoGet(FavoritesFragment favoritesFragment) {

    }

    public DoGet(JoinFamilyFragment joinFamilyFragment) {

    }


    private String doGet(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d("Doget : ", "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            return null;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            return doGet(strings[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
