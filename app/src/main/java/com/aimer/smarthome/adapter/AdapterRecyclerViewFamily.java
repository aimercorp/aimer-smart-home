package com.aimer.smarthome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.model.ModelAccounts;
import com.aimer.smarthome.utils.ItemClickListenerJoinFamily;

import java.util.ArrayList;

public class AdapterRecyclerViewFamily extends RecyclerView.Adapter<AdapterRecyclerViewFamily.MultipleJoinsViewHolder> {

    private Context context;
    private ArrayList<ModelAccounts> accountsArrayList;

    public AdapterRecyclerViewFamily(Context context, ArrayList<ModelAccounts> accountsArrayList){
        this.context = context;
        this.accountsArrayList = accountsArrayList;

    }

    @NonNull
    @Override
    public MultipleJoinsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_family,parent,false);
        return new  MultipleJoinsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MultipleJoinsViewHolder holder, int position) {
        final ModelAccounts accounts = (ModelAccounts) accountsArrayList.get(position);
        holder.mEmail.setText(accounts.getAccountsEmail());
        holder.mName.setText(accounts.getAccountsName());
        int accountID =  context.getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).getInt("APP_ACCOUNT_ID", 0);
        if (accounts.getAccountsId() == accountID){
            holder.btnMenuFamily.setVisibility(View.VISIBLE);
        }else {
            holder.btnMenuFamily.setVisibility(View.GONE);
        }
        if (accounts.getModelHome().getIsAccept() == 0){
            holder.mStatus.setText("รอการตอบรับ");
        }
        holder.btnMenuFamily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSelectedMenuFamily(v,accounts);
            }
        });


    }

    private void onClickSelectedMenuFamily(View view, final ModelAccounts accounts) {
        PopupMenu menu = new PopupMenu(context, view);
        menu.getMenuInflater().inflate(R.menu.setting_itme_family,menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_family_delete:
                        Toast.makeText(context, "Delete "+accounts.getAccountsName(), Toast.LENGTH_SHORT).show();
                        break;
                }

                return true;
            }
        });

        menu.show();
    }

    @Override
    public int getItemCount() {
        return accountsArrayList.size();
    }




    public class MultipleJoinsViewHolder extends RecyclerView.ViewHolder{
        ImageView mImgFamily,btnMenuFamily;
        TextView mEmail,mName,mStatus;
        public MultipleJoinsViewHolder(@NonNull View itemView) {
            super(itemView);
            mImgFamily = itemView.findViewById(R.id.img_family);
            mEmail = itemView.findViewById(R.id.txt_email_family);
            mName = itemView.findViewById(R.id.txt_name_family);
            mStatus = itemView.findViewById(R.id.txt_status_family);
            btnMenuFamily = itemView.findViewById(R.id.btn_menu_family);
        }
    }

}
