package com.aimer.smarthome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.model.ModelAccounts;
import com.aimer.smarthome.utils.ItemClickListenerJoinFamily;

import java.util.ArrayList;

public class AdapterRecyclerViewMultipleJoins extends RecyclerView.Adapter<AdapterRecyclerViewMultipleJoins.MultipleJoinsViewHolder>{

    private Context context;
    private ArrayList<ModelAccounts> accountsArrayList;
    private ItemClickListenerJoinFamily itemClickListenerJoinFamily;

    public AdapterRecyclerViewMultipleJoins(Context context, ArrayList<ModelAccounts> accountsArrayList,ItemClickListenerJoinFamily itemClickListenerJoinFamily){
        this.context = context;
        this.accountsArrayList = accountsArrayList;
        this.itemClickListenerJoinFamily = itemClickListenerJoinFamily;

    }

    @NonNull
    @Override
    public MultipleJoinsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_multiple_joins,parent,false);
        return new  MultipleJoinsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MultipleJoinsViewHolder holder, int position) {
        final ModelAccounts accounts = (ModelAccounts) accountsArrayList.get(position);
        holder.email.setText(accounts.getAccountsEmail());
        holder.name.setText(accounts.getAccountsName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListenerJoinFamily.itemClicked(accounts);
            }
        });

    }

    @Override
    public int getItemCount() {
        return accountsArrayList.size();
    }


    public class MultipleJoinsViewHolder extends RecyclerView.ViewHolder{
        TextView email,name;
        public MultipleJoinsViewHolder(@NonNull View itemView) {
            super(itemView);
            email = itemView.findViewById(R.id.multiple_joins_email);
            name = itemView.findViewById(R.id.multiple_joins_name);
        }
    }

}
