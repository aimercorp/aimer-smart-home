package com.aimer.smarthome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.model.ModelHome;

import java.util.ArrayList;

public class AdapterSpinnerHome extends BaseAdapter {
    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<ModelHome> listHome;
    private LinearLayout linearLayout;

    public AdapterSpinnerHome(Context context,  ArrayList<ModelHome> listHome) {
        this.context = context;
        this.mInflater  = LayoutInflater.from(context);
        this.listHome = listHome;
    }
    @Override
    public int getCount() {
        return listHome.size();
    }

    @Override
    public Object getItem(int position) {
        return listHome.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ModelHome home = (ModelHome) getItem(position);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = mInflater.inflate(R.layout.layout_spinner_home, parent, false);

        TextView text_new_home = convertView.findViewById(R.id.text_new_home);
        text_new_home.setText(home.getHomeName());


        return convertView;
    }
}
