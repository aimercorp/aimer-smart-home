package com.aimer.smarthome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.utils.ItemClickListenerSelectBoard;

import java.util.ArrayList;

public class AdapterRecyclerViewSelectBoard extends RecyclerView.Adapter<AdapterRecyclerViewSelectBoard.SelectBoardViewHolder> {

    private Context context;
    private ArrayList<ModelBoard> modelBoard;
    private ItemClickListenerSelectBoard mClickListener;

    public AdapterRecyclerViewSelectBoard(Context context, ArrayList<ModelBoard> modelBoards,ItemClickListenerSelectBoard mClickListener) {
        this.context = context;
        this.modelBoard = modelBoards;
        this.mClickListener = mClickListener;
    }

    @NonNull
    @Override
    public SelectBoardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name_board,parent,false);

        return new SelectBoardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectBoardViewHolder holder, final int position) {
        final ModelBoard board = (ModelBoard) modelBoard.get(position);
        holder.txtBoardName.setText(board.getBoardName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.itemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelBoard.size();
    }

    public class SelectBoardViewHolder extends RecyclerView.ViewHolder{
        TextView txtBoardName;
        public SelectBoardViewHolder(@NonNull View itemView) {
            super(itemView);
            txtBoardName = itemView.findViewById(R.id.txt_select_board_name);
        }
    }



}
