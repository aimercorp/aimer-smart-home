package com.aimer.smarthome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.model.ModelHome;
import com.aimer.smarthome.model.ModelTypeDevices;
import com.aimer.smarthome.utils.ItemClickListenerOnClickDevices;
import com.aimer.smarthome.utils.ItemClickListenerSpinnerFilter;

import java.util.ArrayList;

public class AdapterSpinnerFilterDevices extends RecyclerView.Adapter<AdapterSpinnerFilterDevices.SpinnerFilterViewHolder> {

    private Context context;
    private ArrayList<ModelTypeDevices> listModelTypeDevices;
    private ItemClickListenerSpinnerFilter mItemClick;

    public AdapterSpinnerFilterDevices(Context context, ArrayList<ModelTypeDevices> listTypeDevices,ItemClickListenerSpinnerFilter mItemClick) {
        this.context = context;
        this.listModelTypeDevices = listTypeDevices;
        this.mItemClick = mItemClick;
    }

    @NonNull
    @Override
    public SpinnerFilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_spinner_filter_devices,parent,false);

        return new SpinnerFilterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SpinnerFilterViewHolder holder, int position) {
        final ModelTypeDevices typeDevices = (ModelTypeDevices) listModelTypeDevices.get(position);
        holder.mTextView.setText(typeDevices.getTypeDevices());
        holder.mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClick.itemClicked(typeDevices);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listModelTypeDevices.size();
    }


    public class SpinnerFilterViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView;
        public SpinnerFilterViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.text_type_devices);
        }
    }
}
