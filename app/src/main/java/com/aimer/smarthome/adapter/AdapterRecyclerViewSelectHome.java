package com.aimer.smarthome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.model.ModelHome;
import com.aimer.smarthome.utils.ItemClickListenerSelectHome;

import java.util.ArrayList;

public class AdapterRecyclerViewSelectHome extends RecyclerView.Adapter<AdapterRecyclerViewSelectHome.SelectBoardViewHolder> {

    private Context context;
    private ArrayList<ModelHome> modelHomeArrayList;
    private ItemClickListenerSelectHome mClickListener;

    public AdapterRecyclerViewSelectHome(Context context, ArrayList<ModelHome> modelHomeArrayList, ItemClickListenerSelectHome mClickListener) {
        this.context = context;
        this.modelHomeArrayList = modelHomeArrayList;
        this.mClickListener = mClickListener;
    }

    @NonNull
    @Override
    public SelectBoardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name_board,parent,false);

        return new SelectBoardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectBoardViewHolder holder, final int position) {
        final ModelHome modelHome = (ModelHome) modelHomeArrayList.get(position);
        holder.txtHomeName.setText(modelHome.getHomeName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.itemClicked(modelHome,position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return modelHomeArrayList.size();
    }

    public class SelectBoardViewHolder extends RecyclerView.ViewHolder{
        TextView txtHomeName;
        public SelectBoardViewHolder(@NonNull View itemView) {
            super(itemView);
            txtHomeName = itemView.findViewById(R.id.txt_select_board_name);
        }
    }



}
