package com.aimer.smarthome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.fragment_popup.SelectChannelFragment;
import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.model.ModelDevices;
import com.aimer.smarthome.utils.ItemClickListenerSelectChannel;

import java.util.ArrayList;

public class AdapterRecyclerViewSelectChannel extends RecyclerView.Adapter<AdapterRecyclerViewSelectChannel.SelectChannelViewHolder>{

    private Context context;
    private ArrayList<ModelDevices> arrayListModelDevices = new ArrayList<>();
    private ItemClickListenerSelectChannel mClickListener;


    public AdapterRecyclerViewSelectChannel(Context context, ArrayList<ModelDevices> arrayListModelDevices, ItemClickListenerSelectChannel mClickListener){
        this.context = context;
        this.arrayListModelDevices = arrayListModelDevices;
        this.mClickListener = mClickListener;
    }



    public class SelectChannelViewHolder extends RecyclerView.ViewHolder{
        TextView txt_DevicesName;
        ImageView img_Devices;
        public SelectChannelViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_DevicesName = itemView.findViewById(R.id.txt_select_channel_devices_name);
            img_Devices = itemView.findViewById(R.id.img_select_channel_devices);
        }

    }

    @NonNull
    @Override
    public SelectChannelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_select_channel,parent,false);

        return new SelectChannelViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectChannelViewHolder holder, int position) {

        final ModelDevices devices = (ModelDevices) arrayListModelDevices.get(position);
        holder.txt_DevicesName.setText(devices.getDevicesName());
        if (devices.getDevicesIcon() != null && devices.getDevicesIcon() != "null"){
            int resId = getResourceByFilename(context, devices.getDevicesIcon());
            holder.img_Devices.setImageResource(resId);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.itemClicked(devices);
            }
        });



    }

    @Override
    public int getItemCount() {
        return arrayListModelDevices == null ? 0 : arrayListModelDevices.size();
    }

    public static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }



}
