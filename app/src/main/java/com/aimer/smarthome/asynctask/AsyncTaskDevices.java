package com.aimer.smarthome.asynctask;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.aimer.smarthome.R;
import com.aimer.smarthome.fragment_filter.FilterALLFragment;
import com.aimer.smarthome.interface_callback.CallBackServiceBoard;
import com.aimer.smarthome.interface_callback.CallBackServiceDevices;
import com.aimer.smarthome.model.DeviceAlert;
import com.aimer.smarthome.model.DeviceCCTV;
import com.aimer.smarthome.model.DeviceElectricMeter;
import com.aimer.smarthome.model.DeviceFireAlarm;
import com.aimer.smarthome.model.DeviceFrontSlidingDoor;
import com.aimer.smarthome.model.DeviceLight;
import com.aimer.smarthome.model.DeviceMagneticDoor;
import com.aimer.smarthome.model.DeviceMagneticSensor;
import com.aimer.smarthome.model.DeviceTelevision;
import com.aimer.smarthome.model.DeviceThermometer;
import com.aimer.smarthome.model.DevicesAirConditioner;
import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.model.ModelDevices;
import com.aimer.smarthome.model.ModelPin;
import com.aimer.smarthome.model.ModelTypeDevices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AsyncTaskDevices extends AsyncTask<String,Integer,String> {
    private ArrayList<ModelBoard> arrayLists = new ArrayList<ModelBoard>();
    CallBackServiceDevices callBackService;
    View view;

    public AsyncTaskDevices(CallBackServiceDevices callBackService){
        this.callBackService = callBackService;

    }

    public AsyncTaskDevices(CallBackServiceDevices callBackService, View view) {
        this.callBackService = callBackService;
        this.view = view;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
            view.findViewById(R.id.loader).setVisibility(View.VISIBLE);
            view.findViewById(R.id.mainContainer).setVisibility(View.GONE);
            view.findViewById(R.id.errorText).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayLists = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerListDevices(arrayLists);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelBoard> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelBoard> arrayList = new ArrayList<>();
        try {

            JSONObject jsonObject = new JSONObject(dataJSon);
            JSONArray jsonBoardData = jsonObject.optJSONArray("BoardDataActive");
            JSONArray jsonDevicesData = jsonObject.optJSONArray("DevicesDataActive");

            if (jsonBoardData != null){
                for(int i=0;i < jsonBoardData.length();i++) {
                    Log.e("Message", "loop");
                    ArrayList<ModelDevices> listDevices = new ArrayList<>();
                    JSONObject json_BoardData = jsonBoardData.optJSONObject(i);

                    //Loop Board

                    if (json_BoardData.getString("board_Id") != "null" && json_BoardData.getString("board_Id") != null ){
                        ModelBoard board = new ModelBoard();
                        board.setBoardId(json_BoardData.getInt("board_Id"));
                        board.setBoardName(json_BoardData.getString("board_name"));
                        board.setBoardMacAddress(json_BoardData.getString("board_macAddress"));
                        board.setType_sizeInPut(json_BoardData.getInt("type_sizeInPut"));
                        board.setType_sizeOutPut(json_BoardData.getInt("type_sizeOutPut"));
                        board.setType_size(json_BoardData.getString("type_size"));

                        //Loop Devices

                        for (int j = 0; j < jsonDevicesData.length() ; j++){

                            //New Object Model
                            DeviceLight light = new DeviceLight();
                            DeviceThermometer thermometer = new DeviceThermometer();
                            DeviceMagneticDoor magneticDoor = new DeviceMagneticDoor();
                            DevicesAirConditioner airConditioner = new DevicesAirConditioner();
                            DeviceTelevision television = new DeviceTelevision();
                            DeviceAlert alert = new DeviceAlert();
                            DeviceCCTV cctv = new DeviceCCTV();
                            DeviceMagneticSensor magneticSensor = new DeviceMagneticSensor();
                            DeviceFireAlarm fireAlarm = new DeviceFireAlarm();
                            DeviceFrontSlidingDoor frontSlidingDoor = new DeviceFrontSlidingDoor();
                            DeviceElectricMeter electricMeter = new DeviceElectricMeter();

                            ModelPin pin = new ModelPin();


                            JSONObject json_DevicesData = jsonDevicesData.optJSONObject(j);

                            if (json_BoardData.getInt("board_Id") == json_DevicesData.getInt("Board_board_Id")){

                                if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null"){

                                    //Thermometer

                                    if (json_DevicesData.getString("typeDevices").equalsIgnoreCase("Thermometer")) {
                                        thermometer.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        thermometer.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        thermometer.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        thermometer.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if (json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null") {
                                            thermometer.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")), json_DevicesData.getString("typeDevices"));
                                            thermometer.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            thermometer.setFavouritesStatus(true);
                                        }else {
                                            thermometer.setFavouritesStatus(false);
                                        }
                                        thermometer.setModelPin(pin);
                                        thermometer.setModelBoard(board);
                                        listDevices.add(thermometer);

                                    }

                                    //Magnetic Door

                                    else if (json_DevicesData.getString("typeDevices").equalsIgnoreCase("Magnetic Door")){
                                        magneticDoor.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        magneticDoor.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        magneticDoor.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        magneticDoor.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                            magneticDoor.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")),json_DevicesData.getString("typeDevices"));
                                            magneticDoor.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            magneticDoor.setFavouritesStatus(true);
                                        }else {
                                            magneticDoor.setFavouritesStatus(false);
                                        }
                                        magneticDoor.setModelPin(pin);
                                        magneticDoor.setModelBoard(board);
                                        listDevices.add(magneticDoor);

                                    }

                                    //Air Conditioner

                                    else if (json_DevicesData.getString("typeDevices").equalsIgnoreCase("Air Conditioner")) {
                                        airConditioner.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        airConditioner.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        airConditioner.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        airConditioner.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                            airConditioner.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")),json_DevicesData.getString("typeDevices"));
                                            airConditioner.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            airConditioner.setFavouritesStatus(true);
                                        }else {
                                            airConditioner.setFavouritesStatus(false);
                                        }
                                        airConditioner.setModelPin(pin);
                                        airConditioner.setModelBoard(board);
                                        listDevices.add(airConditioner);
                                    }

                                    //Television

                                    else if (json_DevicesData.getString("typeDevices").equalsIgnoreCase("Television")) {
                                        television.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        television.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        television.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        television.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                            television.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")),json_DevicesData.getString("typeDevices"));
                                            television.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            television.setFavouritesStatus(true);
                                        }else {
                                            television.setFavouritesStatus(false);
                                        }
                                        television.setModelPin(pin);
                                        television.setModelBoard(board);
                                        listDevices.add(television);
                                    }

                                    //CCTV

                                    else if (json_DevicesData.getString("typeDevices").equalsIgnoreCase("CCTV")) {
                                        cctv.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        cctv.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        cctv.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        cctv.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                            cctv.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")),json_DevicesData.getString("typeDevices"));
                                            cctv.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            cctv.setFavouritesStatus(true);
                                        }else {
                                            cctv.setFavouritesStatus(false);
                                        }
                                        cctv.setModelPin(pin);
                                        cctv.setModelBoard(board);
                                        listDevices.add(cctv);
                                    }

                                    //Magnetic Sensor

                                    else if (json_DevicesData.getString("typeDevices").equalsIgnoreCase("Magnetic Sensor")) {
                                        magneticSensor.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        magneticSensor.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        magneticSensor.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        magneticSensor.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                            magneticSensor.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")),json_DevicesData.getString("typeDevices"));
                                            magneticSensor.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            magneticSensor.setFavouritesStatus(true);
                                        }else {
                                            magneticSensor.setFavouritesStatus(false);
                                        }
                                        magneticSensor.setModelPin(pin);
                                        magneticSensor.setModelBoard(board);
                                        listDevices.add(magneticSensor);
                                    }

                                    //Fire Alarm

                                    else if (json_DevicesData.getString("typeDevices").equalsIgnoreCase("Fire Alarm")) {
                                        fireAlarm.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        fireAlarm.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        fireAlarm.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        fireAlarm.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                            fireAlarm.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")),json_DevicesData.getString("typeDevices"));
                                            fireAlarm.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            fireAlarm.setFavouritesStatus(true);
                                        }else {
                                            fireAlarm.setFavouritesStatus(false);
                                        }
                                        fireAlarm.setModelPin(pin);
                                        fireAlarm.setModelBoard(board);
                                        listDevices.add(fireAlarm);
                                    }

                                    //Alert

                                    else if (json_DevicesData.getString("typeDevices").equalsIgnoreCase("Alert")) {
                                        alert.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        alert.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        alert.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        alert.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                            alert.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")),json_DevicesData.getString("typeDevices"));
                                            alert.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            alert.setFavouritesStatus(true);
                                        }else {
                                            alert.setFavouritesStatus(false);
                                        }
                                        alert.setModelPin(pin);
                                        alert.setModelBoard(board);
                                        listDevices.add(alert);
                                    }

                                    //Front Sliding Door

                                    else if (json_DevicesData.getString("typeDevices").equalsIgnoreCase("Front Sliding Door")) {
                                        frontSlidingDoor.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        frontSlidingDoor.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        frontSlidingDoor.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        frontSlidingDoor.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                            frontSlidingDoor.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")),json_DevicesData.getString("typeDevices"));
                                            frontSlidingDoor.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            frontSlidingDoor.setFavouritesStatus(true);
                                        }else {
                                            frontSlidingDoor.setFavouritesStatus(false);
                                        }
                                        frontSlidingDoor.setModelPin(pin);
                                        frontSlidingDoor.setModelBoard(board);
                                        listDevices.add(frontSlidingDoor);
                                    }

                                    //Electric Meter

                                    else if (json_DevicesData.getString("typeDevices").equalsIgnoreCase("Electric Meter")) {
                                        electricMeter.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        electricMeter.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        electricMeter.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        electricMeter.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                            electricMeter.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")),json_DevicesData.getString("typeDevices"));
                                            electricMeter.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            electricMeter.setFavouritesStatus(true);
                                        }else {
                                            electricMeter.setFavouritesStatus(false);
                                        }
                                        electricMeter.setModelPin(pin);
                                        electricMeter.setModelBoard(board);
                                        listDevices.add(electricMeter);
                                    }

                                    //Light

                                    else {
                                        light.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                        light.setDevicesName(json_DevicesData.getString("devices_Name"));
                                        light.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                        light.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                        if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                            light.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                        }
                                        pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                        pin.setPin(json_DevicesData.getInt("pin"));
                                        pin.setTypePin(json_DevicesData.getString("typePin"));
                                        if (json_DevicesData.getString("TypeDevices_typeDevices_Id") != "null" && json_DevicesData.getString("typeDevices") != "null") {
                                            ModelTypeDevices typeDevices = new ModelTypeDevices(Integer.parseInt(json_DevicesData.getString("TypeDevices_typeDevices_Id")),json_DevicesData.getString("typeDevices"));
                                            light.setModelTypeDevices(typeDevices);
                                        }
                                        if (json_DevicesData.getString("favourites_status") != "null" && json_DevicesData.getInt("favourites_status") == 1){
                                            light.setFavouritesStatus(true);
                                        }else {
                                            light.setFavouritesStatus(false);
                                        }
                                        light.setModelPin(pin);
                                        light.setModelBoard(board);
                                        listDevices.add(light);
                                        }

                                }else {

                                } // close check type devices id

                            } // close check board

                        }


                        board.setModelDevices(listDevices);
                        arrayList.add(board);
                    }

                }

            }
            view.findViewById(R.id.loader).setVisibility(View.GONE);
            view.findViewById(R.id.mainContainer).setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
            view.findViewById(R.id.loader).setVisibility(View.GONE);
            view.findViewById(R.id.errorText).setVisibility(View.VISIBLE);
        }
        return arrayList;
    }

}

