package com.aimer.smarthome.asynctask;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.aimer.smarthome.R;
import com.aimer.smarthome.fragment.FavoritesFragment;
import com.aimer.smarthome.interface_callback.CallBackServiceHome;
import com.aimer.smarthome.model.ModelHome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AsyncTaskHome extends AsyncTask<String,Integer,String> {
    private ArrayList<ModelHome> arrayLists = new ArrayList<ModelHome>();
    CallBackServiceHome callBackService;
    View view;
    public AsyncTaskHome(CallBackServiceHome callBackService,View view){
        this.callBackService = callBackService;
        this.view = view;
    }


    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();

                view.findViewById(R.id.loader).setVisibility(View.VISIBLE);
//                view.findViewById(R.id.loader_home).setVisibility(View.GONE);
                view.findViewById(R.id.mainContainer).setVisibility(View.GONE);
                view.findViewById(R.id.errorText).setVisibility(View.GONE);

        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayLists = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayLists);

        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelHome> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelHome> arrayList = new ArrayList<>();
        try {

            JSONArray jsonArray = new JSONArray(dataJSon);

            if (jsonArray != null){
                for(int i=0;i < jsonArray.length();i++) {
                    Log.e("Message", "loop");

                    ModelHome home = new ModelHome();
                    JSONObject json = jsonArray.optJSONObject(i);
                    home.setHomeId(json.getInt("home_Id"));
                    home.setHomeName(json.getString("home_Name"));

                    arrayList.add(home);
                }
            }
                view.findViewById(R.id.loader).setVisibility(View.GONE);
//                view.findViewById(R.id.loader_home).setVisibility(View.VISIBLE);
                view.findViewById(R.id.mainContainer).setVisibility(View.VISIBLE);
                view.findViewById(R.id.errorHomeText).setVisibility(View.GONE);
                view.findViewById(R.id.createHomeText).setVisibility(View.GONE);

        } catch (JSONException e) {
            e.printStackTrace();
//                view.findViewById(R.id.loader_home).setVisibility(View.GONE);
                view.findViewById(R.id.loader).setVisibility(View.GONE);
                view.findViewById(R.id.errorText).setVisibility(View.VISIBLE);
                view.findViewById(R.id.errorHomeText).setVisibility(View.GONE);
                view.findViewById(R.id.createHomeText).setVisibility(View.GONE);
        }
        return arrayList;
    }
}
