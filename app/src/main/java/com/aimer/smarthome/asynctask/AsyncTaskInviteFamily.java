package com.aimer.smarthome.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.aimer.smarthome.interface_callback.CallBackServiceInviteFamily;
import com.aimer.smarthome.interface_callback.CallBackServiceWeather;
import com.aimer.smarthome.model.ModelAccounts;
import com.aimer.smarthome.model.ModelWeather;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AsyncTaskInviteFamily extends AsyncTask<String,Integer,String> {

    private ModelAccounts modelAccounts = new ModelAccounts();
    CallBackServiceInviteFamily callBackService;

    public AsyncTaskInviteFamily(CallBackServiceInviteFamily callBackService){
        this.callBackService = callBackService;

    }


    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            modelAccounts = onParserContentToModel(s);
            callBackService.onRequestCompleteModelAccounts(modelAccounts);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ModelAccounts onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ModelAccounts accounts = new ModelAccounts();
        try {

            JSONObject jsonObject = new JSONObject(dataJSon);

            if (jsonObject != null) {
                accounts.setAccountsId(jsonObject.getInt("accounts_Id"));
                accounts.setAccountsKey(jsonObject.getString("accounts_Key"));
                accounts.setAccountsEmail(jsonObject.getString("accounts_Email"));
                accounts.setAccountsPin(jsonObject.getString("accounts_Pin"));
                accounts.setAccountsName(jsonObject.getString("accounts_Name"));
                accounts.setAccountsPhoto(jsonObject.getString("accounts_Photo"));
                accounts.setAccountsIsActive(jsonObject.getInt("accounts_isActive"));
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
        return accounts;
    }


}
