package com.aimer.smarthome.asynctask;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.aimer.smarthome.R;
import com.aimer.smarthome.interface_callback.CallBackServiceTypeDevices;
import com.aimer.smarthome.model.ModelTypeDevices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AsyncTaskTypeDevices extends AsyncTask<String,Integer,String> {
    private ArrayList<ModelTypeDevices> arrayLists = new ArrayList<ModelTypeDevices>();
    CallBackServiceTypeDevices callBackService;
    View view;
    public AsyncTaskTypeDevices(CallBackServiceTypeDevices callBackService,View view){
        this.callBackService = callBackService;
        this.view = view;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
                view.findViewById(R.id.selected_filter_loader).setVisibility(View.VISIBLE);
                view.findViewById(R.id.selected_filter_mainContainer).setVisibility(View.GONE);
                view.findViewById(R.id.selected_filter_errorText).setVisibility(View.GONE);
                view.findViewById(R.id.selected_filter_noDataText).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayLists = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerModelTypeDevices(arrayLists);

        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelTypeDevices> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelTypeDevices> arrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(dataJSon);
            JSONArray jsonArray= jsonObject.optJSONArray("TypeDevices");


            if (jsonArray != null){
                for(int i=0;i < jsonArray.length();i++) {
                    Log.e("Message", "loop");

                    ModelTypeDevices typeDevices = new ModelTypeDevices();
                    JSONObject json = jsonArray.optJSONObject(i);
                    typeDevices.setTypeDevicesId(json.getInt("typeDevices_Id"));
                    typeDevices.setTypeDevices(json.getString("typeDevices"));

                    arrayList.add(typeDevices);
                }
            }
            if (arrayList.size() != 0 && arrayList != null){
                view.findViewById(R.id.selected_filter_loader).setVisibility(View.GONE);
                view.findViewById(R.id.selected_filter_mainContainer).setVisibility(View.VISIBLE);
            }else {
                view.findViewById(R.id.selected_filter_noDataText).setVisibility(View.VISIBLE);
                view.findViewById(R.id.selected_filter_loader).setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
                view.findViewById(R.id.selected_filter_loader).setVisibility(View.GONE);
                view.findViewById(R.id.selected_filter_errorText).setVisibility(View.VISIBLE);
        }
        return arrayList;
    }
}
