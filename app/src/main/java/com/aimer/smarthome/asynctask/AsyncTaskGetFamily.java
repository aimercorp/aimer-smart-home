package com.aimer.smarthome.asynctask;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.aimer.smarthome.interface_callback.CallBackServiceGetFamily;
import com.aimer.smarthome.interface_callback.CallBackServiceInvited;
import com.aimer.smarthome.model.ModelAccounts;
import com.aimer.smarthome.model.ModelHome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class AsyncTaskGetFamily extends AsyncTask<String,Integer,String> {
    private ArrayList<ModelAccounts> modelAccounts = new ArrayList<>();
    CallBackServiceGetFamily callBackService;
    View view;

    public AsyncTaskGetFamily(CallBackServiceGetFamily callBackService,View view){
        this.callBackService = callBackService;
        this.view = view;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            modelAccounts = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerFamily(modelAccounts);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelAccounts> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelAccounts> arrayList = new ArrayList<>();
        try {

            JSONArray jsonArray = new JSONArray(dataJSon);

            if (jsonArray != null){
                for(int i=0;i < jsonArray.length();i++) {
                    Log.e("Message", "loop");
                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                    ModelAccounts accounts = new ModelAccounts();
                    ModelHome modelHome = new ModelHome();

                    accounts.setAccountsId(jsonObject.getInt("accounts_Id"));
                    accounts.setAccountsKey(jsonObject.getString("accounts_Key"));
                    accounts.setAccountsEmail(jsonObject.getString("accounts_Email"));
                    accounts.setAccountsPin(jsonObject.getString("accounts_Pin"));
                    accounts.setAccountsToken(jsonObject.getString("accounts_Token"));
                    accounts.setAccountsName(jsonObject.getString("accounts_Name"));
                    accounts.setAccountsPhoto(jsonObject.getString("accounts_Photo"));
                    accounts.setAccountsIsActive(jsonObject.getInt("accounts_isActive"));
                    modelHome.setHomeId(jsonObject.getInt("home_Id"));
                    modelHome.setHomeName(jsonObject.getString("home_Name"));
                    modelHome.setIsAccept(jsonObject.getInt("isAccept"));
                    accounts.setModelHome(modelHome);
                    arrayList.add(accounts);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrayList;
    }
}
