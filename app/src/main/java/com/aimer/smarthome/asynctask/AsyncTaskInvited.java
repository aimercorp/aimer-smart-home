package com.aimer.smarthome.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.aimer.smarthome.interface_callback.CallBackServiceAccount;
import com.aimer.smarthome.interface_callback.CallBackServiceInvited;
import com.aimer.smarthome.model.ModelAccounts;
import com.aimer.smarthome.model.ModelHome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class AsyncTaskInvited extends AsyncTask<String,Integer,String> {
    private ArrayList<ModelAccounts> modelAccounts = new ArrayList<>();
    CallBackServiceInvited callBackService;


    public AsyncTaskInvited(CallBackServiceInvited callBackService){
        this.callBackService = callBackService;

    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            modelAccounts = onParserContentToModel(s);
            callBackService.onRequestCompleteListenerInvited(modelAccounts);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelAccounts> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelAccounts> arrayList = new ArrayList<>();
        try {

            JSONObject jsonObject = new JSONObject(dataJSon);
            JSONArray jsonInvite = jsonObject.optJSONArray("invite");
            JSONArray jsonHome= jsonObject.optJSONArray("home");

            if (jsonInvite != null){
                for(int i=0;i < jsonInvite.length();i++) {
                    Log.e("Message", "loop");
                    JSONObject invite = jsonInvite.optJSONObject(i);
                    ModelAccounts accounts = new ModelAccounts();
                    ModelHome modelHome = new ModelHome();

                    accounts.setAccountsId(invite.getInt("Accounts_accounts_Id"));
                    accounts.setAccountsEmail(invite.getString("Invite_accounts_Email"));
                    accounts.setAccountsName(invite.getString("Invite_accounts_Name"));

                    for (int j=0;j < jsonHome.length(); j++){
                        JSONObject home = jsonHome.optJSONObject(j);

                        if (invite.getString("invite_homeId").equals(home.getString("home_Id"))){
                            modelHome.setHomeId(home.getInt("home_Id"));
                            modelHome.setHomeName(home.getString("home_Name"));
                        }
                    }
                    accounts.setModelHome(modelHome);
                    arrayList.add(accounts);
                }



            }

        } catch (JSONException e) {
            e.printStackTrace();

        }

        return arrayList;
    }
}
