package com.aimer.smarthome.asynctask;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.aimer.smarthome.R;
import com.aimer.smarthome.interface_callback.CallBackServiceBoard;
import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.model.ModelDevices;
import com.aimer.smarthome.model.ModelPin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AsyncTaskBoard extends AsyncTask<String,Integer,String> {
    private ArrayList<ModelBoard> arrayLists = new ArrayList<ModelBoard>();
    CallBackServiceBoard callBackService;
    View view;
    public AsyncTaskBoard(CallBackServiceBoard callBackService,View view){
        this.callBackService = callBackService;
        this.view = view;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
            view.findViewById(R.id.selected_channel_loader).setVisibility(View.VISIBLE);
            view.findViewById(R.id.selected_channel_mainContainer).setVisibility(View.GONE);
            view.findViewById(R.id.selected_channel_errorText).setVisibility(View.GONE);
            view.findViewById(R.id.selected_channel_noDataText).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayLists = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayLists);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelBoard> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelBoard> arrayList = new ArrayList<>();
        try {

            JSONObject jsonObject = new JSONObject(dataJSon);
            JSONArray jsonBoardData = jsonObject.optJSONArray("BoardData");
            JSONArray jsonDevicesData = jsonObject.optJSONArray("DevicesData");

            if (jsonBoardData != null){
                for(int i=0;i < jsonBoardData.length();i++) {
                    Log.e("Message", "loop");
                    ArrayList<ModelDevices> listDevices = new ArrayList<>();
                    JSONObject json_BoardData = jsonBoardData.optJSONObject(i);
                    if (json_BoardData.getString("board_Id") != "null" && json_BoardData.getString("board_Id") != null ){
                        ModelBoard board = new ModelBoard();
                        board.setBoardId(json_BoardData.getInt("board_Id"));
                        board.setBoardName(json_BoardData.getString("board_name"));
                        board.setBoardMacAddress(json_BoardData.getString("board_macAddress"));
                        board.setType_sizeInPut(json_BoardData.getInt("type_sizeInPut"));
                        board.setType_sizeOutPut(json_BoardData.getInt("type_sizeOutPut"));
                        board.setType_size(json_BoardData.getString("type_size"));

                        for (int j = 0; j < jsonDevicesData.length() ; j++){
                            ModelDevices devices = new ModelDevices();
                            ModelPin pin = new ModelPin();
                            JSONObject json_DevicesData = jsonDevicesData.optJSONObject(j);
                            if (json_BoardData.getInt("board_Id") == json_DevicesData.getInt("Board_board_Id")){
                                devices.setDevicesId(json_DevicesData.getInt("devices_Id"));
                                devices.setDevicesName(json_DevicesData.getString("devices_Name"));
                                devices.setDevicesIsActive(json_DevicesData.getInt("devices_isActive"));
                                devices.setDevicesStatus(json_DevicesData.getString("devices_status"));

                                if ( json_DevicesData.getString("devices_Icon") != null && json_DevicesData.getString("devices_Icon") != "" && json_DevicesData.getString("devices_Icon") != "null"){
                                    devices.setDevicesIcon(json_DevicesData.getString("devices_Icon"));
                                }
                                pin.setPinId(json_DevicesData.getInt("Pin_pin_Id"));
                                pin.setPin(json_DevicesData.getInt("pin"));
                                pin.setTypePin(json_DevicesData.getString("typePin"));
                                devices.setModelPin(pin);
                                listDevices.add(devices);
                            }

                        }
                        board.setModelDevices(listDevices);
                        arrayList.add(board);
                    }

                }

            }

            if (arrayList.size() != 0 && arrayList != null){
                view.findViewById(R.id.selected_channel_loader).setVisibility(View.GONE);
                view.findViewById(R.id.selected_channel_mainContainer).setVisibility(View.VISIBLE);
            }else {
                view.findViewById(R.id.selected_channel_noDataText).setVisibility(View.VISIBLE);
                view.findViewById(R.id.selected_channel_loader).setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            view.findViewById(R.id.selected_channel_loader).setVisibility(View.GONE);
            view.findViewById(R.id.selected_channel_errorText).setVisibility(View.VISIBLE);
        }
        return arrayList;
    }

}

