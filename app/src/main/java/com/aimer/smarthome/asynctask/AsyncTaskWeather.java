package com.aimer.smarthome.asynctask;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.aimer.smarthome.interface_callback.CallBackServiceWeather;
import com.aimer.smarthome.model.ModelWeather;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AsyncTaskWeather extends AsyncTask<String,Integer,String> {

    private ModelWeather modelWeather = new ModelWeather();
    CallBackServiceWeather callBackService;

    public AsyncTaskWeather(CallBackServiceWeather callBackService){
        this.callBackService = callBackService;

    }


    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            modelWeather = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(modelWeather);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ModelWeather onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ModelWeather modelWeather = new ModelWeather();
        try {

            JSONObject jsonObj = new JSONObject(dataJSon);
            JSONObject main = jsonObj.getJSONObject("main");
            JSONObject sys = jsonObj.getJSONObject("sys");
            JSONObject wind = jsonObj.getJSONObject("wind");
            JSONObject weather = jsonObj.getJSONArray("weather").getJSONObject(0);

            if (jsonObj != null) {
                Long updatedAt = jsonObj.getLong("dt");
                modelWeather.setUpdated_at("Updated at: " + new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH).format(new Date(updatedAt * 1000)));
                modelWeather.setTemp(main.getString("temp") + "°C");
                modelWeather.setTempMin(main.getString("temp_min") + "°C");
                modelWeather.setTempMax(main.getString("temp_max") + "°C");
                modelWeather.setPressure(main.getString("pressure"));
                modelWeather.setHumidity("humidity : " + main.getString("humidity") +" %");
                Long sunrise = sys.getLong("sunrise");
                Long sunset = sys.getLong("sunset");
                modelWeather.setSunrise(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date(sunrise * 1000)));
                modelWeather.setSunset(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date(sunset * 1000)));
                modelWeather.setWindSpeed(wind.getString("speed"));
                modelWeather.setDescription(weather.getString("description"));
                modelWeather.setAddress(jsonObj.getString("name").replaceAll("Province", ""));

                if (weather.getString("description").equalsIgnoreCase("broken clouds")) {
                    modelWeather.setIcon("weather_broken_clouds");
                } else if (weather.getString("description").equalsIgnoreCase("few clouds")) {
                    modelWeather.setIcon("weather_few_clouds");
                } else if (weather.getString("description").equalsIgnoreCase("light rain")
                        || weather.getString("description").equalsIgnoreCase("moderate rain")
                        || weather.getString("description").equalsIgnoreCase("heavy intensity rain")){

                    modelWeather.setIcon("weather_rain");
                } else if (weather.getString("description").equalsIgnoreCase("rainbow")) {
                    modelWeather.setIcon("weather_rainbow");
                } else if (weather.getString("description").equalsIgnoreCase("scattered clouds")) {
                    modelWeather.setIcon("weather_scattered_clouds");
                } else {
                    modelWeather.setIcon("weather_sky");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
        return modelWeather;
    }


}
