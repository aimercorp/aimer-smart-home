package com.aimer.smarthome.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.activity.MainActivity;
import com.aimer.smarthome.asynctask.AsyncTaskAddFavourite;
import com.aimer.smarthome.asynctask.AsyncTaskGetFavorite;
import com.aimer.smarthome.asynctask.AsyncTaskGetHomeFavorites;
import com.aimer.smarthome.asynctask.AsyncTaskHome;
import com.aimer.smarthome.doget.DoGet;
import com.aimer.smarthome.fragment_filter.FilterALLFragment;
import com.aimer.smarthome.fragment_popup.PowerPushFragment;
import com.aimer.smarthome.interface_callback.CallBackServiceAddFavourite;
import com.aimer.smarthome.interface_callback.CallBackServiceGetFavorite;
import com.aimer.smarthome.interface_callback.CallBackServiceHome;
import com.aimer.smarthome.model.DeviceMagneticDoor;
import com.aimer.smarthome.model.DeviceThermometer;
import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.model.ModelDevices;
import com.aimer.smarthome.model.ModelHome;
import com.aimer.smarthome.mqtt.MqttHelper;
import com.aimer.smarthome.sectioned_expandable.LayoutHelperBoard;
import com.aimer.smarthome.sectioned_expandable.LayoutHelperFavorite;
import com.aimer.smarthome.utils.ItemClickListener;
import com.aimer.smarthome.utils.ItemClickListenerOnClickDevices;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

public class FavoritesFragment extends Fragment implements CallBackServiceHome, CallBackServiceGetFavorite, CallBackServiceAddFavourite, ItemClickListener, ItemClickListenerOnClickDevices , SwipeRefreshLayout.OnRefreshListener{

    //Set TAG
    private static final String TAG ="FavoritesFragment";

    //Toolbar
    private Toolbar mToolbar;

    //MqttHelper
    private MqttHelper MqttClient;
    private MqttAndroidClient client;
    private String android_id;

    //Set String
    private String urlJsonUpdateDevices = "";

    //Int
    private int homeId;
    private int accountId;

    //View
    private View view;

    //RefreshLayout
    private SwipeRefreshLayout mSwipeRefreshLayout;

    //RecyclerView
    private RecyclerView recyclerView;

    //LayoutHelperFavorite
    private LayoutHelperFavorite mLayoutHelperFavorite;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);

        android_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        MqttClient = new MqttHelper();
        client = MqttClient.getMqttClient(getContext(), android_id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorites, container, false);

        mToolbar = view.findViewById(R.id.Toolbar_Favorite);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");
        }


        mSwipeRefreshLayout = view.findViewById(R.id.container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = view.findViewById(R.id.RecyclerView_Favourite);
        mLayoutHelperFavorite = new LayoutHelperFavorite(getContext(),recyclerView,2,this,this);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (urlGetFavorite() != null){
            new AsyncTaskGetFavorite(this,view).execute(urlGetFavorite());
        }else {
            String urlGetHome = getResources().getString(R.string.api_ai_control)+"/smart_home_api/home/controller_home/RegisterHome?accountID="+accountId+"&homeName=";
            new AsyncTaskGetHomeFavorites(this,view).execute(urlGetHome);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.i("FragCreateList","onCreateOptionsMenu called");
        inflater.inflate(R.menu.favorite_navigation_menu,menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem item = menu.findItem(R.id.action_favorite_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mLayoutHelperFavorite.getFilterFavorite(newText);
                return false;
            }
        });

    }

    private String urlGetFavorite(){
        String url = "";
        homeId = getContext().getSharedPreferences("PREF_APP_SELECTED_HOME_ID", Context.MODE_PRIVATE).getInt("APP_SELECTED_HOME_ID", 0);
        accountId =  getContext().getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).getInt("APP_ACCOUNT_ID", 0);
        if (homeId != 0 && accountId != 0){
            url = getResources().getString(R.string.api_ai_control)+"smart_home_api/favourite/controller_favourite/GetFavourite?accountsId="+accountId+"&homeId="+homeId;
        }else {
            url = null;
        }
        return url;
    }

    //AsyncTask GET Home
    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelHome> modelHomes) {
        if (modelHomes != null && modelHomes.size() != 0){
            Log.i("Check data", "" + modelHomes);

            String url = getResources().getString(R.string.api_ai_control)+"smart_home_api/favourite/controller_favourite/GetFavourite?accountsId="+accountId+"&homeId="+modelHomes.get(0).getHomeId();;
            new AsyncTaskGetFavorite(this,view).execute(url);
        }
    }

    @Override
    public void onRequestCompleteListenerListDevices(ArrayList<ModelBoard> listBoard) {
        if (listBoard != null && listBoard.size() != 0) {
            Log.i("Check data", "" + listBoard);

            mLayoutHelperFavorite.clearAllSection();
            mLayoutHelperFavorite.notifyDataSetChanged();
            for (ModelBoard list_model_board : listBoard) {
                ArrayList<Object> list = new ArrayList<>();
                for (Object light : list_model_board.getModelDevices()){
                    list.add(light);
                }
                mLayoutHelperFavorite.addSection(list_model_board.getBoardId(),list_model_board.getBoardMacAddress(),list_model_board.getBoardName(),list);
                mLayoutHelperFavorite.notifyDataSetChanged();
            }
        }else {
            mLayoutHelperFavorite.clearAllSection();
            mLayoutHelperFavorite.notifyDataSetChanged();
        }
    }

    @Override
    public void onRequestCompleteListenerStatusFavorite(ArrayList<Object> status) {
        if (status != null && status.size() != 0){
            Log.i("Check data", "" + status);
            boolean state = Boolean.parseBoolean(status.get(0).toString());
            String msg = status.get(1).toString();
            Toast.makeText(getContext(), msg+"", Toast.LENGTH_SHORT).show();
            loadFragment();
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }//End AsyncTask get home

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    @Override
    public void itemClicked(ModelBoard modelBoard) {

    }

    @Override
    public void itemClicked(ModelDevices modelDevices) {
        onClickDevicesPowerPush(modelDevices);
    }

    @Override
    public void itemClickedThermometer(DeviceThermometer thermometer) {

    }

    @Override
    public void itemClickedMagneticDoor(DeviceMagneticDoor magneticDoor) {

    }

    @Override
    public void itemClickedFavourite(ModelDevices modelDevices, ImageView imgFavourite) {
        int accountID =  getContext().getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).getInt("APP_ACCOUNT_ID", 0);
        String url_addFavourite = getResources().getString(R.string.api_ai_control)+"/smart_home_api/favourite/controller_favourite/addFavourite?accountsId="+accountID+"&devicesId="+modelDevices.getDevicesId();
        new AsyncTaskAddFavourite(this).execute(url_addFavourite);
        if (modelDevices.isFavouritesStatus()){
            modelDevices.setFavouritesStatus(false);
            DrawableCompat.setTint(imgFavourite.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorFilter));
        }else {
            modelDevices.setFavouritesStatus(true);
            DrawableCompat.setTint(imgFavourite.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorNeonRed));

        }
    }

    @Override
    public void setTemp(final TextView temp) {
        client.setCallback(new MqttCallbackExtended(){
            @Override
            public void connectionLost(Throwable cause) {
            }
            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.d(TAG, new String(message.getPayload()));
                temp.setText(onParserContentDHT(new String(message.getPayload()))+" °C");
            }
            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
            }
        });
    }

    @Override
    public void itemClickedOnDevices(ModelDevices modelDevices) {
        modelDevices.setDevicesStatus("1");
        String JsonFormat = "{\"deviceId\":\""+modelDevices.getModelBoard().getBoardMacAddress()+"\",\"action\":\"action.devices.commands.relay.OnOff\",\"value\":{\"on\":1,\"pin\":"+modelDevices.getModelPin().getPin()+"}}";
        sendEvent(JsonFormat,modelDevices.getModelBoard().getBoardMacAddress()+"/ESP/RELAY");
        urlJsonUpdateDevices = getResources().getString(R.string.api_ai_control)+"/smart_home_api/devices/controller_devices/UpdateDevices?id="+modelDevices.getDevicesId()+"&status=1";
        new DoGet(this).execute(urlJsonUpdateDevices);
    }

    @Override
    public void itemClickedOffDevices(ModelDevices modelDevices) {
        modelDevices.setDevicesStatus("0");
        String JsonFormat = "{\"deviceId\":\""+modelDevices.getModelBoard().getBoardMacAddress()+"\",\"action\":\"action.devices.commands.relay.OnOff\",\"value\":{\"on\":0,\"pin\":"+modelDevices.getModelPin().getPin()+"}}";
        sendEvent(JsonFormat,modelDevices.getModelBoard().getBoardMacAddress()+"/ESP/RELAY");
        urlJsonUpdateDevices = getResources().getString(R.string.api_ai_control)+"/smart_home_api/devices/controller_devices/UpdateDevices?id="+modelDevices.getDevicesId()+"&status=0";
        new DoGet(this).execute(urlJsonUpdateDevices);
    }

    //Send Event to board
    private void sendEvent(String JsonFormat,String Topic)
    {
        try {
            MqttClient.publishMessage(client, JsonFormat, 1, Topic);
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }//End Send Event to board

    //GET Temps
    private String onParserContentDHT(String dataJSon) {
        String result = "";
        try {
            JSONObject jsonObject = new JSONObject(dataJSon);
            JSONArray jsonArray = jsonObject.getJSONArray("value");

            if (jsonArray != null){
                for(int i=0;i < jsonArray.length();i++) {
                    Log.e("Message", "loop");

                    JSONObject json = jsonArray.optJSONObject(i);
                    result = json.getString("T");
                    String h = json.getString("H");
                    String f = json.getString("F");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return result = "false";
        }

        return result;
    }//End GET Temps

    private void onClickDevicesPowerPush(ModelDevices modelDevices) {
        int REQ_CODE = 1001;
        Bundle args = new Bundle();
        args.putParcelable("MODEL_DEVICES",modelDevices);
        FragmentManager manager = getFragmentManager();
        PowerPushFragment dialog = new PowerPushFragment();
        dialog.setTargetFragment(FavoritesFragment.this, REQ_CODE);
        dialog.setArguments(args);
        dialog.show(manager,"PowerPushFragment");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001 && resultCode == RESULT_OK) {
            loadFragment();
        }else {

        }
    }

    // Reload current fragment
    private boolean loadFragment(){
        Fragment fragment = new FavoritesFragment();
        if (fragment != null){
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.FrameLayout, fragment)
                    .commit();
            return true;
        }
        return false;
    }//End Reload current fragment


    //Refresh
    @Override
    public void onRefresh() {
        Toast.makeText(getContext(), "Refresh", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
                loadFragment();
            }
        }, 2000);
    }//End Refresh

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
