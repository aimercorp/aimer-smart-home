package com.aimer.smarthome.fragment;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.activity.MainActivity;
import com.aimer.smarthome.activity.NewHomeActivity;
import com.aimer.smarthome.adapter.AdapterSpinnerFilterDevices;
import com.aimer.smarthome.asynctask.AsyncTaskHome;
import com.aimer.smarthome.asynctask.AsyncTaskInvited;
import com.aimer.smarthome.asynctask.AsyncTaskTypeDevices;
import com.aimer.smarthome.asynctask.AsyncTaskWeather;
import com.aimer.smarthome.fragment_filter.FilterALLFragment;
import com.aimer.smarthome.fragment_filter.FilterFavoriteFragment;
import com.aimer.smarthome.fragment_filter.FilterOffFragment;
import com.aimer.smarthome.fragment_filter.FilterOnFragment;
import com.aimer.smarthome.fragment_popup.JoinFamilyFragment;
import com.aimer.smarthome.fragment_popup.MultipleJoinsFamilyFragment;
import com.aimer.smarthome.fragment_popup.SelectFilterFragment;
import com.aimer.smarthome.fragment_popup.SelectHomeFragment;
import com.aimer.smarthome.fragment_popup.SettingsHomeFragment;
import com.aimer.smarthome.interface_callback.CallBackServiceHome;
import com.aimer.smarthome.interface_callback.CallBackServiceInvited;
import com.aimer.smarthome.interface_callback.CallBackServiceTypeDevices;
import com.aimer.smarthome.interface_callback.CallBackServiceWeather;
import com.aimer.smarthome.model.ModelAccounts;
import com.aimer.smarthome.model.ModelHome;
import com.aimer.smarthome.model.ModelTypeDevices;
import com.aimer.smarthome.model.ModelWeather;
import com.aimer.smarthome.mqtt.MqttHelper;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.eclipse.paho.android.service.MqttAndroidClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.app.Activity.RESULT_OK;
import static com.aimer.smarthome.fragment.NotificationsFragment.NOTIFICATION_ID;

import android.provider.Settings.Secure;

public class HomeFragment extends Fragment implements  CallBackServiceHome, CallBackServiceWeather, CallBackServiceInvited,  View.OnClickListener , SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG ="HomeFragment";
    //NOTIFICATION
    private int NOTIFICATION_ID = 1;
    private String CHANNEL_ID = "Notification_id";

    private LinearLayout mLinearLayout;
    private Toolbar toolbar;
    private ArrayList<ModelHome> arrayListHome = null;
    private ImageView imgSettingsHome;
    private TextView txtHomeName,txtDateTime;
    private int accountID,positionHome;
    private String url_json ="", url_GetTypeDevices ="";
    private static String default_HomeName =  null;
    private static int default_HomeID = 0;
    private ModelHome home = new ModelHome();
    private String url_json_BoardAndDevice ="";
    private String url_json_GetFavourite ="";

    //Spinner selected filter devices
    private Spinner mSpinner;

    //ModelTypeDevices
    private ArrayList<ModelTypeDevices> listTypeDevices;
    private ModelTypeDevices modelTypeDevices;

    //ArrayList
    private ArrayList<ModelAccounts> accountsArrayList;

    //MqttHelper
    private MqttHelper MqttClient;
    private MqttAndroidClient client;
    private String android_id;

    //filter page
    private TextView filter_all_devices;
    private TextView filter_favourite_devices;
    private TextView filter_on_devices;
    private TextView filter_off_devices;
    private EditText spinner_filter;

    //TextView
    private TextView errorHomeText;
    private TextView createHomeText;
    private TextView invitedHomeText;

    //LinearLayout
    private LinearLayout mainContainer;

    //RelativeLayout
    private RelativeLayout mainInvite;

    //RefreshLayout
    private SwipeRefreshLayout mSwipeRefreshLayout;

    //View
    private View view;

    //Fragment
    private Fragment fragment ;

    //Location
    private FusedLocationProviderClient providerClient;
    private boolean resultGetLocation;

    //Weather
    private ImageView weatherImg;
    private TextView weatherAddress;
    private TextView weatherDescription;
    private TextView weatherHumidity;
    private TextView weatherTemp;
    private TextView weatherTempMinMax;


    public HomeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setUpObject();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);
        toolbar = view.findViewById(R.id.Toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        setRetainInstance(true);

        //set filter
        filter_all_devices = view.findViewById(R.id.filter_all_devices);
        filter_favourite_devices = view.findViewById(R.id.filter_favourite_devices);
        filter_on_devices = view.findViewById(R.id.filter_on_devices);
        filter_off_devices = view.findViewById(R.id.filter_off_devices);
        spinner_filter = view.findViewById(R.id.Spinner_Filter);
        //set text
        spinner_filter.setText("All Devices");
        //set weather
        weatherImg = view.findViewById(R.id.weather_img);
        weatherAddress = view.findViewById(R.id.weather_address);
        weatherHumidity = view.findViewById(R.id.weather_humidity);
        weatherDescription = view.findViewById(R.id.weather_description);
        weatherTemp = view.findViewById(R.id.weather_temp);
        weatherTempMinMax = view.findViewById(R.id.weather_min_max);
        //set mainContainer
        mainContainer = view.findViewById(R.id.mainContainer);
        errorHomeText = view.findViewById(R.id.errorHomeText);
        createHomeText = view.findViewById(R.id.createHomeText);
        mainInvite = view.findViewById(R.id.RelativeLayout_Invite);
        invitedHomeText = view.findViewById(R.id.txt_invite_to_join);
        //set onClick
        filter_all_devices.setOnClickListener(this);
        filter_favourite_devices.setOnClickListener(this);
        filter_on_devices.setOnClickListener(this);
        filter_off_devices.setOnClickListener(this);
        spinner_filter.setOnClickListener(this);
        createHomeText.setOnClickListener(this);
        mainInvite.setOnClickListener(this);

        mSwipeRefreshLayout = view.findViewById(R.id.container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        imgSettingsHome = view.findViewById(R.id.onClickSettingsHome);
        imgSettingsHome.setOnClickListener(this);
        mLinearLayout = view.findViewById(R.id.onClickSelectedHome);
        mLinearLayout.setOnClickListener(this);
        txtHomeName = view.findViewById(R.id.txt_select_home_name);
        txtDateTime = view.findViewById(R.id.txt_date_time);
        txtDateTime.setText(getCurrentDateTime());


        if(savedInstanceState == null) {
            new AsyncTaskHome(this,view).execute(url_json);

        } else {
            arrayListHome = savedInstanceState.getParcelableArrayList("SaveInstance_ListHome");
        }

        return view;

    }

    //New Object
    private void setUpObject(){
        arrayListHome = new ArrayList<>();
        accountID =  getContext().getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).getInt("APP_ACCOUNT_ID", 0);
        url_json = getResources().getString(R.string.api_ai_control)+"/smart_home_api/home/controller_home/RegisterHome?accountID="+accountID+"&homeName=&isAccept=3";
        android_id = Secure.getString(getContext().getContentResolver(), Secure.ANDROID_ID);
        MqttClient = new MqttHelper();
        client = MqttClient.getMqttClient(getContext(), android_id);

        //setUp providerClient
        providerClient = LocationServices.getFusedLocationProviderClient(getContext());

        modelTypeDevices = new ModelTypeDevices();
    }
    //End New Object

    //AsyncTask GET Home
    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerInvited(ArrayList<ModelAccounts> listAccounts) {
        if (listAccounts != null && listAccounts.size() != 0){
            this.accountsArrayList = listAccounts;
            if (listAccounts.size() > 1){
                invitedHomeText.setText(getString(R.string.you_been_invited)+" "+listAccounts.size());
            }
            mainInvite.setVisibility(View.VISIBLE);
            showNotification(listAccounts);
        }
    }


    @Override
    public void onRequestCompleteListener(ModelWeather weather) {
        if (weather != null){
            Log.i("Check data weather", "" + weather);
//            Picasso.get().load("http://openweathermap.org/img/w/" + weather.getIcon() + ".png").into(weatherImg);
            int iconWeather = getResourceByFilename(getContext(),weather.getIcon());
            weatherImg.setImageResource(iconWeather);
            weatherAddress.setText(weather.getAddress());
            weatherHumidity.setText(weather.getHumidity());
            weatherDescription.setText(weather.getDescription());
            weatherTemp.setText(weather.getTemp());
            weatherTempMinMax.setText(weather.getTempMin() +" / "+ weather.getTempMax());
        }
    }

    //List Home
    @Override
    public void onRequestCompleteListener(ArrayList<ModelHome> listHome) {
        if (listHome != null && listHome.size() != 0){
            Log.i("Check data", "" + listHome);
            this.arrayListHome = listHome;
            mainContainer.setVisibility(View.VISIBLE);
            if (arrayListHome.size() == 1){
                txtHomeName.setText(arrayListHome.get(0).getHomeName());
                url_json_BoardAndDevice = getResources().getString(R.string.api_ai_control)+"/smart_home_api/board/controller_board/GetBoardActive?uid="+accountID+"&homeId="+arrayListHome.get(0).getHomeId();
                url_json_GetFavourite = getResources().getString(R.string.api_ai_control)+"smart_home_api/favourite/controller_favourite/GetFavourite?accountsId="+accountID+"&homeId="+arrayListHome.get(0).getHomeId();
                url_GetTypeDevices = getResources().getString(R.string.api_ai_control)+"/smart_home_api/devices/controller_devices/GetTypeDevices?accountId="+accountID+"&homeId="+arrayListHome.get(0).getHomeId();
            }else {
                default_HomeName = getContext().getSharedPreferences("PREF_APP_SELECTED_HOME_NAME", Context.MODE_PRIVATE).getString("APP_SELECTED_HOME_NAME", null);
                default_HomeID = getContext().getSharedPreferences("PREF_APP_SELECTED_HOME_ID", Context.MODE_PRIVATE).getInt("APP_SELECTED_HOME_ID", 0);
                if (default_HomeName == null && default_HomeID == 0){
                    txtHomeName.setText(arrayListHome.get(0).getHomeName());
                    url_json_BoardAndDevice = getResources().getString(R.string.api_ai_control)+"/smart_home_api/board/controller_board/GetBoardActive?uid="+accountID+"&homeId="+arrayListHome.get(0).getHomeId();
                    url_json_GetFavourite = getResources().getString(R.string.api_ai_control)+"smart_home_api/favourite/controller_favourite/GetFavourite?accountsId="+accountID+"&homeId="+arrayListHome.get(0).getHomeId();
                    url_GetTypeDevices = getResources().getString(R.string.api_ai_control)+"/smart_home_api/devices/controller_devices/GetTypeDevices?accountId="+accountID+"&homeId="+arrayListHome.get(0).getHomeId();
                }else {
                    txtHomeName.setText(default_HomeName);
                    url_json_BoardAndDevice = getResources().getString(R.string.api_ai_control)+"/smart_home_api/board/controller_board/GetBoardActive?uid="+accountID+"&homeId="+default_HomeID;
                    url_json_GetFavourite = getResources().getString(R.string.api_ai_control)+"smart_home_api/favourite/controller_favourite/GetFavourite?accountsId="+accountID+"&homeId="+default_HomeID;
                    url_GetTypeDevices = getResources().getString(R.string.api_ai_control)+"/smart_home_api/devices/controller_devices/GetTypeDevices?accountId="+accountID+"&homeId="+default_HomeID;
                }
            }

        }else {
            mainContainer.setVisibility(View.GONE);
            errorHomeText.setVisibility(View.VISIBLE);
            createHomeText.setVisibility(View.VISIBLE);

            String url_json_Invited = getResources().getString(R.string.api_ai_control)+"/smart_home_api/invite/controller_invite/GetInviteIsActive?accountId="+accountID;
            new AsyncTaskInvited(this).execute(url_json_Invited);


        }
        fragment = new FilterALLFragment(url_json_BoardAndDevice,spinner_filter);
        loadFragmentFilter(fragment);
    }

    @Override
    public void onRequestFailed(String result) {

    }//End End AsyncTask GET Home;

    //Even OnClick view
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.onClickSettingsHome:
                onClickSettingsHome();
                break;
            case R.id.onClickSelectedHome:
                onClickSelectedHome();
                break;
            case R.id.filter_all_devices:

                filter_all_devices.setSelected(true);
                filter_favourite_devices.setSelected(false);
                filter_on_devices.setSelected(false);
                filter_off_devices.setSelected(false);

                fragment = new FilterALLFragment(url_json_BoardAndDevice,spinner_filter);
                loadFragmentFilter(fragment);

                break;
            case R.id.filter_favourite_devices:

                filter_all_devices.setSelected(false);
                filter_favourite_devices.setSelected(true);
                filter_on_devices.setSelected(false);
                filter_off_devices.setSelected(false);

                fragment = new FilterFavoriteFragment(url_json_GetFavourite,spinner_filter);
                loadFragmentFilter(fragment);

                break;
            case R.id.filter_on_devices:

                filter_all_devices.setSelected(false);
                filter_favourite_devices.setSelected(false);
                filter_on_devices.setSelected(true);
                filter_off_devices.setSelected(false);
                fragment = new FilterOnFragment(url_json_BoardAndDevice+"&status=1",spinner_filter);
                loadFragmentFilter(fragment);

                break;
            case R.id.filter_off_devices:

                filter_all_devices.setSelected(false);
                filter_favourite_devices.setSelected(false);
                filter_on_devices.setSelected(false);
                filter_off_devices.setSelected(true);
                fragment = new FilterOffFragment(url_json_BoardAndDevice+"&status=0" ,spinner_filter);
                loadFragmentFilter(fragment);
                break;

            case R.id.Spinner_Filter:
                showSelectFilterDialog();
                break;
            case R.id.createHomeText:
                startActivity(new Intent(getContext(), NewHomeActivity.class));
                break;
            case R.id.RelativeLayout_Invite:
                showJoinFamily();
                break;

                default:
                    break;
        }
    }//End Even OnClick view

    private void showJoinFamily() {
        if (accountsArrayList.size() > 1){
            Bundle args = new Bundle();
            args.putParcelableArrayList("LIST_MODEL_ACCOUNT",accountsArrayList);
            FragmentManager manager = getFragmentManager();
            MultipleJoinsFamilyFragment dialog = new MultipleJoinsFamilyFragment();
            dialog.setArguments(args);
            dialog.show(manager,"MultipleJoinsFamilyFragment");
        }else {
            Bundle args = new Bundle();
            args.putParcelable("MODEL_ACCOUNT",accountsArrayList.get(0));
            FragmentManager manager = getFragmentManager();
            JoinFamilyFragment dialog = new JoinFamilyFragment();
            dialog.setArguments(args);
            dialog.show(manager,"JoinFamilyFragment");
        }
    }

    private void onClickSelectedHome() {
        if (arrayListHome != null && arrayListHome.size() != 0) {
            int REQ_CODE = 5678;
            Bundle args = new Bundle();
            args.putParcelableArrayList("LIST_MODEL_HOME", arrayListHome);
            FragmentManager manager = getFragmentManager();
            SelectHomeFragment dialog = new SelectHomeFragment();
            dialog.setTargetFragment(HomeFragment.this, REQ_CODE);
            dialog.setArguments(args);
            dialog.show(manager, "SelectHomeFragment");
        }
    }

    private void onClickSettingsHome() {
        if (arrayListHome != null && arrayListHome.size() != 0) {
            int REQ_CODE = 5678;
            int position = getContext().getSharedPreferences("PREF_APP_SELECTED_POSITION_HOME", Context.MODE_PRIVATE).getInt("APP_SELECTED_POSITION_HOME", 0);
            Bundle args = new Bundle();
            args.putParcelable("MODEL_HOME", arrayListHome.get(position));
            FragmentManager manager = getFragmentManager();
            SettingsHomeFragment dialog = new SettingsHomeFragment();
            dialog.setTargetFragment(HomeFragment.this, REQ_CODE);
            dialog.setArguments(args);
            dialog.show(manager, "SettingsHomeFragment");
        }
    }

    //Result Selected Home
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5678 && resultCode == RESULT_OK) {
            home = data.getParcelableExtra("GET_MODEL_HOME");
            positionHome = data.getIntExtra("GET_POSITION_HOME",0);
            txtHomeName.setText(home.getHomeName());
            getContext().getSharedPreferences("PREF_APP_SELECTED_HOME_NAME", Context.MODE_PRIVATE).edit().putString("APP_SELECTED_HOME_NAME", home.getHomeName()).apply();
            getContext().getSharedPreferences("PREF_APP_SELECTED_HOME_ID",Context.MODE_PRIVATE).edit().putInt("APP_SELECTED_HOME_ID",home.getHomeId()).apply();
            getContext().getSharedPreferences("PREF_APP_SELECTED_POSITION_HOME",Context.MODE_PRIVATE).edit().putInt("APP_SELECTED_POSITION_HOME",positionHome).apply();
            url_json_BoardAndDevice = getResources().getString(R.string.api_ai_control)+"/smart_home_api/board/controller_board/GetBoardActive?uid="+accountID+"&homeId="+home.getHomeId();
            fragment = new FilterALLFragment(url_json_BoardAndDevice,spinner_filter);
            loadFragmentFilter(fragment);
        }else if (requestCode == 3356 && resultCode == RESULT_OK){
            modelTypeDevices = data.getParcelableExtra("SELECTED_TYPE_DEVICES");
//            getContext().getSharedPreferences("PREF_APP_SELECTED_TYPE_DEVICES", Context.MODE_PRIVATE).edit().putString("APP_SELECTED_TYPE_DEVICES", modelTypeDevices.getTypeDevices()).apply();
            spinner_filter.setText(modelTypeDevices.getTypeDevices());

        }
    }//End Result Selected Home


    //Current DateTime
    private static String getCurrentDateTime(){
        // Default date format
        final String DATE_FORMAT =  "E dd, MMM yyyy HH:mm a";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }//End Current DateTime

    //GET Location
    private boolean getLocation(){
        providerClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null){
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
                    String response = "https://api.openweathermap.org/data/2.5/weather?lat="+latitude+"&lon="+longitude+"&units=metric&appid="+getResources().getString(R.string.api_weather);
                    getWeather(response);
                    resultGetLocation = true;
                }else {
                    resultGetLocation = false;
                }
            }
        });
        return resultGetLocation;
    }//End GET Location

    //GET Weather
    private void getWeather(String response) {
        new AsyncTaskWeather(this).execute(response);
    }//End Get Weather

    @Override
    public void onStart() {
        super.onStart();
        filter_all_devices.setSelected(true);
        getLocation();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    //Refresh
    @Override
    public void onRefresh() {
        Toast.makeText(getContext(), "Refresh", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
                startActivity(new Intent(getContext(),MainActivity.class));
                getActivity().finish();
            }
        }, 2000);
    }//End Refresh

    @Override
    public void onResume() {
        super.onResume();
        getLocation();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void showSelectFilterDialog() {
        int REQ_CODE = 3356;
        DialogFragment dialog = new SelectFilterFragment(url_GetTypeDevices);
        dialog.setTargetFragment(HomeFragment.this,REQ_CODE);
        dialog.show(getFragmentManager(), "SelectFilterFragment");
    }

    //Load Fragment
    private void loadFragmentFilter(Fragment frg){
        if (frg != null){
            getFragmentManager().beginTransaction().replace(R.id.FLayout_Filter_Home, frg).commit();
        }
    }//End Load Fragment

    private static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }

    private void showNotification(ArrayList<ModelAccounts> arrayList) {
        for (ModelAccounts accounts : arrayList){
        createNotificationChannel();
        // Get the layouts to use in the custom notification
        RemoteViews notificationLayout = new RemoteViews(getContext().getPackageName(), R.layout.notification_small);
        RemoteViews notificationLayoutExpanded = new RemoteViews(getContext().getPackageName(), R.layout.notification_large);

            notificationLayout.setTextViewText(R.id.notificationTitle, "You've been invited to join a home");
            notificationLayout.setTextViewText(R.id.notificationEmail, accounts.getAccountsEmail());

            notificationLayoutExpanded.setTextViewText(R.id.txt_notification_home_name, accounts.getModelHome().getHomeName());

        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_alert)
                .setCustomContentView(notificationLayout)
                .setCustomBigContentView(notificationLayoutExpanded)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        //notification manager
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getContext());
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());

        }

    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("SaveInstance_ListHome",arrayListHome);
    }


}
