package com.aimer.smarthome.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.asynctask.AsyncTaskAddNameDevices;
import com.aimer.smarthome.fragment_popup.BackToHomeFragment;
import com.aimer.smarthome.interface_callback.CallBackServiceAddNameDevices;
import com.aimer.smarthome.model.ModelDevices;

public class NewDeviceActivity extends AppCompatActivity implements CallBackServiceAddNameDevices, View.OnClickListener {

    private ImageView mIconNewDevices;
    private TextView mTextNewDevices,txtSkipNewDevice;
    private EditText mEditNewDevices;
    private String url_json ="";
    private String icon_devices ="";
    private int type_devices;
    private ModelDevices modelDevices = new ModelDevices();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_device);

        mIconNewDevices = findViewById(R.id.img_new_devices);
        mTextNewDevices = findViewById(R.id.txt_name_new_devices);
        mEditNewDevices = findViewById(R.id.edt_new_name_devices);
        txtSkipNewDevice = findViewById(R.id.txt_skip_step_new_device);

        icon_devices = getIntent().getStringExtra("ICON_DEVICES");
        int resId = getResourceByFilename(this, icon_devices);
        mIconNewDevices.setImageResource(resId);
        mTextNewDevices.setText(getIntent().getStringExtra("NAME_DEVICES"));
        type_devices = getIntent().getIntExtra("TYPE_DEVICES",0);

        modelDevices = getIntent().getParcelableExtra("ModelDevices");
        mEditNewDevices.setText(modelDevices.getDevicesName());
        mEditNewDevices.setSelection(mEditNewDevices.getText().length());

        txtSkipNewDevice.setOnClickListener(this);



    }

    @Override
    protected void onResume() {
        super.onResume();
        mEditNewDevices.setFocusableInTouchMode(true);
        mEditNewDevices.requestFocus();
    }

    public void onClickLooksPretty(View view){
        if (TextUtils.isEmpty(mEditNewDevices.getText().toString())){
            Toast.makeText(getApplication(), "Empty field not allowed!", Toast.LENGTH_SHORT).show();
        }else {
            String nameDevices = mEditNewDevices.getText().toString().replaceAll(" ", "%20");
            url_json = getResources().getString(R.string.api_ai_control)+"smart_home_api/board/controller_board/UpdateDevices?devices_Id="+modelDevices.getDevicesId()+"&devices_Name="+nameDevices+"&devices_isActive=1&devices_Icon="+icon_devices+"&typeDevices_Id="+type_devices;
            new AsyncTaskAddNameDevices(this).execute(url_json);
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(String result) {
        if (result != null && result.equals("true")){
            FragmentManager manager = getSupportFragmentManager();
            BackToHomeFragment dialog =  new BackToHomeFragment();

            Bundle args = new Bundle();
            args.putString("NAME_DEVICES", mEditNewDevices.getText().toString());
            args.putString("ICON_DEVICES",icon_devices);
            dialog.setArguments(args);
            dialog.show(manager,"BackToHomeFragment");
        }

    }

    @Override
    public void onRequestFailed(String result) {

    }

    public static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_skip_step_new_device:
                onClickSkipStepNewDevice();
                break;
        }
    }

    private void onClickSkipStepNewDevice() {
        Toast.makeText(this, "Skip...!!", Toast.LENGTH_SHORT).show();
    }
}
