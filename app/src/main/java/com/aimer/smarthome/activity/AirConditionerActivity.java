package com.aimer.smarthome.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.aimer.smarthome.R;
import com.aimer.smarthome.custom.SeekBarSemiCircle;



public class AirConditionerActivity extends AppCompatActivity {

    private SeekBarSemiCircle seekBarSemiCircle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_air_conditioner);


        seekBarSemiCircle = findViewById(R.id.SeekBar_id);
        seekBarSemiCircle.setProgressWidth(25);
        seekBarSemiCircle.setArcWidth(25);
        seekBarSemiCircle.setArcColor(R.color.arc_gray);

    }

}
