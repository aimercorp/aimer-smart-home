package com.aimer.smarthome.activity;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.asynctask.AsyncTaskGetHomeName;
import com.aimer.smarthome.interface_callback.CallBackServiceHome;
import com.aimer.smarthome.model.ModelHome;

import java.util.ArrayList;

public class NewHomeActivity extends AppCompatActivity implements CallBackServiceHome {

    private EditText edt_new_home;
    private String url_json ="";
    private int accountID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);

        edt_new_home = findViewById(R.id.edt_new_home);


        SharedPreferences prefs = getSharedPreferences("PREF_APP_FIRST_START",MODE_PRIVATE);
        boolean firstStart = prefs.getBoolean("APP_FIRST_START",true);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("APP_FIRST_START",false);
        editor.apply();

        accountID =  getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).getInt("APP_ACCOUNT_ID", 0);


    }

    public void onClickSoundsPretty(View view){
        if (TextUtils.isEmpty(edt_new_home.getText().toString())){
            Toast.makeText(NewHomeActivity.this, "Empty field not allowed!", Toast.LENGTH_SHORT).show();
        }else {
            String nameHome = edt_new_home.getText().toString().replaceAll(" ", "%20");
            url_json = getResources().getString(R.string.api_ai_control)+"/smart_home_api/home/controller_home/RegisterHome?accountID="+accountID+"&homeName="+nameHome;
            Toast.makeText(NewHomeActivity.this, "Proceed..", Toast.LENGTH_SHORT).show();
            new AsyncTaskGetHomeName(this).execute(url_json);
        }

    }

    public void onClickSkipNewHome(View view){
        startActivity(new Intent(this,MainActivity.class));
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelHome> listHome) {
        if (listHome != null ){
            Log.i("Check data", "" + listHome);
            startActivity(new Intent(this,MainActivity.class));
        }
    }


    @Override
    public void onRequestFailed(String result) {

    }
}
