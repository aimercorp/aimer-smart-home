package com.aimer.smarthome.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.Tag;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.fragment.FavoritesFragment;
import com.aimer.smarthome.fragment.HomeFragment;
import com.aimer.smarthome.fragment.NotificationsFragment;
import com.aimer.smarthome.fragment_popup.SelectChannelFragment;
import com.aimer.smarthome.fragment_popup.SelectFilterFragment;
import com.aimer.smarthome.fragment_popup.SettingFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {

    private static String TAG = "MainActivity";
    private static final int PERMISSION_ID = 1;
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Setup navigation
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);


        if (checkPermissions() && isLocationEnabled()){
        }else {
            requestPermissions();
        }
        checkInternetConnection();

    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void checkInternetConnection(){
        if (isConnected()){
            checkStepActionApplication();
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No internet access");
            builder.setMessage("recheck if there is an internet connection ......");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // Do something
                    startActivity(new Intent(getApplication(),MainActivity.class));
                    finish();
                }
            });
            builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // Do something
                    android.os.Process.killProcess(android.os.Process.myPid());
                    finish();
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void checkStepActionApplication() {
        boolean verifyLogin = getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).getBoolean("APP_LOGIN",false);
        boolean firstStart = getSharedPreferences("PREF_APP_FIRST_START",MODE_PRIVATE).getBoolean("APP_FIRST_START",true);

        if (verifyLogin){
            if (firstStart){
                Toast.makeText(this, "FIRST START", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this,NewHomeActivity.class));
            }else {
                loadFragment(new HomeFragment());
            }
        }else {
            startActivity(new Intent(this, SignInActivity.class));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setting_item_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
//            case R.id.action_New_Controller:
//                startActivity(new Intent(getApplication(), NewControllerActivity.class));
//                return true;
//            case R.id.action_Searching_Controller:
//                return true;
//            case R.id.action_Select_Channel:
//                FragmentManager manager = getSupportFragmentManager();
//                SelectChannelFragment dialog = new SelectChannelFragment();
//                dialog.show(manager,"SelectChannelFragment");
//                return true;
//            case R.id.action_Account:
//                startActivity(new Intent(getApplication(),AccountActivity.class));
//                return true;
//            case R.id.action_New_Home:
//                startActivity(new Intent(getApplication(),NewHomeActivity.class));
//                return true;
            case R.id.action_Setting:
                showFragmentSetting();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void showFragmentSetting() {
        SettingFragment fragment = new SettingFragment();
        fragment.show(getSupportFragmentManager(), TAG);
    }


    private boolean checkPermissions(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }

    private boolean isLocationEnabled(){
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    private boolean isConnected(){
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    private void requestPermissions(){
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ID
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // Granted. Start getting the location information
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()){
            case R.id.bottomNavigationHomeMenuId:
                fragment = new HomeFragment();
                break;
            case R.id.bottomNavigationFavoriteMenuId:
                fragment = new FavoritesFragment();
                break;
            case R.id.bottomNavigationCCTVMenuId:
                break;
            case R.id.bottomNavigationNotificationsMenuId:
                fragment = new NotificationsFragment();
                break;
        }
        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.FrameLayout, fragment)
                    .commit();
            return true;
        }
        return false;
    }


}
