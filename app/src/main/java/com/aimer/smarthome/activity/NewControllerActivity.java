package com.aimer.smarthome.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aimer.esptouch.EsptouchTask;
import com.aimer.esptouch.IEsptouchListener;
import com.aimer.esptouch.IEsptouchResult;
import com.aimer.esptouch.IEsptouchTask;
import com.aimer.esptouch.task.__IEsptouchTask;
import com.aimer.esptouch.util.ByteUtil;
import com.aimer.esptouch.util.TouchNetUtil;
import com.aimer.smarthome.R;
import com.aimer.smarthome.adapter.AdapterSpinnerHome;
import com.aimer.smarthome.asynctask.AsyncTaskGetHomeName;
import com.aimer.smarthome.doget.DoGet;
import com.aimer.smarthome.interface_callback.CallBackServiceHome;
import com.aimer.smarthome.model.ModelHome;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NewControllerActivity extends AppCompatActivity implements OnClickListener , CallBackServiceHome {
    private static final String TAG = "NewControllerActivity";

    private static final int REQUEST_PERMISSION = 0x01;

    private static final int MENU_ITEM_ABOUT = 0;

    private TextView mApSsidTV;
    private TextView mApBssidTV;
    private EditText mApPasswordET;
    private EditText mDeviceName;
    private EditText mDeviceCountET;
    private RadioGroup mPackageModeGroup;
    private TextView mMessageTV;
    private Button mConfirmBtn;
    private Toolbar toolbar_SettingsNewController;
    private AdapterSpinnerHome adapterSpinnerHome;

    private IEsptouchListener myListener = new IEsptouchListener() {

        @Override
        public void onEsptouchResultAdded(final IEsptouchResult result) {
            onEsptoucResultAddedPerform(result);
        }
    };

    private EsptouchAsyncTask4 mTask;

    private boolean mReceiverRegistered = false;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }

            WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                    .getSystemService(WIFI_SERVICE);
            assert wifiManager != null;

            switch (action) {
                case WifiManager.NETWORK_STATE_CHANGED_ACTION:
                    WifiInfo wifiInfo;
                    if (intent.hasExtra(WifiManager.EXTRA_WIFI_INFO)) {
                        wifiInfo = intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);
                    } else {
                        wifiInfo = wifiManager.getConnectionInfo();
                    }
                    onWifiChanged(wifiInfo);
                    break;
                case LocationManager.PROVIDERS_CHANGED_ACTION:
                    onWifiChanged(wifiManager.getConnectionInfo());
                    break;
            }
        }
    };

    private boolean mDestroyed = false;

    private Spinner spinner_Selected_Home;
    private ArrayList<ModelHome> arrayListHome = null;
    private int accountID;
    private String url_json ="";
    private String home_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_controller);

        toolbar_SettingsNewController = findViewById(R.id.Toolbar_Settings_NewController);
        setSupportActionBar(toolbar_SettingsNewController);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_SettingsNewController.setTitle("");
        toolbar_SettingsNewController.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        toolbar_SettingsNewController.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        spinner_Selected_Home = findViewById(R.id.Spinner_Selected_Home);
        arrayListHome = new ArrayList<>();
        accountID =  getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).getInt("APP_ACCOUNT_ID", 0);
        url_json = getResources().getString(R.string.api_ai_control)+"/smart_home_api/home/controller_home/RegisterHome?accountID="+accountID+"&homeName=";

        new AsyncTaskGetHomeName(this).execute(url_json);

        mApSsidTV = findViewById(R.id.ap_ssid_text);
        mApBssidTV = findViewById(R.id.ap_bssid_text);
        mApPasswordET = findViewById(R.id.ap_password_edit);
        mDeviceName = findViewById(R.id.device_name_edit);
        mDeviceName.setText("Bedroom");
        mDeviceName.setSelection(mDeviceName.getText().length());
        mDeviceCountET = findViewById(R.id.device_count_edit);
        mDeviceCountET.setText("1");
        mPackageModeGroup = findViewById(R.id.package_mode_group);
        mMessageTV = findViewById(R.id.message);
        mConfirmBtn = findViewById(R.id.confirm_btn);
        mConfirmBtn.setEnabled(false);
        mConfirmBtn.setOnClickListener(this);

        if (isSDKAtLeastP()) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                String[] permissions = {
                        Manifest.permission.ACCESS_COARSE_LOCATION
                };

                requestPermissions(permissions, REQUEST_PERMISSION);
            } else {
                registerBroadcastReceiver();
            }

        } else {
            registerBroadcastReceiver();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        mApPasswordET.setSelection(0);
        mApPasswordET.setFocusableInTouchMode(true);
        mApPasswordET.requestFocus();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!mDestroyed) {
                        registerBroadcastReceiver();
                    }
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mDestroyed = true;
        if (mReceiverRegistered) {
            unregisterReceiver(mReceiver);
        }
    }


    private boolean isSDKAtLeastP() {
        return Build.VERSION.SDK_INT >= 28;
    }

    private void registerBroadcastReceiver() {
        IntentFilter filter = new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        if (isSDKAtLeastP()) {
            filter.addAction(LocationManager.PROVIDERS_CHANGED_ACTION);
        }
        registerReceiver(mReceiver, filter);
        mReceiverRegistered = true;
    }

    private void onWifiChanged(WifiInfo info) {
        boolean disconnected = info == null
                || info.getNetworkId() == -1
                || "<unknown ssid>".equals(info.getSSID());
        if (disconnected) {
            mApSsidTV.setText("");
            mApSsidTV.setTag(null);
            mApBssidTV.setText("");
            mMessageTV.setText(R.string.no_wifi_connection);
            mConfirmBtn.setEnabled(false);

            if (isSDKAtLeastP()) {
                checkLocation();
            }

            if (mTask != null) {
                mTask.cancelEsptouch();
                mTask = null;
                new AlertDialog.Builder(NewControllerActivity.this)
                        .setMessage(R.string.configure_wifi_change_message)
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
            }
        } else {
            String ssid = info.getSSID();
            if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
                ssid = ssid.substring(1, ssid.length() - 1);
            }
            mApSsidTV.setText(ssid);
            mApSsidTV.setTag(ByteUtil.getBytesByString(ssid));
            byte[] ssidOriginalData = TouchNetUtil.getOriginalSsidBytes(info);
            mApSsidTV.setTag(ssidOriginalData);

            String bssid = info.getBSSID();
            mApBssidTV.setText(bssid);

            mConfirmBtn.setEnabled(true);
            mMessageTV.setText("");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int frequency = info.getFrequency();
                if (frequency > 4900 && frequency < 5900) {
                    // Connected 5G wifi. Device does not support 5G
                    mMessageTV.setText(R.string.wifi_5g_message);
                }
            }
        }
    }

    private void checkLocation() {
        boolean enable;
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager == null) {
            enable = false;
        } else {
            boolean locationGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean locationNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            enable = locationGPS || locationNetwork;
        }

        if (!enable) {
            mMessageTV.setText(R.string.location_disable_message);
        }
    }

    @Override
    public void onClick(View v) {
        if (TextUtils.isEmpty(mApPasswordET.getText().toString())){
            Toast.makeText(getApplication(), "Empty field not allowed!", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(mDeviceName.getText().toString())){
            Toast.makeText(getApplication(), "Empty field not allowed!", Toast.LENGTH_SHORT).show();
        }
        else {
            if (v == mConfirmBtn) {
                byte[] ssid = mApSsidTV.getTag() == null ? ByteUtil.getBytesByString(mApSsidTV.getText().toString())
                        : (byte[]) mApSsidTV.getTag();
                byte[] password = ByteUtil.getBytesByString(mApPasswordET.getText().toString());
                byte [] bssid = TouchNetUtil.parseBssid2bytes(mApBssidTV.getText().toString());
                byte[] deviceCount = mDeviceCountET.getText().toString().getBytes();
                byte[] broadcast = {(byte) (mPackageModeGroup.getCheckedRadioButtonId() == R.id.package_broadcast
                        ? 1 : 0)};

                if(mTask != null) {
                    mTask.cancelEsptouch();
                }
                mTask = new EsptouchAsyncTask4(this);
                mTask.execute(ssid, bssid, password, deviceCount, broadcast);
            }
        }

    }

    private void onEsptoucResultAddedPerform(final IEsptouchResult result) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                String text = result.getBssid() + " is connected to the wifi";
                Toast.makeText(NewControllerActivity.this, text,
                        Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelHome> listHome) {
        if (listHome != null && listHome.size() != 0 ){
            Log.i("Check data", "" + listHome);
            this.arrayListHome = listHome;
            adapterSpinnerHome = new AdapterSpinnerHome(this,arrayListHome);
            spinner_Selected_Home.setAdapter(adapterSpinnerHome);
            selectedHome();
        }else {
            mConfirmBtn.setEnabled(false);
        }
    }

    private void selectedHome() {
        spinner_Selected_Home.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                home_id = String.valueOf(arrayListHome.get(position).getHomeId());
                Toast.makeText(NewControllerActivity.this, arrayListHome.get(position).getHomeName()+"", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onRequestFailed(String result) {

    }

    public class EsptouchAsyncTask4 extends AsyncTask<byte[], Void, List<IEsptouchResult>> {
        private WeakReference<NewControllerActivity> mActivity;

        // without the lock, if the user tap confirm and cancel quickly enough,
        // the bug will arise. the reason is follows:
        // 0. task is starting created, but not finished
        // 1. the task is cancel for the task hasn't been created, it do nothing
        // 2. task is created
        // 3. Oops, the task should be cancelled, but it is running
        private final Object mLock = new Object();
        private ProgressDialog mProgressDialog;
        private AlertDialog mResultDialog;
        private IEsptouchTask mEsptouchTask;


        EsptouchAsyncTask4(NewControllerActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        void cancelEsptouch() {
            cancel(true);
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
            if (mResultDialog != null) {
                mResultDialog.dismiss();
            }
            if (mEsptouchTask != null) {
                mEsptouchTask.interrupt();
            }
        }

        @Override
        protected void onPreExecute() {
            Activity activity = mActivity.get();
            mProgressDialog = new ProgressDialog(activity);
            mProgressDialog.setMessage(activity.getString(R.string.configuring_message));
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    synchronized (mLock) {
                        if (__IEsptouchTask.DEBUG) {
                            Log.i(TAG, "progress dialog back pressed canceled");
                        }
                        if (mEsptouchTask != null) {
                            mEsptouchTask.interrupt();
                        }
                    }
                }
            });
            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, activity.getText(android.R.string.cancel),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            synchronized (mLock) {
                                if (__IEsptouchTask.DEBUG) {
                                    Log.i(TAG, "progress dialog cancel button canceled");
                                }
                                if (mEsptouchTask != null) {
                                    mEsptouchTask.interrupt();
                                }
                            }
                        }
                    });
            mProgressDialog.show();
        }

        @Override
        protected List<IEsptouchResult> doInBackground(byte[]... params) {
            NewControllerActivity activity = mActivity.get();
            int taskResultCount;
            synchronized (mLock) {
                byte[] apSsid = params[0];
                byte[] apBssid = params[1];
                byte[] apPassword = params[2];
                byte[] deviceCountData = params[3];
                byte[] broadcastData = params[4];
                taskResultCount = deviceCountData.length == 0 ? -1 : Integer.parseInt(new String(deviceCountData));
                Context context = activity.getApplicationContext();
                mEsptouchTask = new EsptouchTask(apSsid, apBssid, apPassword, context);
                mEsptouchTask.setPackageBroadcast(broadcastData[0] == 1);
                mEsptouchTask.setEsptouchListener(activity.myListener);
            }
            return mEsptouchTask.executeForResults(taskResultCount);
        }

        @Override
        protected void onPostExecute(List<IEsptouchResult> result) {
            String URL_JSON = "";
            NewControllerActivity activity = mActivity.get();
            mProgressDialog.dismiss();
            if (result == null) {
                mResultDialog = new AlertDialog.Builder(activity)
                        .setMessage(R.string.configure_result_failed_port)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
                mResultDialog.setCanceledOnTouchOutside(false);
                return;
            }

            IEsptouchResult firstResult = result.get(0);
            // check whether the task is cancelled and no results received
            if (!firstResult.isCancelled()) {
                // the task received some results including cancelled while
                // executing before receiving enough results
                if (firstResult.isSuc()) {
                    ArrayList<CharSequence> resultMsgList = new ArrayList<>(result.size());
                    for (IEsptouchResult touchResult : result) {
                        String message = activity.getString(R.string.configure_result_success_item,
                                touchResult.getBssid(), touchResult.getInetAddress().getHostAddress());
                        resultMsgList.add(message);
                        String getDeviceName = mDeviceName.getText().toString().replaceAll(" ", "%20");
                        Toast.makeText(activity, getDeviceName+"", Toast.LENGTH_SHORT).show();
                        URL_JSON = getResources().getString(R.string.api_ai_control)+"smart_home_api/home/controller_home/UpdateBoardToHome?boardID="+touchResult.getBssid()+"&homeID="+home_id+"&name="+getDeviceName;
                        new DoGet(this).execute(URL_JSON);
                    }

                    CharSequence[] items = new CharSequence[resultMsgList.size()];
                    mResultDialog = new AlertDialog.Builder(activity)
                            .setTitle(R.string.configure_result_success)
                            .setItems(resultMsgList.toArray(items), null)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                    mResultDialog.setCanceledOnTouchOutside(false);
                    onBackPressed();
                } else {
                    mResultDialog = new AlertDialog.Builder(activity)
                            .setMessage(R.string.configure_result_failed)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                    mResultDialog.setCanceledOnTouchOutside(false);
                }
            }

            activity.mTask = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
