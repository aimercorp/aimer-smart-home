package com.aimer.smarthome.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.asynctask.AsyncTaskAccount;
import com.aimer.smarthome.interface_callback.CallBackServiceAccount;
import com.aimer.smarthome.model.ModelAccounts;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class AccountActivity extends AppCompatActivity implements CallBackServiceAccount {
    private String url = "";
    private String userLoginType;
    private GoogleSignInClient mGoogleSignInClient;
    private ImageButton user_profile_photo;
    private TextView user_profile_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        user_profile_name = findViewById(R.id.user_profile_name);
        user_profile_photo = findViewById(R.id.user_profile_photo);

        userLoginType = getSharedPreferences("PREF_APP_UserLoginType", Context.MODE_PRIVATE).getString("APP_UserLoginType","");
        String getToken = getSharedPreferences("PREF_APP_TOKEN", Context.MODE_PRIVATE).getString("APP_TOKEN","");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("167802330894-2ei7s3o95p1qet0jvi1o11mivbdf09vj.apps.googleusercontent.com")
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

//        Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(user_profile_photo);
        url = getResources().getString(R.string.api_ai_control)+"smart_home_api/user/controller_user/RegisterLogin?idToken="+getToken+"";
        if (getToken != null){
            new AsyncTaskAccount(this).execute(url);
        }

    }

    public void onClickSignOut(View view){
        switch (userLoginType){
            case "google":
                signOutGoogle();
                break;
            case "facebook":
                signOutFacebook();
                break;
        }

    }

    public void signOutGoogle() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(AccountActivity.this, "Sign Out Google", Toast.LENGTH_SHORT).show();
                        getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).edit().putBoolean("APP_LOGIN", false).apply();
                        getSharedPreferences("PREF_APP_TOKEN", Context.MODE_PRIVATE).edit().putString("APP_TOKEN", "").apply();
                        getSharedPreferences("PREF_APP_SELECTED_HOME_NAME", Context.MODE_PRIVATE).edit().putString("APP_SELECTED_HOME_NAME", null).apply();
                        getSharedPreferences("PREF_APP_SELECTED_HOME_ID",Context.MODE_PRIVATE).edit().putInt("APP_SELECTED_HOME_ID",0).apply();
                        getSharedPreferences("PREF_APP_ACCOUNT_NAME", Context.MODE_PRIVATE).edit().putString("APP_ACCOUNT_NAME", null).apply();
                        getSharedPreferences("PREF_APP_ACCOUNT_EMAIL", Context.MODE_PRIVATE).edit().putString("APP_ACCOUNT_EMAIL", null).apply();
                        startActivity(new Intent(getApplication(),MainActivity.class));
                    }
                });
    }

    public void signOutFacebook(){
        FacebookSdk.sdkInitialize(this);
        LoginManager.getInstance().logOut();
        Toast.makeText(AccountActivity.this, "Sign Out Facebook", Toast.LENGTH_SHORT).show();
        getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).edit().putBoolean("APP_LOGIN", false).apply();
        startActivity(new Intent(getApplication(),MainActivity.class));
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ModelAccounts accounts) {
        if (accounts != null ){
            Log.i("Check data", "" + accounts);
            setProfileUsers(accounts);
        }
    }

    private void setProfileUsers(ModelAccounts accounts) {
        user_profile_name.setText(accounts.getAccountsName());
    }

    @Override
    public void onRequestFailed(String result) {

    }
}
