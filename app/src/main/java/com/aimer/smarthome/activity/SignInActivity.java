package com.aimer.smarthome.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.asynctask.AsyncTaskAccount;
import com.aimer.smarthome.interface_callback.CallBackServiceAccount;
import com.aimer.smarthome.model.ModelAccounts;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener , CallBackServiceAccount {

    private static final int RC_SIGN_IN = 321;
    private CallbackManager callbackManager;
    private SignInButton signInButton;
    private LoginButton loginButton;
    private GoogleSignInClient mGoogleSignInClient;
    private AccessTokenTracker accessTokenTracker;
    private String url_json = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        loginButton = findViewById(R.id.login_button);
        signInButton = findViewById(R.id.sign_in_button);

        setLoginFacebook();
        setLoginGoogle();
    }

    private void setLoginGoogle() {
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("167802330894-2ei7s3o95p1qet0jvi1o11mivbdf09vj.apps.googleusercontent.com")
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void setLoginFacebook() {

        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions(Arrays.asList("email,public_profile"));
        loginButton.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Toast.makeText(SignInActivity.this, "onSuccess", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Log.v("LoginActivity", "cancel");
                        Toast.makeText(SignInActivity.this, "cancel", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.v("LoginActivity", exception.getMessage());
                        Toast.makeText(SignInActivity.this, "FacebookException", Toast.LENGTH_SHORT).show();
                    }
                });
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken != null){
                    loadUserProfile(currentAccessToken);
                }else {

                }
            }
        };

    }



    private void loadUserProfile(AccessToken newAccessToken){
        GraphRequest request = GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");
                    String email = object.getString("email");
                    String profile_id = object.getString("id");
                    String image_url = "https://graph.facebook.com/"+profile_id+"/picture?type=normal";
                    url_json = getResources().getString(R.string.api_ai_control)+"smart_home_api/user/controller_user/RegisterLogin?email="+email+"&name="+first_name+"||"+last_name+"&idToken="+profile_id+"";
                    socialLoginHelper(url_json);
                    getSharedPreferences("PREF_APP_UserLoginType", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "facebook").apply();
                    getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).edit().putBoolean("APP_LOGIN", true).apply();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields","first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();
    }


    public void onClickLogin(View view){
        startActivity(new Intent(this,NewHomeActivity.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signInGoogle();
                break;
//            case R.id.button_sign_out:
//                signOut();
//                break;
        }
    }

    private void signInGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void signOutGoogle() {
        mGoogleSignInClient.signOut()
            .addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);

        }else {

        }
    }


    //Get Profile Google
    @SuppressLint("LongLogTag")
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                String personGivenName = account.getGivenName();
                String personFamilyName = account.getFamilyName();
                String personEmail = account.getEmail();
                String personId = account.getId();
                Uri personPhoto = account.getPhotoUrl();
            }
            updateUI(account);
        } catch (ApiException e) {

            Log.e("Login Google", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(GoogleSignInAccount account) {
        String fullName = account.getDisplayName().replaceAll(" ", "%20");

        url_json = getResources().getString(R.string.api_ai_control)+"smart_home_api/user/controller_user/RegisterLogin?email="+account.getEmail()+"&name="+fullName+"&idToken="+account.getId()+"&accountId=";
        socialLoginHelper(url_json);

        getSharedPreferences("PREF_APP_UserLoginType", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "google").apply();
        getSharedPreferences("PREF_APP_TOKEN", Context.MODE_PRIVATE).edit().putString("APP_TOKEN", account.getId()).apply();
    }

    public void socialLoginHelper(String url){
//        new DoGet(this).execute(url);
        new AsyncTaskAccount(this).execute(url);

    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ModelAccounts accounts) {
        if (accounts != null ){
            Log.i("Check data", "" + accounts);
            getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).edit().putInt("APP_ACCOUNT_ID", accounts.getAccountsId()).apply();
            getSharedPreferences("PREF_APP_ACCOUNT_NAME", Context.MODE_PRIVATE).edit().putString("APP_ACCOUNT_NAME", accounts.getAccountsName()).apply();
            getSharedPreferences("PREF_APP_ACCOUNT_EMAIL", Context.MODE_PRIVATE).edit().putString("APP_ACCOUNT_EMAIL", accounts.getAccountsEmail()).apply();
            getSharedPreferences("PREF_APP_LOGIN", Context.MODE_PRIVATE).edit().putBoolean("APP_LOGIN", true).apply();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }
}
