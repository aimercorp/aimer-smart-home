package com.aimer.smarthome.fragment_popup;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.aimer.smarthome.R;
import com.aimer.smarthome.activity.MainActivity;
import com.aimer.smarthome.doget.DoGet;
import com.aimer.smarthome.fragment.HomeFragment;
import com.aimer.smarthome.fragment_filter.FilterALLFragment;
import com.aimer.smarthome.fragment_filter.FilterFavoriteFragment;
import com.aimer.smarthome.model.ModelDevices;
import com.aimer.smarthome.mqtt.MqttHelper;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.UnsupportedEncodingException;

public class PowerPushFragment extends DialogFragment implements View.OnClickListener {

    //Set TAG
    private static final String TAG ="PowerPushFragment";

    //REQUEST_CODE
    private static int REQUEST_CODE = 1001;

    //MqttHelper
    private MqttHelper MqttClient;
    private MqttAndroidClient client;
    private String android_id;

    //Toolbar
    private Toolbar mToolbar;

    //TextView
    private TextView mNameDevice;
    private TextView mState;

    //ImageView
    private ImageView mImageState;

    //ModelDevices
    private ModelDevices devices = new ModelDevices();

    //Fragment
    private Fragment fragment = this.getTargetFragment();

    public PowerPushFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        setRetainInstance(true);
        if (getArguments() != null){
            devices = getArguments().getParcelable("MODEL_DEVICES");
        }
        if (savedInstanceState == null){
            fragment = this.getTargetFragment();
            android_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            MqttClient = new MqttHelper();
            client = MqttClient.getMqttClient(getContext(), android_id);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_power_push, container, false);

        //setup toolbar
        mToolbar = view.findViewById(R.id.Toolbar_Power_Push);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_round_close));
        //set on click
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickClose();
            }
        });


//        if (savedInstanceState != null) {
//            android_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//            MqttClient = new MqttHelper();
//            client = MqttClient.getMqttClient(getContext(), android_id);
//            devices = savedInstanceState.getParcelable("SaveInstance_Devices");
//            REQUEST_CODE = savedInstanceState.getInt("REQUEST_CODE");
//            fragment = getFragmentManager().getFragment(savedInstanceState, "SaveInstance_Fragment");
//        }else {
//            fragment = this.getTargetFragment();
//            android_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//            MqttClient = new MqttHelper();
//            client = MqttClient.getMqttClient(getContext(), android_id);
//        }

        //setup name
        mNameDevice = view.findViewById(R.id.PowerPush_nameDevice);
        mNameDevice.setText(devices.getDevicesName());
        //setup image
        mImageState = view.findViewById(R.id.PowerPush_Image);
        mImageState.setOnClickListener(this);
        //setup state
        mState = view.findViewById(R.id.PowerPush_State);

        if (devices.getDevicesStatus().equals("1")){
            int imgId = getResourceByFilename(getContext(),"power_push_on");
            mImageState.setImageResource(imgId);
            mState.setText("เปิด");
        }else {
            int imgId = getResourceByFilename(getContext(),"power_push_off");
            mImageState.setImageResource(imgId);
            mState.setText("ปิด");
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            android_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            MqttClient = new MqttHelper();
            client = MqttClient.getMqttClient(getContext(), android_id);
            devices = savedInstanceState.getParcelable("SaveInstance_Devices");
            REQUEST_CODE = savedInstanceState.getInt("REQUEST_CODE");
            fragment = getFragmentManager().getFragment(savedInstanceState, "SaveInstance_Fragment");
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("DEBUG", "onResume of PowerPushFragment");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("DEBUG", "onPause of PowerPushFragment");
        dismiss();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    void onClickClose() {
        if (fragment == null) {
            dismiss();
            startActivity(new Intent(getContext(),MainActivity.class));

            return;
        }else {
            Intent intent = new Intent();
            fragment.onActivityResult(REQUEST_CODE, Activity.RESULT_OK, intent);
        }
        dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                Toast.makeText(getContext(), "Back", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        };
    }

    @Override
    public void onClick(View v) {
        String urlJsonUpdateDevices = "";
        switch (v.getId()){
            case R.id.PowerPush_Image:
                if (devices.getDevicesStatus().equals("1")){
                    String JsonFormat = "{\"deviceId\":\""+devices.getModelBoard().getBoardMacAddress()+"\",\"action\":\"action.devices.commands.relay.OnOff\",\"value\":{\"on\":0,\"pin\":"+devices.getModelPin().getPin()+"}}";
                    sendEvent(JsonFormat,devices.getModelBoard().getBoardMacAddress()+"/ESP/RELAY");
                    urlJsonUpdateDevices = getResources().getString(R.string.api_ai_control)+"/smart_home_api/devices/controller_devices/UpdateDevices?id="+devices.getDevicesId()+"&status=0";
                    devices.setDevicesStatus("0");
                    int imgId = getResourceByFilename(getContext(),"power_push_off");
                    mImageState.setImageResource(imgId);
                    mState.setText("ปิด");
                }else {
                    String JsonFormat = "{\"deviceId\":\""+devices.getModelBoard().getBoardMacAddress()+"\",\"action\":\"action.devices.commands.relay.OnOff\",\"value\":{\"on\":1,\"pin\":"+devices.getModelPin().getPin()+"}}";
                    sendEvent(JsonFormat,devices.getModelBoard().getBoardMacAddress()+"/ESP/RELAY");
                    urlJsonUpdateDevices = getResources().getString(R.string.api_ai_control)+"/smart_home_api/devices/controller_devices/UpdateDevices?id="+devices.getDevicesId()+"&status=1";
                    devices.setDevicesStatus("1");
                    int imgId = getResourceByFilename(getContext(),"power_push_on");
                    mImageState.setImageResource(imgId);
                    mState.setText("เปิด");
                }
                new DoGet(this).execute(urlJsonUpdateDevices);
                break;
        }

    }

    //Send Event to board
    private void sendEvent(String JsonFormat,String Topic)
    {
        try {
            MqttClient.publishMessage(client, JsonFormat, 1, Topic);
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }//End Send Event to board

    private static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        try {
            setTargetFragment(null, -1);
            outState.putParcelable("SaveInstance_Devices",devices);
            outState.putInt("REQUEST_CODE",REQUEST_CODE);
            getFragmentManager().putFragment(outState, "SaveInstance_Fragment", fragment);


            if (0 < getFragmentManager().getBackStackEntryCount()) {
                getFragmentManager().putFragment(outState, "mContent", this);
            }
        } catch (Exception ex) {
            Log.i(TAG,ex.toString());
        }

    }


}
