package com.aimer.smarthome.fragment_popup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.activity.MainActivity;

public class BackToHomeFragment extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "BackToHomeFragment";
    private ImageView mImgNewDevice;
    private TextView mTxtNameNewDevices;
    private Button mBtnBackToHome;

    public BackToHomeFragment() {

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_back_to_home, container, false);

        mImgNewDevice = view.findViewById(R.id.img_new_devices_BackToHome);
        mTxtNameNewDevices = view.findViewById(R.id.txt_name_new_devices_BackToHome);
        mBtnBackToHome = view.findViewById(R.id.btn_Back_To_Home);
        mBtnBackToHome.setOnClickListener(this);

        int resId = getResourceByFilename(getContext(), getArguments().getString("ICON_DEVICES"));
        mImgNewDevice.setImageResource(resId);
        mTxtNameNewDevices.setText(getArguments().getString("NAME_DEVICES"));


        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Back_To_Home:
                startActivity(new Intent(getContext(), MainActivity.class));
//                FragmentManager manager = getFragmentManager();
//                SelectChannelFragment dialog =  new SelectChannelFragment();
//                dialog.show(manager,"SelectChannelFragment");
                break;
        }
    }

    public static int getResourceByFilename(Context context, String filename) {
        return context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
    }
}
