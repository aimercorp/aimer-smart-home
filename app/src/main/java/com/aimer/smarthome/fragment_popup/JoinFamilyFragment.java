package com.aimer.smarthome.fragment_popup;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.aimer.smarthome.R;
import com.aimer.smarthome.activity.MainActivity;
import com.aimer.smarthome.doget.DoGet;
import com.aimer.smarthome.model.ModelAccounts;

import java.util.ArrayList;


public class JoinFamilyFragment extends DialogFragment implements View.OnClickListener {


    private ModelAccounts accounts;

    private TextView homeName;
    private TextView email;
    private TextView name;

    private Button mBtnJoinFamily;

    private int accountID;

    public JoinFamilyFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        setRetainInstance(true);

        if (getArguments() != null){
            accounts = getArguments().getParcelable("MODEL_ACCOUNT");
            accountID =  getContext().getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).getInt("APP_ACCOUNT_ID", 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_join_family, container, false);
            homeName = view.findViewById(R.id.txt_invite_home_name);
            email = view.findViewById(R.id.txt_invite_email);
            name = view.findViewById(R.id.txt_invite_name);
            mBtnJoinFamily = view.findViewById(R.id.btn_join_family);

            homeName.setText(accounts.getModelHome().getHomeName());
            email.setText(accounts.getAccountsEmail());
            name.setText(accounts.getAccountsName());

            mBtnJoinFamily.setOnClickListener(this);

        return view;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                Toast.makeText(getContext(), "Back", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_join_family:
                String url = getResources().getString(R.string.api_ai_control)+"/smart_home_api/invite/controller_invite/isAccept?accountId="+accountID+"&homeId="+accounts.getModelHome().getHomeId();
                new DoGet(this).execute(url);
                dismiss();
                startActivity(new Intent(getContext(),MainActivity.class));
                break;
        }
    }
}
