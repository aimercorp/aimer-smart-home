package com.aimer.smarthome.fragment_popup;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.activity.MainActivity;
import com.aimer.smarthome.adapter.AdapterRecyclerViewSelectBoard;
import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.utils.ItemClickListenerSelectBoard;

import java.util.ArrayList;

public class SelectBoardFragment extends DialogFragment implements ItemClickListenerSelectBoard{

    private String TAG ="SelectBoardFragment";
    private ArrayList<ModelBoard> modelBoards;
    private RecyclerView recyclerView;
    private AdapterRecyclerViewSelectBoard mAdapter;

    public SelectBoardFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialogTheme);
        modelBoards = getArguments().getParcelableArrayList("LIST_MODEL_BOARD");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_select_board, container, false);
        recyclerView = view.findViewById(R.id.RecyclerView_select_board);
        mAdapter = new AdapterRecyclerViewSelectBoard(getContext(),modelBoards,this);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawableResource(R.color.TRANSPARENT);
        }
    }


    @Override
    public void itemClicked(int position) {
        Bundle args = new Bundle();
        args.putInt("ModelBoard_Position",position);
        FragmentManager manager = getFragmentManager();
        SelectChannelFragment dialog =  new SelectChannelFragment();
        dialog.setArguments(args);
        dialog.show(manager,"SelectChannelFragment");
        getContext().getSharedPreferences("PREF_APP_SELECTED_BOARD", Context.MODE_PRIVATE).edit().putBoolean("APP_SELECTED_BOARD", true).apply();
        dismiss();
    }
}
