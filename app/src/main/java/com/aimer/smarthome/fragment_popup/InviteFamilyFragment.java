package com.aimer.smarthome.fragment_popup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.aimer.smarthome.R;
import com.aimer.smarthome.asynctask.AsyncTaskInviteFamily;
import com.aimer.smarthome.interface_callback.CallBackServiceInviteFamily;
import com.aimer.smarthome.model.ModelAccounts;
import com.aimer.smarthome.model.ModelHome;

import java.util.regex.Pattern;


public class InviteFamilyFragment extends DialogFragment implements View.OnClickListener , CallBackServiceInviteFamily {

    //String
    private static final String TAG ="InviteFamilyFragment";
    private String url_json = "";

    //EditText
    private EditText mEmail;

    //TextView
    private TextView mCancel;
    private TextView mAdd;

    //ModelHome
    private ModelHome modelHome = new ModelHome();


    public InviteFamilyFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            modelHome = getArguments().getParcelable("MODEL_HOME");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invite_family, container, false);
        mEmail = view.findViewById(R.id.edt_invite_family);
        mAdd = view.findViewById(R.id.btn_InviteFamily);
        mCancel = view.findViewById(R.id.btn_cancel);


        mAdd.setOnClickListener(this);
        mCancel.setOnClickListener(this);


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_InviteFamily:
                onClickInviteFamily();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }

    private void onClickInviteFamily() {
        int accountID =  getContext().getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).getInt("APP_ACCOUNT_ID", 0);
        if (checkValidEmailFormatEntered() != null){
            url_json = getResources().getString(R.string.api_ai_control)+"smart_home_api/user/controller_user/RegisterLogin?email="+checkValidEmailFormatEntered()+"&name=&idToken=&accountId="+accountID+"&InviteHomeId="+modelHome.getHomeId();
            new AsyncTaskInviteFamily(this).execute(url_json);
        }

    }

    private String checkValidEmailFormatEntered(){
        String getEmail = mEmail.getText().toString().trim().replaceAll(" ", "");
        if (getEmail.isEmpty()){
            mEmail.setError("Empty field not allowed!");
            return null;
        }else if (!Patterns.EMAIL_ADDRESS.matcher(getEmail).matches()){
            mEmail.setError("Please enter a valid email address");
            return null;
        }else {
            mEmail.setError(null);
            return getEmail;
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteModelAccounts(ModelAccounts accounts) {
        if (accounts != null){
            Log.i("Check data accounts",""+accounts);
            Toast.makeText(getContext(), "ส่งคำเชิญแล้ว", Toast.LENGTH_SHORT).show();
            int REQUEST_CODE = 1150;
            Intent intent = new Intent();
            Fragment fragment = getTargetFragment();
            fragment.onActivityResult(REQUEST_CODE, Activity.RESULT_OK, intent);
            dismiss();
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }
}
