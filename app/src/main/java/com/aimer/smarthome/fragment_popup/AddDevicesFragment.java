package com.aimer.smarthome.fragment_popup;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.activity.NewDeviceActivity;
import com.aimer.smarthome.model.ModelDevices;


public class AddDevicesFragment extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "AddDevicesFragment";
    private ImageView imgClosefragment;
    private LinearLayout lnl_light_bulb,lnl_sliding_door,lnl_magnetic_door,lnl_thermometer,lnl_air_conditioner
            ,lnl_cctv,lnl_tv,lnl_magnetic_sensor,lnl_fire_alarm,lnl_electric_meter,lnl_alert;
    private ModelDevices modelDevices = new ModelDevices();
    private Intent intent;
    public AddDevicesFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        modelDevices = getArguments().getParcelable("ModelDevices");
        intent = new Intent(getContext(), NewDeviceActivity.class);
        intent.putExtra("ModelDevices",modelDevices);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_devices, container, false);

        imgClosefragment = view.findViewById(R.id.img_close_fragment_add_devices);
        imgClosefragment.setOnClickListener(this);
        lnl_light_bulb = view.findViewById(R.id.lnl_light_bulb);
        lnl_light_bulb.setOnClickListener(this);
        lnl_sliding_door = view.findViewById(R.id.lnl_sliding_door);
        lnl_sliding_door.setOnClickListener(this);
        lnl_magnetic_door = view.findViewById(R.id.lnl_magnetic_door);
        lnl_magnetic_door.setOnClickListener(this);
        lnl_thermometer = view.findViewById(R.id.lnl_thermometer);
        lnl_thermometer.setOnClickListener(this);
        lnl_air_conditioner = view.findViewById(R.id.lnl_air_conditioner);
        lnl_air_conditioner.setOnClickListener(this);
        lnl_cctv = view.findViewById(R.id.lnl_cctv);
        lnl_cctv.setOnClickListener(this);
        lnl_tv = view.findViewById(R.id.lnl_tv);
        lnl_tv.setOnClickListener(this);
        lnl_magnetic_sensor = view.findViewById(R.id.lnl_magnetic_sensor);
        lnl_magnetic_sensor.setOnClickListener(this);
        lnl_fire_alarm = view.findViewById(R.id.lnl_fire_alarm);
        lnl_fire_alarm.setOnClickListener(this);
        lnl_electric_meter = view.findViewById(R.id.lnl_electric_meter);
        lnl_electric_meter.setOnClickListener(this);
        lnl_alert = view.findViewById(R.id.lnl_alert);
        lnl_alert.setOnClickListener(this);


        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()){
            case R.id.img_close_fragment_add_devices:
                dismiss();
                break;
            case R.id.lnl_light_bulb:

                intent.putExtra("ICON_DEVICES","ic_light_bulb");
                intent.putExtra("NAME_DEVICES","Light bulb");
                intent.putExtra("TYPE_DEVICES", 1);
                startActivity(intent);
                break;
            case R.id.lnl_sliding_door:
                intent.putExtra("ICON_DEVICES","ic_sliding_door");
                intent.putExtra("NAME_DEVICES","Sliding Door");
                intent.putExtra("TYPE_DEVICES", 2);
                startActivity(intent);
                break;
            case R.id.lnl_magnetic_door:
                intent.putExtra("ICON_DEVICES","ic_magnetic_door");
                intent.putExtra("NAME_DEVICES","Magnetic Door");
                intent.putExtra("TYPE_DEVICES", 3);
                startActivity(intent);
                break;
            case R.id.lnl_thermometer:
                intent.putExtra("ICON_DEVICES","ic_thermometer");
                intent.putExtra("NAME_DEVICES","Thermometer");
                intent.putExtra("TYPE_DEVICES", 4);
                startActivity(intent);
                break;
            case R.id.lnl_air_conditioner:
                intent.putExtra("ICON_DEVICES","ic_air_conditioner");
                intent.putExtra("NAME_DEVICES","Air Conditioner");
                intent.putExtra("TYPE_DEVICES", 5);
                startActivity(intent);
                break;
            case R.id.lnl_cctv:
                intent.putExtra("ICON_DEVICES","ic_cctv");
                intent.putExtra("NAME_DEVICES","CCTV");
                intent.putExtra("TYPE_DEVICES", 6);
                startActivity(intent);
                break;
            case R.id.lnl_tv:
                intent.putExtra("ICON_DEVICES","ic_tv");
                intent.putExtra("NAME_DEVICES","TV");
                intent.putExtra("TYPE_DEVICES", 7);
                startActivity(intent);
                break;
            case R.id.lnl_magnetic_sensor:
                intent.putExtra("ICON_DEVICES","ic_magnetic_sensor");
                intent.putExtra("NAME_DEVICES","Magnetic Sensor");
                intent.putExtra("TYPE_DEVICES", 8);
                startActivity(intent);
                break;
            case R.id.lnl_fire_alarm:
                intent.putExtra("ICON_DEVICES","ic_fire_alarm");
                intent.putExtra("NAME_DEVICES","Fire Alarm");
                intent.putExtra("TYPE_DEVICES", 9);
                startActivity(intent);
                break;
            case R.id.lnl_electric_meter:
                intent.putExtra("ICON_DEVICES","ic_electric_meter");
                intent.putExtra("NAME_DEVICES","Electric Meter");
                intent.putExtra("TYPE_DEVICES", 10);
                startActivity(intent);
                break;
            case R.id.lnl_alert:
                intent.putExtra("ICON_DEVICES","ic_alert");
                intent.putExtra("NAME_DEVICES","Alert");
                intent.putExtra("TYPE_DEVICES", 11);
                startActivity(intent);
                break;
        }


    }


}
