package com.aimer.smarthome.fragment_popup;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.Nullable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.aimer.smarthome.R;
import com.aimer.smarthome.model.ModelHome;


public class SettingsHomeFragment extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "SettingsHomeFragment";

    //Toolbar
    private Toolbar toolbar_SettingsHome;

    //TextView
    private TextView txtHomeName;
    private TextView txtDeleteHome;
    private TextView btnFamily;

    //Model
    private ModelHome modelHome = new ModelHome();

    public SettingsHomeFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        modelHome = getArguments().getParcelable("MODEL_HOME");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings_home, container, false);

        toolbar_SettingsHome = view.findViewById(R.id.Toolbar_Settings_Home);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar_SettingsHome);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
        toolbar_SettingsHome.setTitle("Settings");
        toolbar_SettingsHome.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        toolbar_SettingsHome.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Back", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });

        txtHomeName = view.findViewById(R.id.txt_home_name);
        txtDeleteHome = view.findViewById(R.id.delete_home);
        btnFamily = view.findViewById(R.id.familyBtn);

        txtHomeName.setText(modelHome.getHomeName());
        txtHomeName.setOnClickListener(this);
        txtDeleteHome.setOnClickListener(this);
        btnFamily.setOnClickListener(this);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.delete_home:
                onClickDeleteHome();
                break;
            case R.id.txt_home_name:
                onClickRenameHome();
                break;
            case R.id.familyBtn:
                onClickFamily();
                break;
        }
    }

    private void onClickFamily() {
        Bundle args = new Bundle();
        args.putParcelable("MODEL_HOME",modelHome);
        FragmentManager manager = getFragmentManager();
        FamilyFragment dialog = new FamilyFragment();
        dialog.setArguments(args);
        dialog.show(manager,"FamilyFragment");
    }

    private void onClickRenameHome() {
        Bundle args = new Bundle();
        args.putParcelable("MODEL_HOME",modelHome);
        FragmentManager manager = getFragmentManager();
        RenameHomeFragment dialog =  new RenameHomeFragment();
        dialog.setArguments(args);
        dialog.show(manager,"RenameHomeFragment");
    }

    private void onClickDeleteHome() {
        Bundle args = new Bundle();
        args.putParcelable("MODEL_HOME",modelHome);
        FragmentManager manager = getFragmentManager();
        DeleteHomeFragment dialog =  new DeleteHomeFragment();
        dialog.setArguments(args);
        dialog.show(manager,"DeleteHomeFragment");
    }
}
