package com.aimer.smarthome.fragment_popup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.adapter.AdapterSpinnerFilterDevices;
import com.aimer.smarthome.asynctask.AsyncTaskTypeDevices;
import com.aimer.smarthome.fragment_warning.DataNotFoundFragment;
import com.aimer.smarthome.interface_callback.CallBackServiceTypeDevices;
import com.aimer.smarthome.model.ModelTypeDevices;
import com.aimer.smarthome.utils.ItemClickListenerSpinnerFilter;

import java.util.ArrayList;


public class SelectFilterFragment extends DialogFragment implements CallBackServiceTypeDevices, ItemClickListenerSpinnerFilter {

    private static String TAG = "SelectFilterFragment";

    //String
    private String url_TypeDevices = "";

    //Int
    private int REQ_CODE = 3356;

    //ModelTypeDevices
    private ArrayList<ModelTypeDevices> listTypeDevices;

    //View
    View view;

    //Adapter
    AdapterSpinnerFilterDevices mAdapterSpinner;

    //RecyclerView
    private RecyclerView mRecyclerView;


    //Fragment
    private Fragment fragment;


    public SelectFilterFragment(String url) {
        this.url_TypeDevices = url;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (savedInstanceState != null){
            url_TypeDevices = savedInstanceState.getString("URL_TYPE_DEVICES");
            listTypeDevices = savedInstanceState.getParcelableArrayList("LIST_TYPE_DEVICES");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_select_filter, container, false);
        mRecyclerView = view.findViewById(R.id.RecyclerView_Select_Filter);

        if (savedInstanceState == null){
            new AsyncTaskTypeDevices(this,view).execute(url_TypeDevices);
            fragment = this.getTargetFragment();
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerModelTypeDevices(ArrayList<ModelTypeDevices> modelTypeDevices) {
        if (modelTypeDevices != null && modelTypeDevices.size() != 0){
            modelTypeDevices.add(0,new ModelTypeDevices(0,"All Devices"));
            int lastIndex = modelTypeDevices.size();
            modelTypeDevices.add(lastIndex,new ModelTypeDevices(99,"Other"));
            this.listTypeDevices = modelTypeDevices;
            mAdapterSpinner = new AdapterSpinnerFilterDevices(getContext(),listTypeDevices,this);
            mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),1));
            mRecyclerView.setAdapter(mAdapterSpinner);
        }else {
            dismiss();
            delayMillis();
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClicked(ModelTypeDevices typeDevices) {
        if (fragment != null) {
            Toast.makeText(getContext(), typeDevices.getTypeDevices() + "", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            intent.putExtra("SELECTED_TYPE_DEVICES",typeDevices);
            fragment.onActivityResult(REQ_CODE, Activity.RESULT_OK, intent);
            dismiss();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("URL_TYPE_DEVICES",url_TypeDevices);
        outState.putParcelableArrayList("LIST_TYPE_DEVICES",listTypeDevices);
    }

    private void delayMillis(){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                Fragment prev = getFragmentManager().findFragmentById(getId());
//                if (prev != null) {
//                    DialogFragment df = (DialogFragment) prev;
//                df.dismiss();
//                }

                DataNotFoundFragment fragment = new DataNotFoundFragment();
                fragment.show(getTargetFragment().getFragmentManager(), TAG);
            }
        }, 1000);
    }


}
