package com.aimer.smarthome.fragment_popup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.aimer.smarthome.R;
import com.aimer.smarthome.activity.MainActivity;
import com.aimer.smarthome.doget.DoGet;
import com.aimer.smarthome.model.ModelHome;


public class RenameHomeFragment extends DialogFragment implements View.OnClickListener {

    private ModelHome modelHome = new ModelHome();

    //EditText
    private EditText edtRenameHome;

    //TextView
    private TextView txtNameToolBar;
    private TextView txtSaveToolBar;

    //ImageButton
    private ImageButton btnCloseRename;

    //String
    private String url_json = "";

    public RenameHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        modelHome = getArguments().getParcelable("MODEL_HOME");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_rename_home, container, false);

        edtRenameHome = view.findViewById(R.id.edt_rename_home);
        txtNameToolBar = view.findViewById(R.id.txt_name_tool_bar);
        txtSaveToolBar = view.findViewById(R.id.txt_save_tool_bar);
        btnCloseRename = view.findViewById(R.id.btn_close_rename);

        edtRenameHome.setText(modelHome.getHomeName());
        edtRenameHome.setSelection(edtRenameHome.getText().length());
        txtSaveToolBar.setOnClickListener(this);
        btnCloseRename.setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        edtRenameHome.setFocusableInTouchMode(true);
        edtRenameHome.requestFocus();

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();

        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_close_rename:
                    dismiss();
                break;
            case R.id.txt_save_tool_bar:
                    onClickSaveRenameHome();
                break;

        }
    }

    private void onClickSaveRenameHome() {
        if (TextUtils.isEmpty(edtRenameHome.getText().toString())){
            Toast.makeText(getContext(), "Empty field not allowed!", Toast.LENGTH_SHORT).show();
        }else {
            String nameHome = edtRenameHome.getText().toString().replaceAll(" ", "%20");
            url_json = getResources().getString(R.string.api_ai_control)+"smart_home_api/home/controller_home/RenameHome?homeId="+modelHome.getHomeId()+"&homeName="+nameHome;
            new DoGet(this).execute(url_json);
            getContext().getSharedPreferences("PREF_APP_SELECTED_HOME_NAME", Context.MODE_PRIVATE).edit().putString("APP_SELECTED_HOME_NAME", nameHome).apply();
            startActivity(new Intent(getContext(), MainActivity.class));
            dismiss();
        }

    }
}
