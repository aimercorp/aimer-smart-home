package com.aimer.smarthome.fragment_popup;


import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.adapter.AdapterRecyclerViewMultipleJoins;
import com.aimer.smarthome.model.ModelAccounts;
import com.aimer.smarthome.utils.ItemClickListenerJoinFamily;

import java.util.ArrayList;

public class MultipleJoinsFamilyFragment extends DialogFragment implements ItemClickListenerJoinFamily {

    private RecyclerView mRecyclerView;

    private AdapterRecyclerViewMultipleJoins mAdapter;

    private ArrayList<ModelAccounts> accountsArrayList;

    public MultipleJoinsFamilyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        setRetainInstance(true);

        if (getArguments() != null){
            accountsArrayList = getArguments().getParcelableArrayList("LIST_MODEL_ACCOUNT");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_multiple_joins_family, container, false);

        mRecyclerView = view.findViewById(R.id.RecyclerView_Multiple_Joins);
        mAdapter = new AdapterRecyclerViewMultipleJoins(getContext(),accountsArrayList,this);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),1));
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                Toast.makeText(getContext(), "Back", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void itemClicked(ModelAccounts modelAccounts) {
        Bundle args = new Bundle();
        args.putParcelable("MODEL_ACCOUNT",modelAccounts);
        FragmentManager manager = getFragmentManager();
        JoinFamilyFragment dialog = new JoinFamilyFragment();
        dialog.setArguments(args);
        dialog.show(manager,"JoinFamilyFragment");
    }
}
