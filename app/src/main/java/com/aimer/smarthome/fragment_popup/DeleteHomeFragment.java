package com.aimer.smarthome.fragment_popup;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.aimer.smarthome.R;
import com.aimer.smarthome.activity.MainActivity;
import com.aimer.smarthome.doget.DoGet;
import com.aimer.smarthome.model.ModelHome;


public class DeleteHomeFragment extends DialogFragment implements View.OnClickListener {

    private ModelHome modelHome = new ModelHome();
    private TextView txtDeleteTitle, txtDeleteName, txtDeleteEmail;
    private Button btnDeleteHome;
    private Toolbar mToolbar;
    private ProgressDialog alertDialog = null;

    public DeleteHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        modelHome = getArguments().getParcelable("MODEL_HOME");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delete_home, container, false);

            mToolbar = view.findViewById(R.id.Toolbar_Delete_Home);
            txtDeleteTitle = view.findViewById(R.id.txt_delete_title);
            txtDeleteName = view.findViewById(R.id.txt_delete_name);
            txtDeleteEmail = view.findViewById(R.id.txt_delete_email);
            btnDeleteHome = view.findViewById(R.id.btn_delete_home);

        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
        mToolbar.setTitle("");
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

            txtDeleteTitle.setText(modelHome.getHomeName());
            txtDeleteName.setText(getContext().getSharedPreferences("PREF_APP_ACCOUNT_NAME", Context.MODE_PRIVATE).getString("APP_ACCOUNT_NAME", "INVALID NAME"));
            txtDeleteEmail.setText(getContext().getSharedPreferences("PREF_APP_ACCOUNT_EMAIL", Context.MODE_PRIVATE).getString("APP_ACCOUNT_EMAIL", "INVALID EMAIL"));
            btnDeleteHome.setOnClickListener(this);

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_delete_home:
                onClickDeleteHome();
                break;
        }
    }

    private void onClickDeleteHome() {
        String urlJson = "";
        urlJson = getString(R.string.api_ai_control)+"/smart_home_api/home/controller_home/DeleteHome?homeId="+modelHome.getHomeId();
        new DoGet(this).execute(urlJson);
        showProgressDialog();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                getActivity().getSharedPreferences("PREF_APP_SELECTED_HOME_NAME", Context.MODE_PRIVATE).edit().putString("APP_SELECTED_HOME_NAME", null).apply();
                getActivity().getSharedPreferences("PREF_APP_SELECTED_HOME_ID",Context.MODE_PRIVATE).edit().putInt("APP_SELECTED_HOME_ID",0).apply();
                dismissProgressDialog();
                alertDialog();
            }
        }, 2000);


    }

    public void showProgressDialog(){
        alertDialog = new ProgressDialog(getContext());
        alertDialog.setMessage("Wait...");
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    public void alertDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("Title");
        builder.setPositiveButton("Ok.", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(getContext(), MainActivity.class));
            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

}
