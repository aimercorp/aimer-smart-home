package com.aimer.smarthome.fragment_popup;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.adapter.AdapterRecyclerViewSelectHome;

import com.aimer.smarthome.model.ModelHome;
import com.aimer.smarthome.utils.ItemClickListenerSelectHome;

import java.util.ArrayList;


public class SelectHomeFragment extends DialogFragment implements ItemClickListenerSelectHome {
    private static final String TAG ="SelectHomeFragment";
    private ArrayList<ModelHome> modelHomes;
    private RecyclerView recyclerView;
    private AdapterRecyclerViewSelectHome mAdapter;
    public SelectHomeFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        modelHomes = getArguments().getParcelableArrayList("LIST_MODEL_HOME");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_select_home, container, false);

        recyclerView = view.findViewById(R.id.RecyclerView_select_home);
        mAdapter = new AdapterRecyclerViewSelectHome(getContext(),modelHomes,this);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        recyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawableResource(R.color.TRANSPARENT);
        }
    }


    @Override
    public void itemClicked(ModelHome home, int position) {
        int REQUEST_CODE = 5678;
        Intent intent = new Intent();
        intent.putExtra("GET_POSITION_HOME",position);
        intent.putExtra("GET_MODEL_HOME", home);
        Fragment fragment = getTargetFragment();
        fragment.onActivityResult(REQUEST_CODE, Activity.RESULT_OK, intent);
        dismiss();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
