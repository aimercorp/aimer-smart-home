package com.aimer.smarthome.fragment_popup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aimer.smarthome.R;
import com.aimer.smarthome.adapter.AdapterRecyclerViewFamily;
import com.aimer.smarthome.asynctask.AsyncTaskGetFamily;
import com.aimer.smarthome.fragment.HomeFragment;
import com.aimer.smarthome.fragment_filter.FilterALLFragment;
import com.aimer.smarthome.fragment_filter.FilterFavoriteFragment;
import com.aimer.smarthome.interface_callback.CallBackServiceGetFamily;
import com.aimer.smarthome.model.ModelAccounts;
import com.aimer.smarthome.model.ModelHome;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


public class FamilyFragment extends DialogFragment implements CallBackServiceGetFamily,MenuItem.OnMenuItemClickListener, View.OnClickListener {

    //Set TAG
    private static final String TAG ="FamilyFragment";

    //String
    private String url_json = "";

    //Toolbar
    private Toolbar mToolbar;

    //RecyclerView
    private RecyclerView mRecyclerView;

    //AdapterRecyclerViewFamily
    private AdapterRecyclerViewFamily mAdapter;

    //Fragment
    private FamilyFragment fragment;

    //ModelHome
    private ModelHome modelHome = new ModelHome();


    public FamilyFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        fragment = new FamilyFragment();
        if (getArguments() != null){
            modelHome = getArguments().getParcelable("MODEL_HOME");
            url_json = getResources().getString(R.string.api_ai_control)+"/smart_home_api/invite/controller_invite/GetFamilyinHome?homeId="+modelHome.getHomeId();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_family, container, false);

        mToolbar = view.findViewById(R.id.Toolbar_Family);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Family");
        }
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mRecyclerView = view.findViewById(R.id.RecyclerView_Family);



        new AsyncTaskGetFamily(this, view).execute(url_json);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                dismiss();
            }
        };
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.family_navigation_menu,menu);
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.findItem(R.id.action_invite_family);
        item.setOnMenuItemClickListener(this);

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_invite_family:
                onClickInviteFamily();
                return true;
        }
        return false;
    }

    private void onClickInviteFamily() {
        int REQ_CODE = 1150;
        Bundle args = new Bundle();
        args.putParcelable("MODEL_HOME",modelHome);
        FragmentManager manager = getFragmentManager();
        InviteFamilyFragment dialog = new InviteFamilyFragment();
        dialog.setTargetFragment(FamilyFragment.this, REQ_CODE);
        dialog.setArguments(args);
        dialog.show(manager,"InviteFamilyFragment");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1150 && resultCode == RESULT_OK) {
            dismiss();
            changeFragment();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    // Reload current fragment
    private void changeFragment() {
        try {
            Bundle args = new Bundle();
            args.putParcelable("MODEL_HOME",modelHome);
            FragmentManager manager = getFragmentManager();
            FamilyFragment dialog = new FamilyFragment();
            dialog.setArguments(args);
            dialog.show(manager,"FamilyFragment");
        } catch (Exception exception) {
            Log.i(TAG,exception.toString());
        }
    }//End Reload current fragment

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerFamily(ArrayList<ModelAccounts> listAccounts) {
        if (listAccounts != null && listAccounts.size() != 0){
            Log.i("Check data",""+listAccounts);

            mAdapter = new AdapterRecyclerViewFamily(getContext(),listAccounts);
            mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),1));
            mRecyclerView.setAdapter(mAdapter);

        }

    }

    @Override
    public void onRequestFailed(String result) {

    }
}
