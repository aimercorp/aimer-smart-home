package com.aimer.smarthome.fragment_popup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aimer.smarthome.R;
import com.aimer.smarthome.adapter.AdapterRecyclerViewSelectChannel;
import com.aimer.smarthome.asynctask.AsyncTaskBoard;
import com.aimer.smarthome.interface_callback.CallBackServiceBoard;
import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.model.ModelDevices;
import com.aimer.smarthome.utils.ItemClickListenerSelectChannel;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class SelectChannelFragment extends DialogFragment implements View.OnClickListener , CallBackServiceBoard,ItemClickListenerSelectChannel {

    public static final String TAG = "SelectChannelFragment";
    private ImageView img_close_fragment_select_channel;
    private TextView txt_SumAvailable,txt_BoardName;
    private RecyclerView recyclerView;
    private LinearLayout mLinearLayout;
    private AdapterRecyclerViewSelectChannel mAdapter;
    private String url_json = "";
    private ArrayList<ModelBoard> arrayListModelBoard = new ArrayList<>();
    private int accountID,modelBoard_Position;
    private Bundle args;

    public SelectChannelFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        args = new Bundle();
        accountID =  getContext().getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).getInt("APP_ACCOUNT_ID", 0);
        boolean verifySelectedBoard = getContext().getSharedPreferences("PREF_APP_SELECTED_BOARD", Context.MODE_PRIVATE).getBoolean("APP_SELECTED_BOARD",false);
        if (verifySelectedBoard){
            getContext().getSharedPreferences("PREF_APP_SELECTED_BOARD", Context.MODE_PRIVATE).edit().putBoolean("APP_SELECTED_BOARD", false).apply();
            modelBoard_Position = getArguments().getInt("ModelBoard_Position",0);
        }else {
            modelBoard_Position = 0;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_channel, container, false);

        recyclerView = view.findViewById(R.id.RecyclerView_Select_Channel);
        img_close_fragment_select_channel = view.findViewById(R.id.img_close_fragment_select_channel);
        img_close_fragment_select_channel.setOnClickListener(this);
        txt_SumAvailable = view.findViewById(R.id.txt_sum_available);
        mLinearLayout = view.findViewById(R.id.onClickSelectedBoard);
        mLinearLayout.setOnClickListener(this);
        txt_BoardName = view.findViewById(R.id.txt_board_name);

        url_json = getResources().getString(R.string.api_ai_control)+"smart_home_api/board/controller_board/GetBoard?uid="+accountID;
        new AsyncTaskBoard(this,view).execute(url_json);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_close_fragment_select_channel:
                dismiss();
                break;
            case R.id.onClickSelectedBoard:
                if (arrayListModelBoard != null && arrayListModelBoard.size() != 0) {
                    args.putParcelableArrayList("LIST_MODEL_BOARD", arrayListModelBoard);
                    FragmentManager manager = getFragmentManager();
                    SelectBoardFragment dialog = new SelectBoardFragment();
                    dialog.setArguments(args);
                    dialog.show(manager, "SelectBoardFragment");
                    dismiss();
                }
                break;
                default:
                    break;

        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelBoard> listBoard) {
        if (listBoard != null && listBoard.size() != 0){
            Log.i("Check data", "" + listBoard);
            this.arrayListModelBoard = listBoard;
            int available = arrayListModelBoard.size();
            txt_BoardName.setText(arrayListModelBoard.get(modelBoard_Position).getBoardName());
            txt_SumAvailable.setText(available+" available");
            mAdapter = new AdapterRecyclerViewSelectChannel(getContext(),arrayListModelBoard.get(modelBoard_Position).getModelDevices(),this);
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
            recyclerView.setAdapter(mAdapter);
        }

    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void itemClicked(ModelDevices modelDevices) {

        args.putParcelable("ModelDevices",modelDevices);
        FragmentManager manager = getFragmentManager();
        AddDevicesFragment dialog =  new AddDevicesFragment();
        dialog.setArguments(args);
        dialog.show(manager,"AddDevicesFragment");
    }

}
