package com.aimer.smarthome.fragment_popup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.aimer.smarthome.R;
import com.aimer.smarthome.activity.AccountActivity;
import com.aimer.smarthome.activity.NewControllerActivity;
import com.aimer.smarthome.activity.NewHomeActivity;

public class SettingFragment extends DialogFragment implements View.OnClickListener {


    public SettingFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e("DEBUG", "onAttach of SettingFragment 1");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        Log.e("DEBUG", "onCreate of SettingFragment 2");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("DEBUG", "onCreateView of SettingFragment 3");
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
            if (view != null){
                ImageView mBackPressed = view.findViewById(R.id.action_BackPressed);
                LinearLayout mSettingDevices = view.findViewById(R.id.setting_devices);
                LinearLayout mSettingAddBoard = view.findViewById(R.id.setting_add_board);
                LinearLayout mSettingAddHome = view.findViewById(R.id.setting_add_home);
                LinearLayout mSettingAccount = view.findViewById(R.id.setting_account);

                mBackPressed.setOnClickListener(this);
                mSettingDevices.setOnClickListener(this);
                mSettingAddBoard.setOnClickListener(this);
                mSettingAddHome.setOnClickListener(this);
                mSettingAccount.setOnClickListener(this);
            }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("DEBUG", "onActivityCreated of SettingFragment 4");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("DEBUG", "onStart of SettingFragment 5");
        Dialog dialog = getDialog();

        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("DEBUG", "onResume of SettingFragment 6");
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.e("DEBUG", "onPause of SettingFragment 7");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("DEBUG", "onStop of SettingFragment 8");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("DEBUG", "onDestroyView of SettingFragment 9");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("DEBUG", "onDestroy of SettingFragment 10");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("DEBUG", "onDetach of SettingFragment 11");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                super.onBackPressed();
                dismiss();
            }
        };

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.action_BackPressed:
                dismiss();
                break;
            case R.id.setting_devices:
                FragmentManager manager = getFragmentManager();
                SelectChannelFragment dialog = new SelectChannelFragment();
                dialog.show(manager,"SelectChannelFragment");
                dismiss();
                break;
            case R.id.setting_add_board:
                startActivity(new Intent(getContext(), NewControllerActivity.class));
                dismiss();
                break;

            case R.id.setting_add_home:
                startActivity(new Intent(getContext(), NewHomeActivity.class));
                dismiss();
                break;
            case R.id.setting_account:
                startActivity(new Intent(getContext(), AccountActivity.class));
                dismiss();
                break;
        }

    }

    private void delayMillis(){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 1000);
    }
}
