package com.aimer.smarthome.fragment_filter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.aimer.smarthome.R;
import com.aimer.smarthome.asynctask.AsyncTaskAddFavourite;
import com.aimer.smarthome.asynctask.AsyncTaskDevices;
import com.aimer.smarthome.asynctask.AsyncTaskFilterOn;
import com.aimer.smarthome.doget.DoGet;
import com.aimer.smarthome.fragment_popup.PowerPushFragment;
import com.aimer.smarthome.interface_callback.CallBackServiceAddFavourite;
import com.aimer.smarthome.interface_callback.CallBackServiceDevices;
import com.aimer.smarthome.interface_callback.CallBackServiceFilterOn;
import com.aimer.smarthome.model.DeviceMagneticDoor;
import com.aimer.smarthome.model.DeviceThermometer;
import com.aimer.smarthome.model.ModelBoard;
import com.aimer.smarthome.model.ModelDevices;
import com.aimer.smarthome.mqtt.MqttHelper;
import com.aimer.smarthome.sectioned_expandable.LayoutHelperBoard;
import com.aimer.smarthome.utils.ItemClickListener;
import com.aimer.smarthome.utils.ItemClickListenerOnClickDevices;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


public class FilterOnFragment extends Fragment implements ItemClickListener, ItemClickListenerOnClickDevices, CallBackServiceDevices, CallBackServiceFilterOn, CallBackServiceAddFavourite, View.OnClickListener {

    //Set TAG
    private static final String TAG ="FilterOnFragment";

    //Set String
    private String urlJsonUpdateDevices = "",url_json = "";

    //TextView
    private TextView mCloseAll;

    //RecyclerView
    private RecyclerView recyclerView;

    //LayoutHelperBoard
    private LayoutHelperBoard mLayoutHelperBoard;

    //MqttHelper
    private MqttHelper MqttClient;
    private MqttAndroidClient client;
    private String android_id;

    //Array List BoardID
    private ArrayList<String> listBoardID = new ArrayList<>();

    //View
    private View view;

    //RelativeLayout
    private RelativeLayout mRLayoutFilterClose;

    //ProgressDialog
     private ProgressDialog alertDialog = null;

    //FilterOnFragment
    private FilterOnFragment currentFragment;

    //TextView
    private EditText mSpinnerFilter;

    public FilterOnFragment() {

    }

    public FilterOnFragment(String url_json, EditText spinner_filter) {
        this.url_json = url_json;
        this.mSpinnerFilter = spinner_filter;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        currentFragment = (FilterOnFragment) getFragmentManager().findFragmentById(getId());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mSpinnerFilter.setText("All Devices");
        android_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        MqttClient = new MqttHelper();
        client = MqttClient.getMqttClient(getContext(), android_id);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_filter_on, container, false);
        //set mRLayoutFilterClose
        mRLayoutFilterClose = view.findViewById(R.id.RLayout_filter_close_all);
        //set Close All
        mCloseAll = view.findViewById(R.id.filter_close_all_devices);
        //set onClick CloseAll
        mCloseAll.setOnClickListener(this);
        new AsyncTaskDevices(this,view).execute(url_json);
        //set recyclerView
        recyclerView =  view.findViewById(R.id.RecyclerView_ItemHome);
        //set LayoutHelperBoard
        mLayoutHelperBoard = new LayoutHelperBoard(getContext(),recyclerView,2,this,this);
        mSpinnerFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mLayoutHelperBoard.setFilter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return view;
    }

    // AsyncTask GET Board and Devices
    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListenerStatusFavorite(ArrayList<Object> status) {
        if (status != null && status.size() != 0){
            Log.i("Check data", "" + status);
            boolean state = Boolean.parseBoolean(status.get(0).toString());
            String msg = status.get(1).toString();
            Toast.makeText(getContext(), msg+"", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestCompleteListener(boolean result) {
        if (result){
            dismissProgressDialog();
            changeFragment(currentFragment,TAG);
        }
    }

    @Override
    public void onRequestCompleteListenerListDevices(ArrayList<ModelBoard> listBoard) {
        if (listBoard != null && listBoard.size() != 0) {
            Log.i("Check data", "" + listBoard);
            mLayoutHelperBoard.clearAllSection();
            mLayoutHelperBoard.notifyDataSetChanged();
            for (ModelBoard list_model_board : listBoard) {
                ArrayList<Object> list = new ArrayList<>();
                listBoardID.add(String.valueOf(list_model_board.getBoardId()));
                for (Object light : list_model_board.getModelDevices()){
                    list.add(light);
                }

                if (list.size() > 0){
                    mRLayoutFilterClose.setVisibility(View.VISIBLE);
                }

                mLayoutHelperBoard.addSection(list_model_board.getBoardId(),list_model_board.getBoardMacAddress(),list_model_board.getBoardName(),list);
                mLayoutHelperBoard.notifyDataSetChanged();
            }

        }else {
            mLayoutHelperBoard.clearAllSection();
            mLayoutHelperBoard.notifyDataSetChanged();
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }// End AsyncTask GET Board and Devices


    //Event Item Clicked
    @Override
    public void itemClicked(ModelBoard modelBoard) {

    }

    @Override
    public void itemClicked(ModelDevices modelDevices) {
        onClickDevicesPowerPush(modelDevices);
    }

    @Override
    public void itemClickedThermometer(DeviceThermometer thermometer) {

    }

    @Override
    public void itemClickedMagneticDoor(DeviceMagneticDoor magneticDoor) {

    }

    @Override
    public void itemClickedFavourite(ModelDevices modelDevices, ImageView imgFavourite) {
        int accountID =  getContext().getSharedPreferences("PREF_APP_ACCOUNT_ID", Context.MODE_PRIVATE).getInt("APP_ACCOUNT_ID", 0);
        String url_addFavourite = getResources().getString(R.string.api_ai_control)+"/smart_home_api/favourite/controller_favourite/addFavourite?accountsId="+accountID+"&devicesId="+modelDevices.getDevicesId();
        new AsyncTaskAddFavourite(this).execute(url_addFavourite);
        changeFragment(currentFragment,TAG);
    }


    @Override
    public void setTemp(final TextView temp) {
        client.setCallback(new MqttCallbackExtended(){
            @Override
            public void connectionLost(Throwable cause) {
            }
            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.d(TAG, new String(message.getPayload()));
                temp.setText(onParserContentDHT(new String(message.getPayload()))+" °C");
            }
            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
            }
        });
    }

    @Override
    public void itemClickedOnDevices(ModelDevices modelDevices) {
        modelDevices.setDevicesStatus("1");
        String JsonFormat = "{\"deviceId\":\""+modelDevices.getModelBoard().getBoardMacAddress()+"\",\"action\":\"action.devices.commands.relay.OnOff\",\"value\":{\"on\":1,\"pin\":"+modelDevices.getModelPin().getPin()+"}}";
        sendEvent(JsonFormat,modelDevices.getModelBoard().getBoardMacAddress()+"/ESP/RELAY");
        urlJsonUpdateDevices = getResources().getString(R.string.api_ai_control)+"/smart_home_api/devices/controller_devices/UpdateDevices?id="+modelDevices.getDevicesId()+"&status=1";
//        new DoGet(this).execute(urlJsonUpdateDevices);
    }

    @Override
    public void itemClickedOffDevices(ModelDevices modelDevices) {
        modelDevices.setDevicesStatus("0");
        String JsonFormat = "{\"deviceId\":\""+modelDevices.getModelBoard().getBoardMacAddress()+"\",\"action\":\"action.devices.commands.relay.OnOff\",\"value\":{\"on\":0,\"pin\":"+modelDevices.getModelPin().getPin()+"}}";
        sendEvent(JsonFormat,modelDevices.getModelBoard().getBoardMacAddress()+"/ESP/RELAY");
        urlJsonUpdateDevices = getResources().getString(R.string.api_ai_control)+"/smart_home_api/devices/controller_devices/UpdateDevices?id="+modelDevices.getDevicesId()+"&status=0";
        new DoGet(this).execute(urlJsonUpdateDevices);
        changeFragment(currentFragment,TAG);

    }//End Event Item Clicked


    //Send Event to board
    private void sendEvent(String JsonFormat,String Topic)
    {
        try {
            MqttClient.publishMessage(client, JsonFormat, 1, Topic);
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }//End Send Event to board

    //GET Temps
    private String onParserContentDHT(String dataJSon) {
        String result = "";
        try {
            JSONObject jsonObject = new JSONObject(dataJSon);
            JSONArray jsonArray = jsonObject.getJSONArray("value");

            if (jsonArray != null){
                for(int i=0;i < jsonArray.length();i++) {
                    Log.e("Message", "loop");

                    JSONObject json = jsonArray.optJSONObject(i);
                    result = json.getString("T");
                    String h = json.getString("H");
                    String f = json.getString("F");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return result = "false";
        }

        return result;
    }//End GET Temps

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.filter_close_all_devices:
                onClickCloseAllDevices();
                break;
        }
    }

    //Event Close all device
    private void onClickCloseAllDevices() {
      showProgressDialog();
        String url_json = "";
        String whereBoardID = "%20IN%20("+String.valueOf(listBoardID.subList(0,listBoardID.size())).replaceAll("[\\[\\]\\ ]","")+")";
        url_json = getResources().getString(R.string.api_ai_control)+"/smart_home_api/devices/controller_devices/UpdateStatusDevices?StatusDevices=0&BoardId="+whereBoardID;
        new AsyncTaskFilterOn(this).execute(url_json);



    }//End Event Close all device

    public void showProgressDialog(){
        alertDialog = new ProgressDialog(getContext());
        alertDialog.setMessage("Progress...");
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();

        }
    }

    // Reload current fragment
    private void changeFragment(FilterOnFragment fragment, String tag) {
        try {
            FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
            mFragmentTransaction.replace(getId(), fragment, tag);
            mFragmentTransaction.addToBackStack(tag);
            mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            mFragmentTransaction.detach(currentFragment);
            mFragmentTransaction.attach(currentFragment);
            mFragmentTransaction.commit();
        } catch (Exception exception) {
            Log.i(TAG,exception.toString());
        }
    }//End Reload current fragment

    private void onClickDevicesPowerPush(ModelDevices modelDevices) {
        int REQ_CODE = 1234;
        Bundle args = new Bundle();
        args.putParcelable("MODEL_DEVICES",modelDevices);
        FragmentManager manager = getFragmentManager();
        PowerPushFragment dialog = new PowerPushFragment();
        dialog.setTargetFragment(FilterOnFragment.this, REQ_CODE);
        dialog.setArguments(args);
        dialog.show(manager,"PowerPushFragment");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001 && resultCode == RESULT_OK) {
            changeFragment(currentFragment,TAG);
        }
    }
}
